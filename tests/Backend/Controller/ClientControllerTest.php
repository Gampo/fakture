<?php

namespace Test\Fakture\Backend\Controller;

use Fakture\Backend\Controller\ClientController;
use Fakture\Service\Service\Service;
use Fakture\Tenant\Model\Tenant;
use Laminas\Session\SessionManager;
use PHPUnit\Framework\TestCase;
use Test\Fakture\Stub\TemplateStub;

class ClientControllerTest extends TestCase
{
    public function testEditFormShouldReturnHtml()
    {
        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(6))
            ->method('getStorage')
            ->willReturnSelf();
        $sessionMock->expects(static::exactly(6))
            ->method('offsetGet')
            ->willReturn('test');
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->onlyMethods(['offsetGet'])
            ->disableOriginalConstructor()
            ->getMock();
        $configMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn('admin');
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)->getMock();
        $clientServiceMock = $this->getMockBuilder(\Fakture\Client\Service\Client::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById', 'getTenants'])
            ->getMock();
        $data = [
            'clientId' => 1,
            'name' => 'test client',
            'address' => 'test address',
            'email' => 'test email',
            'accountNumber' => 'test ',
            'bank' => 'test ',
            'tenant' => $this->createStub(Tenant::class),
            'logo' => 'test ',
            'city' => 'test city',
            'country' => 'Serbia',
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'createdAt' => '2022-01-01 00:00:00',
            'updatedAt' => '2022-01-01 00:00:00',
            'contractInfo' => []
        ];
        $clientStub = new \Fakture\Client\Model\Client(...$data);
        $clientServiceMock->expects(static::once())
            ->method('getById')
//            ->willReturn($this->createStub(Client::class));
            ->willReturn($clientStub);
        $clientServiceMock->expects(static::once())
            ->method('getTenants')
            ->willReturn([]);
        $tplMock = TemplateStub::createTemplateInstance();
        $serviceMock = $this->getMockBuilder(Service::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getEntities'])
            ->getMock();
        $serviceMock->expects(static::once())
            ->method('getEntities')
            ->willReturn([]);
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'GET', '', [], ''
        );
        $request = $request->withAttribute('action', 'form');
        $request = $request->withAttribute('id', 1);
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new ClientController($clientServiceMock, $sessionMock, $configMock, $flashMock, $tplMock, $serviceMock);
        $html = $controller($request, $response)->getBody()->getContents();
        $this->assertStringContainsString('<h1 class="page-header">Edit client: test client</h1>', $html);
    }

    public function testSearchAprReturnJson()
    {
        $_SESSION = [];
        $sessionMock = $this->getMockBuilder(SessionManager::class)
            ->onlyMethods(['getStorage'])
            ->addMethods(['offsetGet'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $sessionMock->expects(static::exactly(5))
            ->method('getStorage')
            ->willReturnSelf();
        $sessionMock->expects(static::exactly(5))
            ->method('offsetGet')
            ->willReturn('test');
        $configMock = $this->getMockBuilder(\Laminas\Config\Config::class)
            ->onlyMethods(['offsetGet'])
            ->disableOriginalConstructor()
            ->getMock();
        $configMock->expects(static::exactly(4))
            ->method('offsetGet')
            ->willReturn('admin');
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)->getMock();
        $clientServiceMock = $this->getMockBuilder(\Fakture\Client\Service\Client::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['searchApr'])
            ->getMock();
        $clientServiceMock->expects(static::once())
            ->method('searchApr')
            ->willReturn(['test']);
        $tplMock = TemplateStub::createTemplateInstance();
        $serviceMock = $this->getMockBuilder(Service::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getEntities'])
            ->getMock();
        $request = new \GuzzleHttp\Psr7\ServerRequest(
            'GET', '', [], 'query=test'
        );
        $request = $request->withAttribute('action', 'searchApr');
        $request = $request->withQueryParams(['query' => 'test']);
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new ClientController($clientServiceMock, $sessionMock, $configMock, $flashMock, $tplMock, $serviceMock);
        $json = $controller($request, $response)->getBody()->getContents();
        $this->assertSame('["test"]', $json);
    }
}

