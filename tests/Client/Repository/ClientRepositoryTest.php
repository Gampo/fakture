<?php
namespace Test\Fakture\Client\Repository;

use Fakture\Client\Model\Client;
use Fakture\Client\Repository\ClientRepository;
use PHPUnit\Framework\TestCase;

class ClientRepositoryTest extends TestCase
{
    private $data = [
        'clientId' => 1,
        'name' => 'test client',
        'address' => 'test address',
        'email' => 'test email',
        'accountNumber' => 'test ',
        'bank' => 'test ',
        'tenantId' => 1,
        'logo' => 'test ',
        'city' => 'test city',
        'country' => 'Serbia',
        'pib' => '123123121',
        'mb' => '12312312',
        'type' => Client::TYPE_LEGAL,
        'website' => 'http://www.example.com',
        'zip' => 11000,
        'note' => 'test note',
        'isActive' => 1,
        'createdAt' => '2022-01-01 00:00:00',
        'updatedAt' => '2022-01-01 00:00:00',
        'clientContract' => [
            'serviceId' => [1],
            'info' => [1],
            'number' => [1],
            'deadline' => [1],
            'price' => [
                'amount' => '123',
                'currency' => 'rsd',
            ],
        ]
    ];

    public function testCreateShouldReturnClientModel()
    {
        $returnData = $data = $this->data;
        unset($returnData['clientContract']);

        $clientMapperMock = $this->getMockBuilder(\Fakture\Client\Mapper\Client::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['insert', 'fetchById', 'inTransaction', 'commitTransaction', 'beginTransaction'])
            ->getMock();
        $clientMapperMock->expects(static::once())
            ->method('insert')
            ->willReturn(1);
        $clientMapperMock->expects(static::once())
            ->method('fetchById')
            ->willReturn($returnData);
        $clientMapperMock->expects(static::exactly(2))
            ->method('inTransaction')
            ->willReturnOnConsecutiveCalls(false, true);
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientContractRepoMock = $this->getMockBuilder(\Fakture\Client\Repository\ClientContractRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['create', 'fetchAll'])
            ->getMock();
        $clientContractRepoMock->expects(static::once())
            ->method('fetchAll')
            ->willReturn([]);
//        $clientContractRepoMock->expects(static::once())
//            ->method('create')
//            ->willReturn([]);

        $dt = new \DateTime();
        $repository = new ClientRepository($tenantRepoMock, $clientContractRepoMock, $clientMapperMock, $dt);
        $this->assertInstanceOf(Client::class, $repository->create($data));
    }

    public function testUpdateShouldReturnClientModel()
    {
        $returnData = $data = $this->data;
        unset($returnData['clientContract']);
        unset($returnData['createdAt']);
        unset($returnData['updatedAt']);

        $clientMapperMock = $this->getMockBuilder(\Fakture\Client\Mapper\Client::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['update', 'fetchById', 'inTransaction', 'commitTransaction', 'beginTransaction'])
            ->getMock();
        $clientMapperMock->expects(static::once())
            ->method('update')
            ->willReturn(1);
        $clientMapperMock->expects(static::once())
            ->method('fetchById')
            ->willReturn($returnData);
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientContractRepoMock = $this->getMockBuilder(\Fakture\Client\Repository\ClientContractRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['create', 'fetchAll'])
            ->getMock();
        $clientContractRepoMock->expects(static::once())
            ->method('fetchAll')
            ->willReturn([]);

        $dt = new \DateTime();
        $repository = new ClientRepository($tenantRepoMock, $clientContractRepoMock, $clientMapperMock, $dt);
        $this->assertInstanceOf(Client::class, $repository->update($data));
    }


}
