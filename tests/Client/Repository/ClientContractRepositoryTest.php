<?php
namespace Test\Fakture\Client\Repository;

use Fakture\Client\Model\Client;
use Fakture\Client\Model\ClientContract;
use Fakture\Client\Repository\ClientContractRepository;
use Fakture\Client\Repository\ClientRepository;
use Fakture\Service\Model\Service;
use PHPUnit\Framework\TestCase;

class ClientContractRepositoryTest extends TestCase
{
    private $data = [
        'id' => 1,
        'clientId' => 1,
        'serviceId' => 'test client',
        'deadline' => 7,
        'number' => 'test email',
        'signedAt' => '2022-01-01 00:00:00',
        'validUntil' => '2023-01-01 00:00:00',
        'tenantId' => 1,
        'info' => 'test ',
        'createdAt' => '2022-01-01 00:00:00',
        'updatedAt' => '2022-01-01 00:00:00',
        'priceId' => null,
        'price' => [
            'priceId' => null,
            'amount' => '123',
            'currency' => 'rsd',
        ],
    ];

    public function testCreateShouldReturnModel()
    {
        $returnData = $data = $this->data;

        $clientMapperMock = $this->getMockBuilder(\Fakture\Client\Mapper\ClientContract::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['insert', 'fetchById', 'update'])
            ->getMock();
        $clientMapperMock->expects(static::once())
            ->method('insert')
            ->willReturn(1);
        $clientMapperMock->expects(static::once())
            ->method('fetchById')
            ->willReturn($returnData);
        $priceRepoMock = $this->getMockBuilder(\Fakture\Price\Repository\PriceRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['create', 'getById'])
            ->getMock();
        $serviceRepoMock = $this->getMockBuilder(\Fakture\Service\Repository\ServiceRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $serviceRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn($this->getMockBuilder(Service::class)->disableOriginalConstructor()->getMock());

        $dt = new \DateTime();
        $repository = new ClientContractRepository($clientMapperMock, $dt, $priceRepoMock, $serviceRepoMock);
        $this->assertInstanceOf(ClientContract::class, $repository->create($data));
    }

    public function testUpdateShouldReturnModel()
    {
        $returnData = $data = $this->data;
        unset($returnData['clientContract']);
        unset($returnData['createdAt']);
        unset($returnData['updatedAt']);

        $clientMapperMock = $this->getMockBuilder(\Fakture\Client\Mapper\ClientContract::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['fetchById', 'update'])
            ->getMock();
        $clientMapperMock->expects(static::once())
            ->method('update')
            ->willReturn(1);
        $clientMapperMock->expects(static::once())
            ->method('fetchById')
            ->willReturn($returnData);
        $priceRepoMock = $this->getMockBuilder(\Fakture\Price\Repository\PriceRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['create', 'getById'])
            ->getMock();
        $serviceRepoMock = $this->getMockBuilder(\Fakture\Service\Repository\ServiceRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $serviceRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn($this->getMockBuilder(Service::class)->disableOriginalConstructor()->getMock());

        $dt = new \DateTime();
        $repository = new ClientContractRepository($clientMapperMock, $dt, $priceRepoMock, $serviceRepoMock);
        $this->assertInstanceOf(ClientContract::class, $repository->update($data));
    }


}
