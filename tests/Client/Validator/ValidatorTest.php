<?php
namespace Test\Fakture\Client\Validator;

use Fakture\Client\Model\Client;
use PHPUnit\Framework\TestCase;
use Test\Fakture\Stub\CsrfFalseStub;
use Test\Fakture\Stub\CsrfTrueStub;
use Volnix\CSRF\CSRF;

class ClientTest extends TestCase
{

    public function testValidatorShouldReturnTrue()
    {
        $_SESSION = [];
        $data = [
//            'clientId' => (isset($postData['clientId'])) ? $int->filter($postData['clientId']) : null,
            'name' => 'test client',
            'address' => 'test address',
            'city' => 'test city',
            'country' => 'Serbia',
            'tenantId' => 0,
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'clientContract' => null,
        ];
        $validator = new \Fakture\Client\Validator\Client(new CsrfTrueStub()); // , new CsrfTrueStub()
        $this->assertTrue($validator->isValid($data));
    }

    public function testValidatorShouldReturnFalseAndSetErrors()
    {
        $_SESSION = ['flash_messages' => []];
        $data = [
            'name' => 'te',
            'address' => 'te',
            'city' => 'te',
            'country' => 'Se',
            'tenantId' => 0,
            'pib' => '123123',
            'mb' => '12312',
            'type' => Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'clientContract' => null,
        ];
        $errors = [
            ["Name must be at least 3 characters long."],
            ["Invoice address must be at least 3 characters long."],
            ["Invoice city must be at least 3 characters long."],
            ["Invoice country must be at least 3 characters long."],
            ["Pib must be exactly 9 characters long."],
            ["MB must be at least 8 characters long."]
        ];

        $validator = new \Fakture\Client\Validator\Client(new CsrfFalseStub());
        $this->assertFalse($validator->isValid($data));
        $messages = serialize($validator->getMessages());
        foreach ($errors as $error) {
            $this->assertStringContainsString($error[0], $messages);
        }
    }
}