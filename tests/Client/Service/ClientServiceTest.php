<?php
namespace Test\Fakture\Client\Service;

use Fakture\Apr\Service\ClientSearch;
use Fakture\Price\Service\ExchangeRate;
use Fakture\Tenant\Model\Tenant;
use Fakture\User\Service\User;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Skeletor\Activity\Repository\ActivityRepository;
use Fakture\Client\Filter\Client;
use Fakture\Client\Repository\ClientRepository;
use Skeletor\User\Service\User as UserService;
use GuzzleHttp\Psr7\ServerRequest as Request;

class ClientServiceTest extends TestCase
{
    public function testSearchAprShouldReturnArray()
    {
        $clientRepoMock = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $activityMock = $this->getMockBuilder(ActivityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $filterMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userServiceMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientSearchMock = $this->getMockBuilder(ClientSearch::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['search'])
            ->getMock();
        $searchData = [[
            'vat' => 'test',
            'name' => 'test',
            'mb' => 'test',
            'address' => 'test',
            'city' => 'test',
            'country' => 'Srbija',
        ]];
        $clientSearchMock->expects(static::once())
            ->method('search')
            ->willReturn($searchData);
        $xchangeRateMock = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new \Fakture\Client\Service\Client($clientRepoMock, $userServiceMock, $loggerMock, $tenantRepoMock,
            $clientSearchMock,  $filterMock, $activityMock, $xchangeRateMock);
        $result = $service->searchApr('test');

        $this->assertSame($result, $searchData);
    }

    public function testCompileTableColumnsReturnArray()
    {
        $clientRepoMock = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $activityMock = $this->getMockBuilder(ActivityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $filterMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userServiceMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientSearchMock = $this->getMockBuilder(ClientSearch::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['search'])
            ->getMock();
        $xchangeRateMock = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new \Fakture\Client\Service\Client($clientRepoMock, $userServiceMock, $loggerMock, $tenantRepoMock,
            $clientSearchMock,  $filterMock, $activityMock, $xchangeRateMock);

        $this->assertIsArray($service->compileTableColumns());
    }

    public function testGetEntityDataShouldReturnArray()
    {
        $expected = [
            'clientVat' => '123123121',
            'clientId' => '12312312',
            'clientName' => 'test client',
            'clientAddress' => 'test address',
            'clientCity' => 'test city',
            'clientCountry' => 'Serbia',
            'contract' => [],
        ];
        $data = [
            'clientId' => 1,
            'name' => 'test client',
            'address' => 'test address',
            'email' => 'test email',
            'accountNumber' => 'test ',
            'bank' => 'test ',
            'tenant' => $this->createStub(Tenant::class),
            'logo' => 'test ',
            'city' => 'test city',
            'country' => 'Serbia',
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'createdAt' => '2022-01-01 00:00:00',
            'updatedAt' => '2022-01-01 00:00:00',
            'contractInfo' => []
        ];
        $clientStub = new \Fakture\Client\Model\Client(...$data);

        $clientRepoMock = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn($clientStub);
        $activityMock = $this->getMockBuilder(ActivityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $filterMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userServiceMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientSearchMock = $this->getMockBuilder(ClientSearch::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['search'])
            ->getMock();
        $xchangeRateMock = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new \Fakture\Client\Service\Client($clientRepoMock, $userServiceMock, $loggerMock, $tenantRepoMock,
            $clientSearchMock,  $filterMock, $activityMock, $xchangeRateMock);
        $result = $service->getEntityData(1);

        $this->assertSame($result, $expected);
    }

    public function testFetchTableDataReturnArray()
    {
        $data = [
            'clientId' => 1,
            'name' => 'test client',
            'address' => 'test address',
            'email' => 'test email',
            'accountNumber' => 'test ',
            'bank' => 'test ',
            'tenant' => $this->createStub(Tenant::class),
            'logo' => 'test ',
            'city' => 'test city',
            'country' => 'Serbia',
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'createdAt' => new \DateTime(),
            'updatedAt' => new \DateTime(),
            'contractInfo' => []
        ];
        $returnData = [
            'count' => 1,
            'entities' => [
                new \Fakture\Client\Model\Client(...$data)
            ]
        ];
        $clientRepoMock = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['fetchTableData'])
            ->getMock();
        $clientRepoMock->expects(static::once())
            ->method('fetchTableData')
            ->willReturn($returnData);
        $activityMock = $this->getMockBuilder(ActivityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $filterMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userServiceMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientSearchMock = $this->getMockBuilder(ClientSearch::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['search'])
            ->getMock();
        $xchangeRateMock = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new \Fakture\Client\Service\Client($clientRepoMock, $userServiceMock, $loggerMock, $tenantRepoMock,
            $clientSearchMock,  $filterMock, $activityMock, $xchangeRateMock);

        $this->assertIsArray($service->fetchTableData([], [], 0, 20, false));
    }

    public function testDeleteShouldFail()
    {
        $data = [
            'clientId' => 1,
            'name' => 'test client',
            'address' => 'test address',
            'email' => 'test email',
            'accountNumber' => 'test ',
            'bank' => 'test ',
            'tenant' => $this->createStub(Tenant::class),
            'logo' => 'test ',
            'city' => 'test city',
            'country' => 'Serbia',
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'createdAt' => new \DateTime(),
            'updatedAt' => new \DateTime(),
            'contractInfo' => []
        ];
        $clientStub = new \Fakture\Client\Model\Client(...$data);
        $clientRepoMock = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn($clientStub);
        $activityMock = $this->getMockBuilder(ActivityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $filterMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userServiceMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientSearchMock = $this->getMockBuilder(ClientSearch::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['search'])
            ->getMock();
        $xchangeRateMock = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new \Fakture\Client\Service\Client($clientRepoMock, $userServiceMock, $loggerMock, $tenantRepoMock,
            $clientSearchMock,  $filterMock, $activityMock, $xchangeRateMock);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Ne mozete obrisati sopstvenu firmu.');
        $service->delete(1);
    }

    public function testDeleteShouldNotFail()
    {
        $data = [
            'clientId' => 1,
            'name' => 'test client',
            'address' => 'test address',
            'email' => 'test email',
            'accountNumber' => 'test ',
            'bank' => 'test ',
            'tenant' => $this->createStub(Tenant::class),
            'logo' => 'test ',
            'city' => 'test city',
            'country' => 'Serbia',
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'createdAt' => new \DateTime(),
            'updatedAt' => new \DateTime(),
            'contractInfo' => []
        ];
        $clientStub = new \Fakture\Client\Model\Client(...$data);
        $clientRepoMock = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById', 'delete'])
            ->getMock();
        $clientRepoMock->expects(static::once())
            ->method('getById')
            ->willReturn($clientStub);
        $activityMock = $this->getMockBuilder(ActivityRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $filterMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userServiceMock = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isAdminLoggedIn', 'getLoggedInTenantId'])
            ->getMock();
        $userServiceMock->expects(static::once())
            ->method('getLoggedInTenantId')
            ->willReturn(5);
        $tenantRepoMock = $this->getMockBuilder(\Skeletor\Tenant\Repository\TenantRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getById'])
            ->getMock();
        $clientSearchMock = $this->getMockBuilder(ClientSearch::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['search'])
            ->getMock();
        $xchangeRateMock = $this->getMockBuilder(ExchangeRate::class)
            ->disableOriginalConstructor()
            ->getMock();

        $service = new \Fakture\Client\Service\Client($clientRepoMock, $userServiceMock, $loggerMock, $tenantRepoMock,
            $clientSearchMock,  $filterMock, $activityMock, $xchangeRateMock);
        $service->delete(1);
    }
}