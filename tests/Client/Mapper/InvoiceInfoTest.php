<?php
namespace Test\Fakture\Client\Mapper;

use Fakture\Client\Mapper\InvoiceInfo;
use PHPUnit\Framework\TestCase;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Mapper\PDOWrite;

class InvoiceInfoTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testGetByClientIdShouldReturnArray()
    {
        $stmtMock = $this->getMockBuilder(\PDOStatement::class)
            ->onlyMethods(['execute', 'bindValue', 'fetch'])
            ->getMock();
        $stmtMock->expects(static::once())
            ->method('execute')
            ->willReturn(true);
        $stmtMock->expects(static::once())
            ->method('fetch')
            ->willReturn(['test']);
        $pdoMock = $this->getMockBuilder(PDOWrite::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['prepare'])
            ->getMock();
        $pdoMock->expects(static::once())
            ->method('prepare')
            ->willReturn($stmtMock);

        $mapper = new InvoiceInfo($pdoMock);

        $this->assertSame(['test'], $mapper->getByClientId(1));
    }

    public function testGetByClientIdShouldThrowException()
    {
        $stmtMock = $this->getMockBuilder(\PDOStatement::class)
            ->onlyMethods(['execute', 'fetch'])
            ->getMock();
        $stmtMock->expects(static::once())
            ->method('execute')
            ->willReturn(true);
        $pdoMock = $this->getMockBuilder(PDOWrite::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['prepare'])
            ->getMock();
        $pdoMock->expects(static::once())
            ->method('prepare')
            ->willReturn($stmtMock);

        $mapper = new InvoiceInfo($pdoMock);

        $this->expectException(NotFoundException::class);
        $mapper->getByClientId(1);
    }
}