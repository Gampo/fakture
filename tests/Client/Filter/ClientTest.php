<?php
namespace Test\Fakture\Client\Filter;

use Fakture\Client\Filter\Client;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\ServerRequest as Request;
use Skeletor\Validator\ValidatorException;
use Volnix\CSRF\CSRF;

class ClientTest  extends TestCase
{
    public function testFilterShouldReturnData()
    {
        $csrfToken = CSRF::getTokenName();
        $data = [
            'clientId' => 1,
            'name' => 'test client',
            'address' => 'test address',
            'city' => 'test city',
            'country' => 'Serbia',
            'tenantId' => 1,
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'clientContract' => null,
            $csrfToken => ''
        ];
        $request = new Request('get', '', [], http_build_query($data));
        $request = $request->withParsedBody($data);

        $clientValidatorMock = $this->getMockBuilder(\Fakture\Client\Validator\Client::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isValid'])
            ->getMock();
        $clientValidatorMock->expects(static::once())
            ->method('isValid')
            ->willReturn(true);
        $filter = new Client($clientValidatorMock);
        unset($data[$csrfToken]);
        $this->assertSame($data, $filter->filter($request));
    }

    public function testFilterShouldThrowExceptionForInvalidData()
    {
        $csrfToken = CSRF::getTokenName();
        $data = [
            'clientId' => 1,
            'name' => 'ab',
            'address' => 'test address',
            'city' => 'test city',
            'country' => 'Serbia',
            'tenantId' => 1,
            'pib' => '123123121',
            'mb' => '12312312',
            'type' => \Fakture\Client\Model\Client::TYPE_LEGAL,
            'website' => 'http://www.example.com',
            'zip' => 11000,
            'note' => 'test note',
            'isActive' => 1,
            'clientContract' => null,
            $csrfToken => ''
        ];
        $request = new Request('get', '', [], http_build_query($data));
        $request = $request->withParsedBody($data);

        $clientValidatorMock = $this->getMockBuilder(\Fakture\Client\Validator\Client::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['isValid'])
            ->getMock();
        $clientValidatorMock->expects(static::once())
            ->method('isValid')
            ->willReturn(false);
        $filter = new Client($clientValidatorMock);
        unset($data[$csrfToken]);
        $this->expectException(ValidatorException::class);
        $this->assertSame([], $filter->getErrors()); // no errors actually set
        $filter->filter($request);
    }
}