<?php

namespace Test\Fakture\Stub;

class TemplateStub
{
    public static function createTemplateInstance()
    {
        $defaultTheme = APP_PATH . '/vendor/dj_avolak/skeletor/themes/admin';
        $theme = APP_PATH . '/themes/admin';
        $plates = new \League\Plates\Engine($theme);
        $plates->addFolder('defaultTheme', $defaultTheme, true);
        $plates->addFolder('layout', APP_PATH . '/themes/admin/layout');
        $plates->addFolder('partialsGlobal', APP_PATH . '/themes/admin/partials/global');
        $plates->registerFunction('printError', function($error, $label) use($plates) {
            return $plates->render('partialsGlobal::error', ['error' => $error, 'label' => $label]);
        });
        $plates->registerFunction('formToken', function () { return \Volnix\CSRF\CSRF::getHiddenInputString(); });
        $plates->registerFunction('t', function ($string) { return $string; });

        return $plates;
    }
}