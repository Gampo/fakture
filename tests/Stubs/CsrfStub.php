<?php

namespace Test\Fakture\Stub;

use Volnix\CSRF\CSRF;

class CsrfTrueStub extends CSRF {
    public static function validate($request_data = array(), $token_name = self::TOKEN_NAME) {
        return true;
    }
}
class CsrfFalseStub extends CSRF {
    public static function validate($request_data = array(), $token_name = self::TOKEN_NAME) {
        return false;
    }
}