<?php

namespace Test\Skeletor\Stub;

use Skeletor\User\Model\User;

class UserStub extends User
{
    public function __construct()
    {
        parent::__construct('1', '', '', '', '', '1', 1, '', '', '');
    }

    public function getEmail(): string
    {
        return 'test@example.org';
    }

    public function getPassword(): string
    {
        return password_hash('testtest', PASSWORD_BCRYPT);
    }
}