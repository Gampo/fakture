<?php

namespace Test\Skeletor\Stub;

use Skeletor\Page\Model\Page;

class PageStub extends Page
{
    public function __construct()
    {
        parent::__construct('1', 'title', 'test', 'src', '1', '1', 1, '');
    }

}