<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('APP_PATH', __DIR__ . '/..');
define('PUBLIC_PATH', __DIR__);
define('DATA_PATH', __DIR__ . '/../data');
define('IMAGES_PATH', __DIR__ . '/images');
define('ADMIN_ASSET_PATH', __DIR__ . '/assets/admin');
define('FRONT_ASSET_PATH', __DIR__ . '/assets/front');
define('FRONT_ASSET_URL', strstr(FRONT_ASSET_PATH,'/assets'));
define('ADMIN_ASSET_URL', strstr(ADMIN_ASSET_PATH,'/assets'));

include(__DIR__ . '/Stubs/CsrfStub.php');
include(__DIR__ . '/Stubs/TemplateStub.php');
//include(__DIR__ . '/Stubs/UserStub.php');
//include(__DIR__ . '/Stubs/PageStub.php');
//include(__DIR__ . '/Stubs/PdoWriteStub.php');

require __DIR__ . '/../vendor/autoload.php';