<?php

use Phinx\Migration\AbstractMigration;

final class CreateInvoiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `invoice` (
  `invoiceId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(12,2) NOT NULL,
  `description` varchar(512) NOT NULL,
  `clientName` varchar(128) NOT NULL,
  `clientAddress` varchar(128) NOT NULL,
  `clientCity` varchar(64) NOT NULL,
  `clientCountry` varchar(128) NOT NULL,
  `clientVat` varchar(16) NULL,
  `clientVatId` varchar(16) NOT NULL,
  `clientId` int(8) NOT NULL,
  `tenantId` int(8) NOT NULL,
  `templateId` int(8) NULL,
  `currency` varchar(8) NULL,
  `type` int(2) NULL,
  `recurring` int(1) NULL,
  `status` int(1) NULL,
  `issueLocation` varchar(64) NULL,
  `turnoverLocation` varchar(64) NULL,
  `deadlineDate` datetime NULL,
  `turnoverDate` datetime NULL,
  `issueDate` datetime NULL,
  `paidAt` datetime NULL,
  `sentAt` datetime NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `invoice`");
    }
}
