<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddInvoiceInfoToUser extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `user` 
    ADD COLUMN `invoiceAddress` varchar(128) NULL,
    ADD COLUMN `invoicePosition` varchar(128) NULL";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user` 
DROP COLUMN `invoiceAddress`,
DROP COLUMN `invoicePosition`";
        $this->query($sql);
    }
}
