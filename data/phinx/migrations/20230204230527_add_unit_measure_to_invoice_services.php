<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddUnitMeasureToInvoiceServices extends AbstractMigration
{
    public function up()
    {
        $this->query("ALTER TABLE `invoiceServices` ADD COLUMN `unitOfMeasure` varchar(16) DEFAULT 'H87';");
        $this->query("ALTER TABLE `invoiceRecurringServices` ADD COLUMN `unitOfMeasure` varchar(16) DEFAULT 'H87';");
    }

    public function down()
    {
        $this->query("ALTER TABLE `invoiceServices` DROP COLUMN `unitOfMeasure`;");
        $this->query("ALTER TABLE `invoiceRecurringServices` DROP COLUMN `unitOfMeasure`;");
    }
}
