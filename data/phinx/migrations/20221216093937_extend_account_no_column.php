<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ExtendAccountNoColumn extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `client` 
    MODIFY COLUMN `accountNumber` varchar(32);";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `client` 
MODIFY COLUMN `accountNumber` varchar(16);";
        $this->query($sql);
    }
}
