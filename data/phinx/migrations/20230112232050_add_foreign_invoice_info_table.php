<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddForeignInvoiceInfoTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `tenantForeignPaymentInstruction` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `tenantId` int NOT NULL,
  `currency` varchar(3) NOT NULL,
  `instruction` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `tenantForeignPaymentInstruction`");
    }
}
