<?php
use Phinx\Migration\AbstractMigration;

final class CreateExchangeRateTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `exchangeRates` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `currency` varchar(16) NOT NULL,
  `rate` varchar(16) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `exchangeRates`");
    }
}
