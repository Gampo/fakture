<?php
use Phinx\Migration\AbstractMigration;

final class AddInvoiceFieldsToUserTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `user` 
    ADD COLUMN `jmbg` varchar(64) NULL,
    ADD COLUMN `lk` varchar(64) NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user` 
DROP COLUMN `jmbg`,
DROP COLUMN `lk`;";
        $this->query($sql);
    }
}
