<?php

use Phinx\Migration\AbstractMigration;

final class CreateGroupTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `serviceGroup` (
  `serviceGroupId` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NULL,
  `tenantId` int(8) unsigned NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`serviceGroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `serviceGroup`");
    }
}
