<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateClientContractTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `clientContract` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `clientId` int unsigned NOT NULL,
  `tenantId` int unsigned NOT NULL,
  `serviceId` int unsigned NOT NULL,
  `deadline` int unsigned NULL,
  `priceId` int unsigned NULL,
  `number` varchar(32) NULL,
  `info` varchar(255) NULL,
  `signedAt` datetime NULL,
  `validUntil` datetime NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        $this->query("ALTER TABLE `clientContract` 
ADD UNIQUE INDEX `UNIQUE` (`clientId` ASC, `tenantId` ASC, `serviceId` ASC);");
    }

    public function down()
    {
        $this->query("DROP TABLE `clientContract`");
    }
}
