<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddInvoiceUserToInvoiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `invoice` ADD COLUMN `invoiceUser` int NULL;";
        $this->query($sql);

        $sql = "ALTER TABLE `invoiceRecurring` ADD COLUMN `invoiceUser` int NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `invoice` DROP COLUMN `invoiceUser`";
        $this->query($sql);
        $sql = "ALTER TABLE `invoiceRecurring` DROP COLUMN `invoiceUser`";
        $this->query($sql);
    }
}
