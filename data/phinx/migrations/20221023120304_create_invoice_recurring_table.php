<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateInvoiceRecurringTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `invoiceRecurring` (
  `invoiceRecurringId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `autoSend` int(1) NOT NULL,
  `autoSendStatus` int(1) NOT NULL,
  `autoSendAt` int(1) NOT NULL,
  `tenantId` int(12) NOT NULL,
  `autoGenerateAt` int(1) NOT NULL,
  `description` varchar(512) NOT NULL,
  `clientId` int(8) NOT NULL,
  `templateId` int(8) NULL,
  `currency` varchar(8) NULL,
  `type` int(2) NULL,
  `status` int(1) NULL,

  `issueLocation` varchar(64) NULL,
  `turnoverLocation` varchar(64) NULL,
  `deadlineDate` datetime NULL,
  `turnoverDate` datetime NULL,
  `issueDate` datetime NULL,

  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoiceRecurringId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `invoiceRecurring`");
    }
}
