<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddActiveFlagToClientTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `client` 
    ADD COLUMN `isActive` tinyint DEFAULT 1;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `client` 
DROP COLUMN `isActive`;";
        $this->query($sql);
    }
}
