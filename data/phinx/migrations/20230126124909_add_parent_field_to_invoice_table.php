<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddParentFieldToInvoiceTable extends AbstractMigration
{
    public function up()
    {
        $this->query("ALTER TABLE `invoice` ADD COLUMN `parentInvoice` int DEFAULT NULL;");
    }

    public function down()
    {
        $this->query("ALTER TABLE `invoice` DROP COLUMN `parentInvoice`;");
    }
}
