<?php

use Phinx\Migration\AbstractMigration;

final class CreateTemplateTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `invoiceTemplate` (
  `invoiceTemplateId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(32) NOT NULL,
  `template` text NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoiceTemplateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `invoiceTemplate`");
    }
}
