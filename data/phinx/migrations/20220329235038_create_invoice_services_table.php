<?php

use Phinx\Migration\AbstractMigration;

final class CreateInvoiceServicesTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `invoiceServices` (
  `invoiceServicesId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceId` int(8) NOT NULL,
  `serviceId` int(8) NOT NULL,
  `serviceName` varchar(256) NOT NULL,
  `price` decimal(14,2) NOT NULL,
  `qty` int(5) NOT NULL,
  `currency` varchar(3) NOT NULL,
  PRIMARY KEY (`invoiceServicesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `invoiceServices`");
    }
}
