<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class RemoveContractInfoFromClientTable extends AbstractMigration
{
    public function up()
    {
        $this->query("ALTER TABLE `client` DROP COLUMN `deadline`;");
        $this->query("ALTER TABLE `client` DROP COLUMN `contractInfo`;");
    }

    public function down()
    {
        $this->query("ALTER TABLE `client` ADD COLUMN `deadline` int;");
        $this->query("ALTER TABLE `client` ADD COLUMN `contractInfo` varchar(255);");
    }
}
