<?php
use Phinx\Migration\AbstractMigration;

final class AddWebsiteToClient extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `client` 
    ADD COLUMN `website` varchar(64) NULL,
    ADD COLUMN `email` varchar(64) NULL,
    ADD COLUMN `accountNumber` varchar(16) NULL,
    ADD COLUMN `logo` varchar(256) NULL,
    ADD COLUMN `invoiceZip` varchar(8) NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `client` 
DROP COLUMN `website`,
DROP COLUMN `logo`,
DROP COLUMN `accountNumber`,
DROP COLUMN `email`,
DROP COLUMN `invoiceZip`;";
        $this->query($sql);
    }
}
