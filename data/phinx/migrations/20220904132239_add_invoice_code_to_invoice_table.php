<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddInvoiceCodeToInvoiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `invoice` 
    ADD COLUMN `invoiceCode` varchar(32) NULL,
    ADD COLUMN `advanceAmount` decimal(14,2) NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `invoice` 
DROP COLUMN `invoiceCode`,
DROP COLUMN `advanceAmount`";
        $this->query($sql);
    }
}
