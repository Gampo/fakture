<?php

use Phinx\Migration\AbstractMigration;

final class CreateProjectTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `project` (
  `projectId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clientId` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(64) NOT NULL,
  `invoiceRate` VARCHAR (256) DEFAULT NULL,
  `invoiceDescription` VARCHAR (256) DEFAULT NULL,  
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `project`");
    }
}
