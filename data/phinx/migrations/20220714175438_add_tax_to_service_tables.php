<?php

use Phinx\Migration\AbstractMigration;

final class AddTaxToServiceTables extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `service` 
    ADD COLUMN `tax` int(2) DEFAULT 0;";
        $this->query($sql);
        $sql = "ALTER TABLE `invoiceServices` 
    ADD COLUMN `tax` int(2) DEFAULT 0;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `service` 
DROP COLUMN `tax`;";
        $this->query($sql);
        $sql = "ALTER TABLE `invoiceServices` 
    DROP COLUMN `tax`;";
        $this->query($sql);
    }
}
