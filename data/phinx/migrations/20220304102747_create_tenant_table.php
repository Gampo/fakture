<?php

use Phinx\Migration\AbstractMigration;

final class CreateTenantTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `tenant` (
  `tenantId` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` varchar(128) NULL,
  `settings` text NULL,
  `clientId` int NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tenantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        $sql = "INSERT INTO `tenant` (`name`) VALUES ('admin tenant');";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `tenant`");
    }
}
