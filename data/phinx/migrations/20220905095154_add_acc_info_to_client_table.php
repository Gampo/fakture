<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddAccInfoToClientTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `client` 
    ADD COLUMN `bank` varchar(32) NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `client` 
DROP COLUMN `bank`";
        $this->query($sql);
    }
}
