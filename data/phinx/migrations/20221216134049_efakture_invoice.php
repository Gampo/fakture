<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class EfaktureInvoice extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `efaktureInvoice` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `invoiceId` int unsigned NOT NULL,
  `sefInvoiceId` varchar(32) NOT NULL,
  `sefPurchaseInvoiceId` varchar(32) NOT NULL,
  `sefSalesInvoiceId` varchar(32) NOT NULL,
  `status` varchar(32) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `efaktureInvoice`");
    }
}
