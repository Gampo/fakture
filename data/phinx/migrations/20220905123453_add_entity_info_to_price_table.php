<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddEntityInfoToPriceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `price` 
    CHANGE `serviceId` `entityId` int(12) unsigned NOT NULL,
    ADD `entityType` int(1) unsigned NOT NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `price` 
    CHANGE `entityId` `serviceId` int(12) unsigned NOT NULL,
    DROP `entityType`;";
        $this->query($sql);
    }
}
