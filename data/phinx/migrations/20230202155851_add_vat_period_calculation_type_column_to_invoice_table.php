<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddVatPeriodCalculationTypeColumnToInvoiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `invoice` ADD COLUMN `vatPeriodCalculationType` int(2) NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `invoice` DROP COLUMN `vatPeriodCalculationType`";
        $this->query($sql);
    }
}
