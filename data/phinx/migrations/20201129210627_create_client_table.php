<?php

use Phinx\Migration\AbstractMigration;

final class CreateClientTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `client` (
    `clientId` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(128) NOT NULL,
    `tenantId` int(8) unsigned NOT NULL,
    `invoiceName` VARCHAR (256) DEFAULT NULL,
    `invoiceAddress` VARCHAR (256) DEFAULT NULL,
    `invoiceCity` VARCHAR (256) DEFAULT NULL,
    `invoiceCountry` VARCHAR (256) DEFAULT NULL,
    `pib` VARCHAR (32) DEFAULT NULL,
    `mb` VARCHAR (32) DEFAULT NULL,
    `type` int(1) DEFAULT NULL,
    `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `client`");
    }
}
