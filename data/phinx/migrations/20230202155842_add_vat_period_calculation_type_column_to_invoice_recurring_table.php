<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddVatPeriodCalculationTypeColumnToInvoiceRecurringTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `invoiceRecurring` ADD COLUMN `vatPeriodCalculationType` int(2) NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `invoiceRecurring` DROP COLUMN `vatPeriodCalculationType`";
        $this->query($sql);
    }
}
