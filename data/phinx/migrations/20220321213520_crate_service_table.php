<?php

use Phinx\Migration\AbstractMigration;

final class CrateServiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `service` (
  `serviceId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `type` int(1) NULL,
  `groupId` int(6) unsigned NULL,
  `tenantId` int(8) unsigned NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`serviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `service`");
    }
}
