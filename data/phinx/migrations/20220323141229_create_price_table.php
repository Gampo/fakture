<?php

use Phinx\Migration\AbstractMigration;

final class CreatePriceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `price` (
  `priceId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `currency` varchar(3) NOT NULL,
  `amount` decimal(16,2) NOT NULL,
  `description` text NULL,
  `tenantId` int(8) unsigned NOT NULL,
  `serviceId` int(8) unsigned NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`priceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `price`");
    }
}
