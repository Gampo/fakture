<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateInvoiceRecurringServicesTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `invoiceRecurringServices` (
  `invoiceRecurringServicesId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceRecurringId` int(8) NOT NULL,
  `serviceId` int(8) NOT NULL,
  `serviceName` varchar(256) NOT NULL,
  `price` decimal(14,2) NOT NULL,
  `qty` int(5) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `tax` int(2) DEFAULT '0',
  PRIMARY KEY (`invoiceRecurringServicesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `invoiceRecurringServices`");
    }
}
