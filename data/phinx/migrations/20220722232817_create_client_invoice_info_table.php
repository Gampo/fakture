<?php

use Phinx\Migration\AbstractMigration;

final class CreateClientInvoiceInfoTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `client` 
    DROP COLUMN `invoiceName`,
    CHANGE `invoiceAddress` `address` varchar(255),
    CHANGE `invoiceCity` `city` varchar(64),
    CHANGE `invoiceCountry` `country` varchar(64),
    CHANGE `invoiceZip` `zip` varchar(16),
    ADD COLUMN `contractInfo` VARCHAR(256) DEFAULT NULL,
    ADD COLUMN `deadline` int(3) DEFAULT NULL,
    ADD COLUMN `note` varchar(255) DEFAULT NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `client` 
    ADD COLUMN `invoiceName` varchar(255),
    CHANGE `address` `invoiceAddress` varchar(255),
    CHANGE `city` `invoiceCity` varchar(64),
    CHANGE `country` `invoiceCountry` varchar(64),
    CHANGE `zip` `invoiceZip` int(6),
    DROP COLUMN `contractInfo`,
    DROP COLUMN `deadline`,
    DROP COLUMN `note`;";
        $this->query($sql);
    }
}