<?php
/* @var \Fakture\Tenant\Model\Tenant $tenant */
if ($data['model']):
    $clientId = $data['model']->getId(); ?>
    <h1 class="page-header"><?= $this->t('Edit client:') ?> <?= $data['model']->getName() ?></h1>
<?php
else:
    $clientId = 'null'; ?>
    <h1 class="page-header extraSpace"><?= $this->t('Create new client') ?></h1>
<?php
endif; ?>
<form action="/client/<?= ($data['model']) ? 'update/' . $clientId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="clientForm" autocomplete="off">
    <?= $this->formToken(); ?>
    <div class="row">
        <div class="col-lg-4">
            <!--<h4 class="page-header extraSpace"><?= $this->t('Basic info') ?></h4> -->
            <?php if (!$data['model']): ?>
            <div id="aprSearchContainer" class="searchContainer">
                <label for="entitySearchInput" class="userLabel"><?= $this->t('Search the APR') ?></label>
                <div id="searchInputContainer">
                    <input type="text" id="entitySearchInput" class="form-control">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                </div>
                <div id="entityOptions" class="hidden"></div>
            </div>
            <?php endif; ?>
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Name') ?></label>
                <input  name="name" value="<?= $data['model']?->getName() ?>" class="form-control inputDesign formInput"
               <?php if (!$data['model']): ?>data-validation="length" data-validation-length="min6"
                   data-validation-error-msg="Unesite ime."<?php endif ?> />
            </div>

            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Homepage') ?></label>
                <input placeholder="http(s)://example.com" name="website" value="<?= $data['model']?->getWebsite() ?>" class="form-control inputDesign formInput"/>
            </div>
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Email') ?></label>
                <input type="email" name="email" value="<?= $data['model']?->getEmail() ?>" class="form-control inputDesign formInput"/>
                
                <!--<label class="userLabel"><?= $this->t('Note') ?></label>-->
                <input type="hidden" name="note" value="<?= $data['model']?->getNote() ?>" class="form-control inputDesign formInput"/>
            </div>
        <?php if ($loggedInRole === 1): ?>
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Tenant') ?></label>
                <select class="form-control selectDesign formInput" name="tenantId">
                <?php foreach ($data['tenants'] as $tenant): ?>
                        <option value="<?= $tenant->getId() ?>" <?php
                        if ($data['model']?->getTenant()?->getId() === $tenant->getId()) {
                            echo 'selected';
                        } ?>
                        ><?= $tenant->getName() ?></option>
                <?php endforeach; ?>
                </select>
            </div>
        <?php else: ?>
            <?php if (!$data['model']): ?>
                <input type="hidden" name="tenantId" value="<?= $data['loggedInTenantId'] ?>"/>
            <?php else: ?>
                <input type="hidden" name="tenantId" value="<?= $data['model']->getTenant()->getId() ?>"/>
            <?php endif; ?>
        <?php endif; ?>
        </div>
        <div class="col-lg-4">
            <!--<h4 class="page-header extraSpace"><?= $this->t('Invoice info') ?></h4> -->

            <div class="form-group divUser">
                <label for="clientType" class="userLabel"><?= $this->t('Type') ?></label>
                <select id="clientType" class="form-control formInput" name="type" >
                    <?php
                    foreach(\Fakture\Client\Model\Client::getHrTypes() as $id => $type):?>
                        <option value="<?= $id ?>" <?php
                        if ($data['model']?->getType() === $id) {
                            echo 'selected';
                        } ?>
                        ><?= $type ?></option>
                    <?php
                    endforeach; ?>
                </select>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group divUser<?= $data['model']?->getType() === 2 ? ' hidden' : ''?>">
                        <label for="pib" class="userLabel"><?= $this->t('PIB') ?></label>
                        <input id="pib" name="pib" value="<?= $data['model']?->getPib() ?>" class="form-control inputDesign <?= $data['model']?->getType() === 2 ? ' hidden' : ''?>"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group divUser">
                    <?php $isCompany = $data['model']?->getType() === 1 || !$data['model']?->getType(); ?>
                        <label id="companyIdLabel" class="userLabel companyId <?= !$isCompany ? ' hidden' : ''?>"><?= $this->t('MB') ?></label>
                        <label id="personalIdLabel" class="userLabel personalId <?= $isCompany ? ' hidden' : ''?>"><?= $this->t('JMBG') ?></label>
                        <input id="mb" name="mb" value="<?= $data['model']?->getMb() ?>" class="form-control inputDesign"/>
                    </div>
                </div>
            </div>
            <div class="form-group divUser">
                <label for="isActive" class="userLabel"><?= $this->t('Active') ?></label>
                <select id="isActive" class="form-control formInput" name="isActive" >
                    <option value="1" <?php if ($data['model']?->isActive()) { echo 'selected';} ?>>Da</option>
                    <option value="0" <?php if (!$data['model']?->isActive()) { echo 'selected';} ?>>Ne</option>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Address') ?></label>
                <input name="address" value="<?= $data['model']?->getAddress() ?>" class="form-control inputDesign formInput"/>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group divUser">
                        <label class="userLabel"><?= $this->t('City') ?></label>
                        <input name="city" value="<?= $data['model']?->getCity() ?>" class="form-control inputDesign formInput"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group divUser">
                        <label class="userLabel"><?= $this->t('Zip') ?></label>
                        <input name="zip" value="<?= $data['model']?->getZip() ?>" class="form-control inputDesign formInput"/>
                    </div>
                </div>
            </div>
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Country') ?></label>
                <input name="country" value="<?= $data['model']?->getCountry() ?>" class="form-control inputDesign formInput"/>
            </div>
        </div>
    </div>

    <h3><?= $this->t('Contract Information') ?></h3>
    <div>
        <div id="addContractInformation" title="<?= $this->t('Add Contract Information') ?>">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"/>
            </svg>
        </div>
    </div>
    <div id="contractInformationContainer">
    <?php if($data['model']):?>
    <?php foreach ($data['model']->getContractInfo() as $contract): ?>
        <div class="clientContract">
            <div>
                <label><?=$this->t('Service')?></label>
                <select class="form-control clientContractServiceSelect" name="clientContract[serviceId][]">
                <?php foreach($data['services'] as $service): ?>
                    <option value="<?=$service['id']?>" <?=($service['id'] == $contract->getService()->getId()) ? 'selected':''; ?>><?=$service['name']?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <div>
                <label><?=$this->t('Deadline')?></label>
                <input class="form-control clientContractDeadlineInput" type="number" name="clientContract[deadline][]"
                    value="<?=$contract->getDeadline()?>" />
            </div>
            <div class="clientContractInfoContainer">
                <label><?=$this->t('Number')?></label>
                <input class="form-control clientContractNumber" type="text" name="clientContract[number][]"
                   value="<?=$contract->getNumber()?>" />
            </div>
            <div class="clientContractInfoContainer">
                <label><?=$this->t('Invoice Info')?></label>
                <input class="form-control" type="text" name="clientContract[info][]"
                   value="<?=$contract->getInfo()?>" />
            </div>
            <div>
                <label><?=$this->t('Price')?></label>
                <input class="form-control clientContractPriceInput" type="text" name="clientContract[price][amount][]"
                   value="<?=$contract->getPrice()->getAmount()?>" />
                <select class="form-control clientContractPriceCurrencySelect" name="clientContract[price][currency][]">
                <?php foreach (\Fakture\Price\Model\Price::getHrCurrencies() as $id => $currency): ?>
                    <option value="<?= $id ?>"
                        <?= ($id == $contract->getPrice()->getCurrency()) ? 'selected' : ''; ?>><?= strtoupper(
                            $currency
                        ) ?></option>
                <?php endforeach; ?>
                </select>
                <input type="hidden" name="clientContract[price][priceId][]" value="<?=$contract->getPrice()->getId()?>" />
            </div>
            <input type="hidden" name="clientContract[id][]" value="<?=$contract->getId()?>" />
            <div class="deleteClientContract" title="<?=$this->t('Remove')?>">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path d="M576 128c0-35.3-28.7-64-64-64H205.3c-17 0-33.3 6.7-45.3 18.7L9.4 233.4c-6 6-9.4 14.1-9.4 22.6s3.4 16.6 9.4 22.6L160 429.3c12 12 28.3 18.7 45.3 18.7H512c35.3 0 64-28.7 64-64V128zM271 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z"/>
                </svg>
            </div>
        </div>
    <?php endforeach?>
    <?php endif;?>
    </div>
    <div class="form-group">
        <input type="hidden" name="clientId" value="<?= $clientId ?>"/>
        <input data-action="/client/<?= ($data['model']) ? 'update/' . $clientId : 'create' ?>/" id="submitButton" type="submit" value="<?= $this->t('Save') ?>"
               class="btn btn-success submitButtonSize"/>
    </div>
    <template id="clientContractTemplate">
        <div class="clientContract">
            <div>
                <label><?=$this->t('Service')?></label>
                <select class="form-control clientContractServiceSelect" name="clientContract[serviceId][]">
                    <option value="-1"><?=$this->t('Select a service')?></option>
                    <?php foreach($data['services'] as $service): ?>
                        <option value="<?=$service['id']?>"><?=$service['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div>
                <label><?=$this->t('Deadline')?></label>
                <input class="form-control clientContractDeadlineInput" type="number" name="clientContract[deadline][]">
            </div>
            <div class="clientContractInfoContainer">
                <label><?=$this->t('Number')?></label>
                <input class="form-control" type="text" name="clientContract[number][]">
            </div>
            <div class="clientContractInfoContainer">
                <label><?=$this->t('Invoice Info')?></label>
                <input class="form-control" type="text" name="clientContract[info][]">
            </div>
            <div>
                <label><?=$this->t('Price')?></label>
                <input class="form-control clientContractPriceInput" type="text" name="clientContract[price][amount][]">
                <select class="form-control clientContractPriceCurrencySelect" name="clientContract[price][currency][]">
                    <?php foreach (\Fakture\Price\Model\Price::getHrCurrencies() as $currency): ?>
                        <option value="<?= $currency ?>"><?= strtoupper($currency) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="deleteClientContract" title="<?=$this->t('Remove')?>">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path d="M576 128c0-35.3-28.7-64-64-64H205.3c-17 0-33.3 6.7-45.3 18.7L9.4 233.4c-6 6-9.4 14.1-9.4 22.6s3.4 16.6 9.4 22.6L160 429.3c12 12 28.3 18.7 45.3 18.7H512c35.3 0 64-28.7 64-64V128zM271 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z"/>
                </svg>
            </div>
        </div>
    </template>
</form>