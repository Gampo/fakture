<?php $this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=$this->t('Template list:')?></h1>
    </div>
</div>

<div class="row biggerBottonMargin">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <!-- /.panel-heading -->
            <div class="panel-body panel-bodyWhite">
                <a href="/template/form/" title="Create new"><?=$this->t('Create new')?></a>
                <table class="table table-striped table-bordered table-hover" id="templatesTable">
                    <thead>
                    <tr>
                        <th><?=$this->t('Label')?></th>
                        <th><?=$this->t('Date created')?></th>
                        <th><?=$this->t('Date updated')?></th>
                        <th><?=$this->t('Action')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /* @var \Fakture\Invoice\Model\Template $template */
                    foreach ($data['models'] as $template): ?>
                    <tr>
                        <td><a href="/template/form/<?=$template->getId()?>/"><?=$template->getLabel()?></a></td>
                        <td><?=$template->getCreatedAt()->format('d/m/Y H:i')?></td>
                        <td><?=$template->getUpdatedAt()->format('d/m/Y H:i')?></td>
                        <td>
                            <a href="/template/delete/<?=$template->getId()?>/" class="delete"><?=$this->t('Delete')?></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
