<?php
/* @var \Fakture\Invoice\Model\Template $template */
$this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']):
            $templateId = $data['model']->getId(); ?>
            <h1 class="page-header"><?=$this->t('Edit template:')?> <?=$data['model']->getName()?></h1>
        <?php else:
            $templateId = 'null'; ?>
            <h1 class="page-header"><?=$this->t('Create template')?></h1>
        <?php endif;?>
    </div>
</div>
<form action="/template/<?= ($data['model']) ? 'update/' . $templateId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="templateForm" autocomplete="off">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <h4><?=$this->t('Basic Info')?></h4>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Label')?></label>
                                <input name="label" value="<?=$data['model']?->getLabel() ?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Template')?></label>
                                <input name="template" value="<?=$data['model']?->getTemplate()?>" class="form-control inputDesign" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <input type="hidden" name="invoiceTemplateId" value="<?=$templateId?>" />
                                <input type="submit" value="<?=$this->t('Save')?>" class="btn btn-success submitButtonSize" id="submitButton" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.row -->
    </div>
</form>
