<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-text mx-3">Gampo</div>
    </a>

<?php if($loggedIn): ?>
    <!-- Heading -->
    <div class="sidebar-heading"><?=$this->t('Dashboard')?></div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/invoice/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Invoice')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/invoice-recurring/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Automatic Invoice')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/client/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Clients')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/service/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Services')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/service-group/view/">
            <i class="fas fa-cogs"></i>
            <span><?=$this->t('Groups')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/template/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Templates')?></span>
        </a>
    </li>

    <?php if($tenantId > 0): ?>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/tenant/form/<?=$tenantId?>/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('My details')?></span>
        </a>
    </li>
    <?php endif; ?>

    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <?php if($loggedInRole === \Skeletor\User\Model\User::ROLE_ADMIN): ?>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/tenant/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Tenants')?></span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/user/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Users')?></span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/activity/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Activities')?></span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/translator/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Translations')?></span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <?php endif; ?>

<?php endif; ?>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>