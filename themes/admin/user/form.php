<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */
/* @var \Fakture\Tenant\Model\Tenant $tenant */

$this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
    <?php if ($data['user']):?>
        <?php if ($loggedInRole === 1): ?>
        <h1 class="page-header"><?=$this->t('Edit user:')?> <?=$data['user']->getDisplayName()?></h1>
        <?php else: ?>
        <h1 class="page-header"><?=$this->t('Edit profile:')?></h1>
        <?php endif; ?>
    <?php else: ?>
        <h1 class="page-header"><?=$this->t('Create user:')?></h1>
    <?php endif; ?>
    </div>
</div>
<form action="/user/<?= ($data['user']) ? 'update' : 'create' ?>/<?php if ($data['user']) { echo $data['user']->getId() .'/'; } ?>"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Email:')?></label>
                                <input name="email" value="<?=$data['user']?->getEmail() ?>" class="form-control inputDesign"
                                <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                   data-validation-error-msg="Enter email."<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('First name:')?></label>
                                <input name="firstName" value="<?=$data['user']?->getFirstName()?>" class="form-control inputDesign"
                                <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                   data-validation-error-msg="Enter first name."<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Last name:')?></label>
                                <input name="lastName" value="<?=$data['user']?->getLastName()?>" class="form-control inputDesign"
                                <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                   data-validation-error-msg="Enter last name."<?php endif?> />
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Display name:')?></label>
                                <input name="displayName" value="<?=$data['user']?->getDisplayName()?>" class="form-control inputDesign"
                                       <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                       data-validation-error-msg="Enter display name."<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Password:')?></label>
                                <input name="password" value="" class="form-control inputDesign" type="password"
                                       <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                       data-validation-error-msg="<?=$this->t('Password:')?>"<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Repeat password:')?></label>
                                <input name="password2" value="" class="form-control inputDesign" type="password"
                                       <?php if (!$data['user']): ?>data-validation="confirmation" data-validation-confirm="password" data-validation-error-msg="<?=$this->t("Password doesn't match.")?>"<?php endif?> />
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('JMBG:')?></label>
                                <input name="jmbg" value="<?=$data['user']?->getJmbg()?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Address:')?></label>
                                <input name="invoiceAddress" value="<?=$data['user']?->getInvoiceAddress()?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Br. l.k.:')?></label>
                                <input name="lk" value="<?=$data['user']?->getLk()?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Pozicija:')?></label>
                                <input name="invoicePosition" value="<?=$data['user']?->getInvoicePosition()?>" class="form-control inputDesign" />
                            </div>
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                        <div class="col-lg-3">
                            <?php if ($loggedInRole === 1): ?>
                            <h4><?=$this->t('Additional information')?></h4>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Role')?></label>
                                <select class="form-control selectDesign userRole" name="role" required data-validation="required"
                                        data-validation-error-msg="Select type of user.">
                                    <?php foreach (\Fakture\User\Model\User::getHrTypes() as $id => $role): ?>
                                    <option value="<?=$id?>"
                                        <?php if ($id == $data['user']?->getRole()) { echo 'selected'; }?>><?=$role?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Status')?></label>
                                <select class="form-control selectDesign" name="isActive">
                                    <option value="1" <?php if ($data['user']?->getIsActive()) { echo 'selected'; }?>
                                    ><?=$this->t('Active')?></option>
                                    <option value="0" <?php if (!$data['user']?->getIsActive()) { echo 'selected'; }?>
                                    ><?=$this->t('Inactive')?></option>
                                </select>
                            </div>
                                <div class="form-group divUser">
                                    <label class="userLabel"><?=$this->t('Tenant')?></label>
                                    <select class="form-control selectDesign" name="tenantId">
                                    <?php foreach ($data['tenants'] as $tenant): ?>
                                        <option value="<?=$tenant->getId()?>" <?php if ($data['user']?->getTenant()->getId() === $tenant->getId()) { echo 'selected'; }?>
                                        ><?=$tenant->getName()?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php else:?>
                                <input type="hidden" name="tenantId" value="<?=$data['user']?->getTenant()->getId()?>" />
                                <input type="hidden" name="role" value="<?=$data['user']?->getRole()?>" />
                                <input type="hidden" name="isActive" value="<?=$data['user']?->getIsActive()?>" />

                            <?php endif;?>

                            <input type="hidden" name="userId" value="<?=$data['user']?->getId()?>" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row" style="float: left;">
                            <div class="form-group">
                                <input type="submit" value="<?=$this->t('Submit')?>" class="btn btn-success submitButtonSize" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.row -->
    </div>
</form>