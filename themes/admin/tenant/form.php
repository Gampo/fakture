<?php $this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']):
            $tenantId = $data['model']->getId(); ?>
        <h1 class="page-header"><?=$this->t('Edit tenant:')?> <?=$data['model']->getName()?></h1>
        <?php else:
        $tenantId = 'null'; ?>
        <h1 class="page-header"><?=$this->t('Create tenant')?></h1>
        <?php endif;?>
    </div>
</div>
<form action="/tenant/<?= ($data['model']) ? 'update/' . $tenantId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?= $this->formToken(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Name')?></label>
                                <input name="name" value="<?=$data['model']?->getName() ?>" class="form-control inputDesign"
                                       <?php if (!$data['model']): ?>data-validation="length" data-validation-length="min6"
                                       data-validation-error-msg="Enter display name."<?php endif?> />
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Description')?></label>
                                <input name="description" value="<?=$data['model']?->getDescription()?>" class="form-control inputDesign" />
                            </div>
                        </div>
                        <div class="col-lg-3">
<!--                            <h4 class="page-header"><?=$this->t('Import settings')?></h4> -->
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('SEF ApiKey')?></label>
                                <input name="settings[sefApiKey]" value="<?=$data['model']?->getSettings()->getSefApiKey()?>" class="form-control inputDesign" />
                            </div>
                            <div class="form-group divUser">
                                <label class="customStartingNumber"><?=$this->t('Custom starting number (last number from old system)')?></label>
                                <input type="number" name="settings[customStartingNumber]" value="<?=$data['model']?->getSettings()->getCustomStartingNumber()?>" class="form-control inputDesign" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <h4 class="page-header"><?=$this->t('App settings')?></h4>
                            <div class="form-group divUser">
                                <label for="useForeignInvoice" class="userLabel"><?=$this->t('Use foreign invoices ?')?></label>
                                <input id="useForeignInvoice" type="checkbox" name="settings[useForeignInvoice]" <?php if ($data['model']?->getSettings()->useForeignInvoice()) { echo 'checked'; } ?> />

                                <label for="inVatSystem" class="userLabel"><?=$this->t('Is in Vat system ?')?></label>
                                <input id="inVatSystem" type="checkbox" name="settings[inVatSystem]" <?php if ($data['model']?->getSettings()->isInVatSystem()) { echo 'checked'; } ?> />

                                <label for="useSef" class="userLabel"><?=$this->t('Use SEF service ?')?></label>
                                <input id="useSef" type="checkbox" name="settings[useSef]" <?php if ($data['model']?->getSettings()->useSef()) { echo 'checked'; } ?> />

                                <label for="useSefForForeign" class="userLabel"><?=$this->t('Use SEF for foreign invoice ?')?></label>
                                <input id="useSefForForeign" type="checkbox" name="settings[useSefForForeign]" <?php if ($data['model']?->getSettings()->useSefForForeign()) { echo 'checked'; } ?> />

                                <label for="useSefToManageInput" class="userLabel"><?=$this->t('Use SEF service to manage input invoices ?')?></label>
                                <input id="useSefToManageInput" type="checkbox" name="settings[useSefToManageInput]"  <?php if ($data['model']?->getSettings()->useSefToManageInput()) { echo 'checked'; } ?> />
                                <br />

                                <label for="invoiceCodePattern" class="userLabel"><?=$this->t('Select invoice code pattern')?>:</label>
                                <select class="form-control" id="invoiceCodePattern" name="settings[invoiceCodePattern]">
                                    <option <?php if ($data['model']?->getSettings()->getInvoiceCodePattern() === 'invoiceCode-year') {echo "selected";}?> value="invoiceCode-year"><?=$this->t('invoiceCode/year (e.g. 123/2022 )')?></option>
                                </select><br />

                                <label for="foreignInvoiceCodePattern" class="userLabel"><?=$this->t('Select foreign invoice code pattern')?>:</label>
                                <select class="form-control" id="foreignInvoiceCodePattern" name="settings[foreignInvoiceCodePattern]">
                                    <option <?php if ($data['model']?->getSettings()->getForeignInvoiceCodePattern() === 'IF-invoiceCode-year') {echo "selected";}?> value="IF-invoiceCode-year"><?=$this->t('IF-invoiceCode/year (e.g. IF-123/2022)')?></option>
                                </select><br />

                                <label for="filenamePattern" class="userLabel"><?=$this->t('Select pdf filename pattern')?>:</label>
                                <select class="form-control" id="filenamePattern" name="settings[filenamePattern]">
                                    <option <?php if ($data['model']?->getSettings()->getFilenamePattern() === 'invoiceCode-clientName') {echo "selected";}?> value="invoiceCode-clientName"><?=$this->t('invoiceCode - clientName.pdf (e.g. "123/2022 - google.pdf")')?></option>
                                    <option <?php if ($data['model']?->getSettings()->getFilenamePattern() === 'invoiceCode') {echo "selected";}?> value="invoiceCode"><?=$this->t('invoiceCode.pdf (e.g. 123/2022.pdf)')?></option>
                                </select>

                                <label class="userLabel"><?=$this->t('VAT exemption')?></label>
                                <select class="form-control" name="settings[vatExemption]">
                                    <option style="background-color: #D3D3D3;color:white;" selected disabled value=""><?=$this->t('Choose a reason for VAT exemption')?></option>
                                    <option <?php if ($data['model']?->getSettings()->getVatExemption() === 'PDV-RS-33') {echo "selected";}?> value="PDV-RS-33">PDV-RS-33</option>
                                    <option <?php if ($data['model']?->getSettings()->getVatExemption() === 'PDV-RS-35-7') {echo "selected";}?> value="PDV-RS-35-7">PDV-RS-35-7</option>
                                    <option <?php if ($data['model']?->getSettings()->getVatExemption() === 'PDV-RS-36-5') {echo "selected";}?> value="PDV-RS-36-5">PDV-RS-36-5</option>
                                    <option <?php if ($data['model']?->getSettings()->getVatExemption() === 'PDV-RS-36b-4') {echo "selected";}?> value="PDV-RS-33">PDV-RS-36b-4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <h2 class="page-header"><?=$this->t('Detalji moje firme')?></h2>
                            <div class="form-group divUser">
                                <input placeholder="pretrazi na APR-u" type="text" class="form-control" id="aprSearch" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Logo')?></label>
                                <?php if (strlen($data['tenantClient']?->getLogo())) {
                                    $url = sprintf('/images/%s/%s', $data['model']->getId(), $data['tenantClient']?->getLogo());
                                    echo '<img height="80" src="'.$url.'" />';
                                }?>
                                <input type="file" name="client[logo]" value="<?=$data['tenantClient']?->getLogo() ?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Website URL')?></label>
                                <input placeholder="[http(s)://www.]example.com" type="text" name="client[website]" value="<?=$data['tenantClient']?->getWebsite() ?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('account number')?></label>
                                <input name="client[accountNumber]" value="<?=$data['tenantClient']?->getAccountNumber() ?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('U Banci (unesite ime kako želite da se vidi na računu)')?></label>
                                <input name="client[bank]" value="<?=$data['tenantClient']?->getBank() ?>" class="form-control inputDesign" />
                            </div>

                        </div>
                        <div class="col-lg-3">
                            <h3 class="page-header"><?=$this->t('Invoice info')?></h3>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Name')?></label>
                                <input name="client[name]" value="<?=$data['tenantClient']?->getName()?>" class="form-control inputDesign" />
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Address')?></label>
                                <input name="client[address]" value="<?=$data['tenantClient']?->getAddress()?>" class="form-control inputDesign" />
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('City')?></label>
                                <input name="client[city]" value="<?=$data['tenantClient']?->getCity()?>" class="form-control inputDesign" />
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Zip')?></label>
                                <input name="client[zip]" value="<?=$data['tenantClient']?->getZip() ?>" class="form-control inputDesign" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <h2 class="page-header">&nbsp;</h2>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Country')?></label>
                                <input name="client[country]" value="<?=$data['tenantClient']?->getCountry()?>" class="form-control inputDesign" />
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('PIB')?></label>
                                <input name="client[pib]" value="<?=$data['tenantClient']?->getPib()?>" class="form-control inputDesign" />
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel companyId"><?=$this->t('MB')?></label>
                                <input name="client[mb]" value="<?=$data['tenantClient']?->getMb()?>" class="form-control inputDesign" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group divUser foreignInstructionsContainer">
                        <label class="userLabel"><?=$this->t('Foreign invoice instructions')?></label>
                        <div>
                            <div id="addInvoiceInstruction" title="<?= $this->t('Add Invoice Instruction') ?>">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                    <path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"/>
                                </svg>
                            </div>
                        </div>
                        <div id="invoiceInstructionContainer">
                            <?php if($data['model']):?>
                                <?php foreach ($data['model']->getInvoiceInstructions() as $instruction): ?>
                                    <div class="invoiceInstruction">
                                        <div>
                                            <label><?=$this->t('Currency')?></label>
                                            <select class="form-control invoiceInstructionSelect" name="invoiceInstruction[currency][]">
                                                <option value="eur" data-old-value="eur" <?=('eur' == $instruction->getCurrency()) ? 'selected':''; ?>>EUR</option>
                                                <option value="usd" data-old-value="usd" <?=('usd' == $instruction->getCurrency()) ? 'selected':''; ?>>USD</option>
                                                <option value="gbp" data-old-value="gbp" <?=('gbp' == $instruction->getCurrency()) ? 'selected':''; ?>>GBP</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label><?=$this->t('Instruction')?></label>
                                            <textarea class="instruction"><?=$instruction->getInstruction()?></textarea>
                                        </div>
                                        <input type="hidden" name="invoiceInstruction[instruction][]" value="<?=$instruction->getInstruction()?>" />
                                        <input type="hidden" name="invoiceInstruction[id][]" value="<?=$instruction->getId()?>" />
                                        <div class="deleteInvoiceInstruction" title="<?=$this->t('Remove')?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                <path d="M576 128c0-35.3-28.7-64-64-64H205.3c-17 0-33.3 6.7-45.3 18.7L9.4 233.4c-6 6-9.4 14.1-9.4 22.6s3.4 16.6 9.4 22.6L160 429.3c12 12 28.3 18.7 45.3 18.7H512c35.3 0 64-28.7 64-64V128zM271 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z"/>
                                            </svg>
                                        </div>
                                    </div>
                                <?php endforeach?>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <input type="hidden" name="client[type]" value="1" />
                                <input type="hidden" name="client[clientId]" value="<?=$data['tenantClient']?->getId()?>"/>
                                <input type="hidden" name="tenantId" value="<?=$tenantId?>"/>
                                <input type="submit" value="<?=$this->t('Save')?>" class="btn btn-success submitButtonSize"/>
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.row -->
    </div>
    <template id="invoiceInstructionTemplate">
        <div class="invoiceInstruction">
            <div>
                <label><?=$this->t('Currency')?></label>
                <select class="form-control invoiceInstructionSelect" name="invoiceInstruction[currency][]">
                    <option value="eur">EUR</option>
                    <option value="usd">USD</option>
                    <option value="gbp">GBP</option>
                </select>
            </div>
            <div>
                <label><?=$this->t('Instruction')?></label>
                <textarea class="instruction" name="invoiceInstruction[instruction][]"></textarea>
            </div>
            <input type="hidden" name="invoiceInstruction[id][]"/>
            <div class="deleteInvoiceInstruction" title="<?=$this->t('Remove')?>">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                    <path d="M576 128c0-35.3-28.7-64-64-64H205.3c-17 0-33.3 6.7-45.3 18.7L9.4 233.4c-6 6-9.4 14.1-9.4 22.6s3.4 16.6 9.4 22.6L160 429.3c12 12 28.3 18.7 45.3 18.7H512c35.3 0 64-28.7 64-64V128zM271 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z"/>
                </svg>
            </div>
        </div>
    </template>
</form>
<script src="<?=ADMIN_ASSET_URL .'/js/gfComponents/pages/TenantForm.js'?>"></script>