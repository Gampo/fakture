<?php
/* @var \Fakture\Tenant\Model\Tenant $tenant */

?>
<?php
if ($data['model']):
    $groupId = $data['model']->getId(); ?>
    <h1 class="page-header"><?= $this->t('Edit group:') ?> <?= $data['model']->getName() ?></h1>
<?php
else:
    $groupId = null; ?>
    <h1 class="page-header"><?= $this->t('Create service group') ?></h1>
<?php
endif; ?>
<form action="/service-group/<?= ($data['model']) ? 'update/' . $groupId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <h2 class="page-header"><?= $this->t('Basic Info') ?></h2>
    <div class="form-group divUser">
        <label class="userLabel"><?= $this->t('Name') ?></label>
        <input name="name" value="<?= $data['model']?->getName() ?>" class="form-control inputDesign"
               <?php
               if (!$data['model']): ?>data-validation="length" data-validation-length="min6"
               data-validation-error-msg="Enter group name."<?php
        endif ?> />
    </div>

    <div class="form-group divUser">
        <label class="userLabel"><?= $this->t('Description') ?></label>
        <input name="description" value="<?= $data['model']?->getDescription() ?>" class="form-control inputDesign"/>
    </div>

    <?php
    if ($loggedInRole === 1): ?>
        <div class="form-group divUser">
            <label class="userLabel"><?= $this->t('Tenant') ?></label>
            <select class="form-control selectDesign" name="tenantId">
                <?php
                foreach ($data['tenants'] as $tenant): ?>
                    <option value="<?= $tenant->getId() ?>" <?php
                    if ($data['model']?->getTenant()->getId() === $tenant->getId()) {
                        echo 'selected';
                    } ?>
                    ><?= $tenant->getName() ?></option>
                <?php
                endforeach; ?>
            </select>
        </div>
    <?php
    else: ?>
        <?php
        if (!$data['model']): ?>
            <input type="hidden" name="tenantId" value="<?= $data['loggedInTenantId'] ?>"/>
        <?php
        else: ?>
            <input type="hidden" name="tenantId" value="<?= $data['model']->getTenant()->getId() ?>"/>
        <?php
        endif; ?>
    <?php
    endif; ?>

    <div class="form-group">
        <?php
        if ($groupId): ?>
            <input type="hidden" name="serviceGroupId" value="<?= $groupId ?>"/>
        <?php
        endif; ?>
        <input data-action="/service-group/<?= ($data['model']) ? 'update/' . $groupId : 'create' ?>/" type="submit" value="<?= $this->t('Save') ?>" id="submitButton" class="btn btn-success submitButtonSize"/>
    </div>

</form>