<?php
/* @var \Fakture\Client\Model\Client $client */

/* @var \Fakture\Tenant\Model\Tenant $tenant */
?>
<?php
if ($data['model']):
    $serviceId = $data['model']->getId(); ?>
    <h1 class="page-header"><?= $this->t('Edit service:') ?> <?= $data['model']->getName() ?></h1>
<?php
else:
    $serviceId = 'null'; ?>
    <h1 class="page-header"><?= $this->t('Create service') ?></h1>
<?php
endif; ?>
<form action="/service/<?= ($data['model']) ? 'update/' . $serviceId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="serviceForm" autocomplete="off">
    <?= $this->formToken()?>
    <h4><?= $this->t('Basic Info') ?></h4>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Name') ?></label>
                <input name="name" value="<?= $data['model']?->getName() ?>" class="form-control inputDesign"
                       <?php
                       if (!$data['model']): ?>data-validation="length" data-validation-length="min6"
                       data-validation-error-msg="Enter name."<?php
                endif ?> />
            </div>
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Description') ?></label>
                <input name="description" value="<?= $data['model']?->getDescription() ?>" class="form-control inputDesign"
                       <?php
                       if (!$data['model']): ?>data-validation="length" data-validation-length="min3"
                       data-validation-error-msg="Enter description."<?php
                endif ?> />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Group') ?></label>
                <select name="groupId" required class="form-control">
                    <option value="0">--- <?= $this->t('Select group') ?> ---</option>
                <?php foreach ($data['groups'] as $group): ?>
                    <option value="<?=$group->getId()?>"
                            <?=($data['model']?->getGroup()?->getId() ===$group->getId()) ?? 'selected';?>><?= $group->getName() ?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group divUser">
                <label class="userLabel"><?= $this->t('Type') ?></label>
                <select name="type" required class="form-control">
                    <option value="1"><?= $this->t('Standard') ?></option>
                </select>
            </div>

            <?php
            if ($loggedInRole === 1): ?>
                <div class="form-group divUser">
                    <label class="userLabel"><?= $this->t('Tenant') ?></label>
                    <select name="tenantId" required class="form-control">
                        <option value="0">--- <?= $this->t('Select tenant') ?> ---</option>
                        <?php
                        foreach ($data['tenants'] as $tenant): ?>
                            <option value="<?= $tenant->getId() ?>" <?php
                            if ($data['model']?->getTenant()->getId() === $tenant->getId()) {
                                echo 'selected';
                            } ?>><?= $tenant->getName() ?></option>
                        <?php
                        endforeach; ?>
                    </select>
                </div>
            <?php
            else: ?>
                <?php
                if (!$data['model']): ?>
                    <input type="hidden" name="tenantId" value="<?= $data['loggedInTenantId'] ?>"/>
                <?php
                else: ?>
                    <input type="hidden" name="tenantId" value="<?= $data['model']->getTenant()->getId() ?>"/>
                <?php
                endif; ?>
            <?php
            endif; ?>
        </div>
    </div>


        <h4><?= $this->t('Price') ?></h4>
    <div class="row">
        <?php
        if ($data['model']): ?>
        <div class="col-lg-6">
            <label class="userLabel"><?= $this->t('Price') ?></label>
            <input name="price" value="<?= $data['model']->getPrice()->getAmount() ?>" class="form-control inputDesign"/>
        </div>
        <div class="col-lg-6">
            <label class="userLabel"><?= $this->t('Currency') ?></label>
            <select name="currency" class="form-control inputDesign">
            <?php foreach (\Fakture\Price\Model\Price::getHrCurrencies() as $currency): ?>
                <option value="<?= $currency ?>"
                    <?= ($currency == $data['model']->getPrice()->getCurrency()) ? 'selected' : ''; ?>><?= strtoupper(
                        $currency
                    ) ?></option>
            <?php endforeach; ?>
            </select>
        </div>
        <?php else: ?>

        <div class="col-lg-6">
            <label class="userLabel"><?= $this->t('Price') ?></label>
            <input name="price" value="" class="form-control inputDesign"/>
        </div>
        <div class="col-lg-6">
            <label class="userLabel"><?= $this->t('Currency') ?></label>
            <select name="currency" class="form-control inputDesign">
                <?php foreach (\Fakture\Price\Model\Price::getHrCurrencies() as $currency): ?>
                    <option value="<?= $currency ?>"><?= strtoupper($currency) ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php endif; ?>
    </div>

    <div class="form-group">
        <input type="hidden" name="serviceId" value="<?= $serviceId ?>"/>
        <input data-action="/service/<?= ($data['model']) ? 'update/' . $serviceId : 'create' ?>/" type="submit"
               value="<?= $this->t('Save') ?>" class="btn btn-success submitButtonSize" id="submitButton"/>
    </div>
</form>
