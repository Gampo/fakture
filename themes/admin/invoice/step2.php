<div class="row" style="border-bottom: solid 1px gray">
    <div class="col-lg-2">
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Status')?></label>
            <select readonly class="form-control selectDesign" name="status">
                <?php foreach (\Fakture\Invoice\Model\Invoice::getHrStatuses() as $status => $name): ?>
                    <option value="<?=$status?>" <?= ($data['model']?->getStatus() === $status) ? 'selected':'disabled'; ?>
                    ><?=$name?></option>
                <?php endforeach; ?>
            </select>
            <?php if (!$data['model']): ?>
                <input type="hidden" name="status" value="<?=\Fakture\Invoice\Model\Invoice::STATUS_NEW?>" />
            <?php endif; ?>
        </div>
        <div class="form-group divUser">
            <?php  if ($loggedInRole === 1) : ?>
            <label class="userLabel"><?= $this->t('Kreiraj u ime:') ?></label>
            <select class="form-control selectDesign" name="tenantId">
                <?php foreach ($data['tenants'] as $tenant): ?>
                    <option value="<?= $tenant->getId() ?>" <?php
                    if ($data['model']?->getTenant()?->getId() === $tenant->getId()) {
                        echo 'selected';
                    } ?>
                    ><?= $tenant->getName() ?></option>
                <?php endforeach; ?>
            </select>
            <?php endif; ?>
            Kreiraj kao : (tenant user)
        </div>
    </div>
    <div class="col-lg-1">

    </div>

    <div class="col-lg-3">
        <h4 class="page-header"><?=$this->t('Automatizacija racuna')?></h4>
        <label>Račun automatski generisati:</label><input type="checkbox" name="recurring" />
        <select name="invoiceCreationAt">
            <option value="lastWorkDay">Poslednjeg radnog dana u mesecu</option>
            <option value="lastDay">Poslednjeg dana u mesecu</option>
            <option value="daysBeforeMonthEnd">X dana pre kraja meseca</option>
        </select>
    </div>

    <div class="col-lg-1">

    </div>

    <div class="col-lg-3">
        <h4 class="page-header"><?=$this->t('Podešavanje automatskog slanja')?></h4>
        <p><label>Račun automatski slati klijentu na email:</label>
            <input type="checkbox" name="autoSend" /></p>
        <label for="ready">Automatsko:</label>
        <input id="ready" type="radio" name="autoSendStatus" value="spremno" />
        <label for="locked">Sa korakom odobrenja:</label>
        <input id="locked" type="radio" name="autoSendStatus" value="zakljucano" />
        <p><label>Račun poslati:</label>
            <select name="autoSendAt">
                <option value="lastWorkDay">Poslednjeg radnog dana u mesecu</option>
                <option value="lastDay">Poslednjeg dana u mesecu</option>
                <option value="daysBeforeMonthEnd">X dana pre kraja meseca</option>
            </select></p>
    </div>
</div>
<div class="row" style="border-bottom: solid 1px gray">
    <div class="col-lg-3 businessInfo">
        <h3 class="page-header"><?=$this->t('Detalji o primaocu računa')?></h3>
        <div class="form-group divUser">
            <!--<label class="userLabel"><?=$this->t('Klijent')?></label>-->
            <select class="form-control selectDesign invoiceClient" name="clientId" data-price="<?=$data['clientPrice']?>">
                <option>--- <?=$this->t('Izaberite klijenta')?> ---</option>
                <?php foreach ($data['clients'] as $client): ?>
                    <option value="<?=$client->getId()?>" <?php if ($data['model']?->getClient()?->getId() === $client->getId()) { echo 'selected'; }?>
                    ><?=$client->getName()?></option>
                <?php endforeach; ?>
            </select>
            <p>&nbsp;</p>
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Name')?></label>
            <input name="clientName" value="<?=$data['model']?->getClientName()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('PIB')?></label>
            <input name="clientVat" value="<?=$data['model']?->getClientVat()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('MB')?></label>
            <input name="clientVatId" value="<?=$data['model']?->getClientVatId()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Address')?></label>
            <input name="clientAddress" value="<?=$data['model']?->getClientAddress()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('City')?></label>
            <input name="clientCity" value="<?=$data['model']?->getClientCity()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Country')?></label>
            <input name="clientCountry" value="<?=$data['model']?->getClientCountry()?>" class="form-control inputDesign" />
        </div>
    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-4 businessInfo">
        <h3 class="page-header">Podaci o datumima</h3>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <label class="userLabel"><?=$this->t('Datum izdavanja')?></label>
        <input type="date" name="issueDate" value="<?=$data['model']?->getIssueDate()?->format('Y-m-d')?>" class="form-control inputDesign" />

        <label class="userLabel"><?=$this->t('Datum prometa')?></label>
        <input type="date" name="turnoverDate" value="<?=$data['model']?->getTurnoverDate()?->format('Y-m-d')?>" class="form-control inputDesign" />

        <label class="userLabel"><?=$this->t('Rok plaćanja')?></label>
        <?php
            if ($data['tenantClient']?->getDeadline()) {
                $data['model']?->getDeadlineDate()->modify(sprintf('+ %s day', $data['tenantClient']?->getDeadline()));
            }
        $deadlineDate = $data['model']?->getDeadlineDate()->format('Y-m-d');
        ?>
        <input type="date" name="deadlineDate" value="<?=$data['model']?->getDeadlineDate()?->format('Y-m-d')?>" class="form-control inputDesign" />

        <label class="userLabel"><?=$this->t('Mesto prometa')?></label>
        <input name="turnoverLocation" value="<?=$data['model']?->getTurnoverLocation()?>" class="form-control inputDesign" />

        <label class="userLabel"><?=$this->t('Mesto izdavanja')?></label>
        <input name="issueLocation" value="<?=$data['model']?->getIssueLocation()?>" class="form-control inputDesign" />
    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-3 businessInfo">
        <h3 class="page-header"><?=$this->t('Podaci o izdavaocu računa')?></h3>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Name')?></label>
            <input readonly value="<?=$data['tenantClient']?->getName()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('PIB')?></label>
            <input readonly value="<?=$data['tenantClient']?->getPib()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('MB')?></label>
            <input readonly value="<?=$data['tenantClient']?->getMb()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Address')?></label>
            <input readonly value="<?=$data['tenantClient']?->getAddress()?>, <?=$data['tenantClient']?->getCity()?>" class="form-control inputDesign" />
        </div>
        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Country')?></label>
            <input readonly value="<?=$data['tenantClient']?->getCountry()?>" class="form-control inputDesign" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group divUser col-lg-4">
            <label class="userLabel"><?=$this->t('Dodaj usluge')?></label>
            <select class="form-control selectDesign selectServices" >
                <option value="0">--- <?=$this->t('Izaberi uslugu')?> ---</option>
                <?php foreach ($data['services'] as $service):
                    $selectedServices = $data['model']?->getServices();
                    $selectedHtml = '';
                    if ($selectedServices && in_array($service->getId(), $selectedServices->column('getServiceId'))) {
                        $selectedHtml = 'selected';
                    }
                    ?>
                    <option value="<?=$service->getId()?>" <?=$selectedHtml?>><?=$service->getName()?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <table class="selectedServices">
            <thead>
            <tr>
                <th><?=$this->t('Naziv usluge')?></th>
                <th><?=$this->t('Količina')?></th>
                <th><?=$this->t('Jedinica mere')?></th>
                <th><?=$this->t('Jedinična cena')?></th>
                <th><?=$this->t('Stopa PDV')?>%</th>
                <th><?=$this->t('Poreska osnovica')?></th>
                <th><?=$this->t('Iznos PDV')?></th>
                <th><?=$this->t('Ukupno')?></th>
                <th><?=$this->t('Ukloni uslugu')?></th>
            </tr>
            </thead>
            <tbody>
            <?php if ($data['model']): ?>
                <?php
                $baseTotal = 0;
                $taxTotal = 0;
                foreach ($data['model']->getServices() as $invoiceService):
                    $rowTax = $invoiceService->getPrice() * $invoiceService->getTax() / 100;
                    $taxTotal += $rowTax;
                    $baseTotal += $invoiceService->getPrice();
                    ?>
                    <tr data-service-id="<?=$invoiceService->getId()?>" data-price="<?=$invoiceService->getPrice()?>"
                        data-qty="<?=$invoiceService->getQty()?>" data-tax="<?=$invoiceService->getTax()?>" class="selectedService">
                        <td><input type="text" name="selectedServices[name][]" value="<?=$invoiceService->getServiceName()?>" class="form-control inputDesign" /></td>
                        <td>
                            <input type="number" name="selectedServices[qty][]" value="<?=$invoiceService->getQty()?>" class="form-control inputDesign qty" />
                        </td>
                        <td>
                            <input type="text" name="selectedServices[measureUnit][]" placeholder="j.m." value="" class="form-control inputDesign" />
                            <input type="hidden" name="selectedServices[serviceId][]" value="<?=$invoiceService->getId()?>" />
                        </td>
                        <td><input type="text" name="selectedServices[price][]" value="<?=$invoiceService->getPrice()?>" class="form-control inputDesign price" /></td>
                        <td>
                            <select name="selectedServices[tax][]" class="tax">
                                <option value="0">0</option>
                                <option <?php if ($invoiceService->getTax() === 10) { echo 'selected'; } ?> value="10">10</option>
                                <option <?php if ($invoiceService->getTax() === 20) { echo 'selected'; } ?> value="20">20</option>
                            </select>
                        </td>
                        <td><input disabled type="text" value="<?=$invoiceService->getPrice()?>" class="form-control inputDesign basePrice" /></td>
                        <td>
                            <input readonly value="<?=number_format($rowTax, 2)?>" class="form-control inputDesign rowTax" />
                        </td>
                        <td><input readonly value="<?=number_format($invoiceService->getPrice() + $rowTax, 2)?>" class="form-control inputDesign rowTotal" /></td>
                        <td><span class="removeService">&nbsp; X</span></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="6" class="taxTotal"></td>
                <td colspan="1"><?=$this->t('Poreska osnovica')?></td>
                <td><input readonly class="baseTotal form-control inputDesign" value="<?=$baseTotal?>" /></td>
            </tr>
            <tr>
                <td colspan="6" class="taxTotal"></td>
                <td colspan="1"><?=$this->t('Ukupno PDV')?></td>
                <td><input readonly class="taxTotal form-control inputDesign" value="<?=$taxTotal?>" /></td>
            </tr>
            <tr>
                <td colspan="6" class="taxTotal"></td>
                <td colspan="1"><?=$this->t('Ukupno sa PDV')?></td>
                <td><input readonly class="total form-control inputDesign" value="<?=$data['model']?->getAmount()?>" /></td>
            </tr>
            <tr>
                <td colspan="6" class="taxTotal"></td>
                <td colspan="1"><?=$this->t('Avans')?></td>
                <td><input class="advance form-control inputDesign" value="<?=(int) $data['model']?->getAdvanceAmount()?>" name="advanceAmount" /></td>
            </tr>
            <tr>
                <td colspan="6"></td>
                <td colspan="1"><b><?=$this->t('Ukupno sa PDV')?></b></td>
                <td><input readonly class="grandTotal form-control inputDesign" value="<?=$data['model']?->getAmount()?>" name="amount" /></td>
                <td></td>
            </tr>
            </tfoot>
        </table>

        <div class="form-group divUser">
            <label class="userLabel"><?=$this->t('Opis računa')?></label>
            <textarea name="description" class="form-control inputDesign"><?=$data['model']?->getDescription()?></textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group divUser">
        <label class="userLabel"><?=$this->t('Template')?></label>
        <select class="form-control selectDesign" name="templateId">
            <option value="1"><?=$this->t('Standard')?></option>
        </select>
    </div>

    <div class="form-group">
        <input type="hidden" name="invoiceId" value="<?=$data['invoiceId']?>" />
        <input type="submit" value="Sačuvaj" class="btn btn-success submitButtonSize" id="submitButton" />
    </div>
</div>