<p>Izaberite tip fakture</p>
<label class="userLabel"><?=$this->t('Tip')?></label>
<select class="selectDesign" name="type">
    <?php foreach (\Fakture\Invoice\Model\Invoice::getHrTypes() as $type => $name): ?>
        <option value="<?=$type?>" <?php if ($data['model']?->getType() === $type) { echo 'selected'; }?>
        ><?=$name?></option>
    <?php endforeach; ?>
</select>&nbsp;
<label class="userLabel"><?=$this->t('Ponavljajuća')?></label>
<select class="selectDesign" name="recurring">
    <option value="0"><?=$this->t('Ne')?></option>
    <option value="1"><?=$this->t('Da')?></option>
</select>&nbsp;

<input type="button" value="<?=$this->t('Dalje')?>" class="btn btn-success submitButtonSize" id="invoiceStep1" />