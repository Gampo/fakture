<?php
/* @var \Fakture\Invoice\Model\Invoice $data['invoice'] */
/* @var \Fakture\Invoice\Model\InvoiceService $invoiceService */
/* @var \Fakture\Client\Model\Client $data['tenantClient'] */
?>
<div style="">
    <img src="/images/<?=$data['invoice']->getTenant()->getId()?>/<?=$data['tenantClient']->getLogo()?>" alt="" />
</div>

<table width=100%>
    <thead>

    </thead>
    <tr>
        <td align="left">
            <div style="font-size:13px"><b><?=$data['invoice']->getClient()->getName()?><br />
                <?=$data['invoice']->getClient()->getAddress()?><br />
                <?=$data['invoice']->getClient()->getZip()?> <?=$data['invoice']->getClient()->getCity()?> <br/>
                    <?=$data['invoice']->getClient()->getCountry()?></b>
            </div>
        </td>
        <td align="right">
            <div style="font-size:12px"><b><?=$data['tenantClient']->getName()?><br />
                <?=$data['tenantClient']->getAddress()?><br />
                <?=$data['tenantClient']->getZip()?> <?=$data['tenantClient']->getCity()?>,
                <?=$data['tenantClient']->getCountry()?><br />
                MB: <?=$data['tenantClient']->getMb()?><br />
                PIB: <?=$data['tenantClient']->getPib()?><br />
                Br računa: <?=$data['tenantClient']->getAccountNumber()?></b>
            </div>
        </td>
    </tr>
</table>

<div>
    <h1 style="text-align: center; margin: 0 auto;"><?=$this->t('Račun br.')?> <?=$data['invoice']->getInvoiceCode()?></h1>
</div>

<table width=100%>
    <tr>
        <td align="left">
            <div style=""><b><span style="text-decoration: underline"><?=$this->t('KLIJENT:')?></span><br />
                <?=$data['invoice']->getClient()->getName()?><br />
                <?=$data['invoice']->getClient()->getAddress()?><br />
                <?=$data['invoice']->getClient()->getZip()?> <?=$data['invoice']->getClient()->getCity()?><br />
                <?php if (strlen($data['invoice']->getClient()->getMb())):?>
                    MB / ID: <?=$data['invoice']->getClient()->getMb()?><br />
                <?php endif; ?>
                <?php if (strlen($data['invoice']->getClient()->getPib())):?>
                    PIB / VAT: <?=$data['invoice']->getClient()->getPib()?></b>
                <?php endif; ?>
            </div>
        </td>
        <td width="15%"></td>
        <?php
        $invoiceIssueDate = str_replace(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                                        ['januar','februar','mart','april','maj','jun','jul','avgust','septembar','oktobar','novembar','decembar'],
                                        $data['invoice']->getIssueDate()?->format('d. M Y.'));
        $turnoverDate = str_replace(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                                    ['januar','februar','mart','april','maj','jun','jul','avgust','septembar','oktobar','novembar','decembar'],
                                    $data['invoice']->getTurnoverDate()?->format('d. M Y.'));
        $deadline = str_replace(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                                ['januar','februar','mart','april','maj','jun','jul','avgust','septembar','oktobar','novembar','decembar'],
                                $data['invoice']->getDeadlineDate()?->format('d. M Y.'));
        $deadlineDays = $data['invoice']->getDeadlineDate()->diff($data['invoice']->getIssueDate())->days;
        ?>
        <td width="53%">
            <div style="text-align: right">
                Mesto izdavanja računa: <?=$data['invoice']->getIssueLocation()?><br />
                Mesto prometa: <?=$data['invoice']->getTurnoverLocation()?><br />
                Datum izdavanja računa: <?=$invoiceIssueDate?><br />
                Datum prometa: <?=$turnoverDate?><br />
                Rok plaćanja: (<?=$deadlineDays?> dana) <?=$deadline?>
            </div>
        </td>
    </tr>
</table>

<table width="100%" border="1" cellpadding="5" align="center">
    <tr>
        <th width="4%"><?=$this->t('#')?></th>
        <th width="26%"><?=$this->t('Description')?></th>
        <th width="7%"><?=$this->t('Kol.')?></th>
        <th width="14%"><?=$this->t('Jedinična cena')?></th>
        <th width="7%"><?=$this->t('PDV')?></th>
        <th width="14%"><?=$this->t('Poreska osnovica')?></th>
        <th width="14%"><?=$this->t('Iznos PDV')?></th>
        <th width="16%"><?=$this->t('Ukupno (RSD)')?></th>
    </tr>
<?php
$totalNoTax = 0;
$totalTax = 0;
$total = 0;
$advance = $data['invoice']->getAdvanceAmount();
foreach ($data['invoice']->getServices() as $key => $invoiceService):
    //service calculations
    $rowTax = ($invoiceService->getPrice() * ('0.' . $invoiceService->getTax())) * $invoiceService->getQty();
    $serviceTotalNoTax = $invoiceService->getPrice() * $invoiceService->getQty();
    $serviceTotal = $rowTax + ($invoiceService->getPrice() * $invoiceService->getQty());
    //invoice calculations
    $totalNoTax += $serviceTotalNoTax;
    $totalTax += $rowTax;
    $total += $serviceTotal;
    $serviceName = $invoiceService->getServiceName();
    ?>
    <tr>
        <td style="vertical-align:middle"><?=$key+1?></td>
        <td><?=$serviceName?></td>
        <td><?=$invoiceService->getQty()?></td>
        <td><?=number_format($invoiceService->getPrice(), 2)?></td>
        <td><?=$invoiceService->getTax()?>%</td>
        <td><?=number_format($serviceTotalNoTax, 2)?></td>
        <td><?=number_format($rowTax, 2)?></td>
        <td align="right">
            <?=number_format($serviceTotal, 2)?>
        </td>
    </tr>
<?php endforeach; ?>
</table>

<table>
    <tr>
        <td width="27%"></td>
        <td width="23.1%"></td>
        <td align="right" width="51.8%">
            <table width="100%" border="1" cellpadding="5" align="center">
                <tr>
                    <td width="68.8%" align="left">Poreska osnovica</td>
                    <td width="31.3%" align="right"><?=number_format($totalNoTax, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Iznos PDV-a (20%)</td>
                    <td align="right"><?=number_format($totalTax, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Ukupan iznos sa PDV</td>
                    <td align="right"><?=number_format($total, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Avans</td>
                    <td align="right"><?=number_format($advance, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Za uplatu</td>
                    <td align="right"><?=number_format($total - $advance, 2)?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<p style="margin: 0 auto;">&nbsp;</p>

<table width="100%">
    <tr>
        <td>Molimo vas da uplatu izvršite na navedeni račun u <?=$data['tenantClient']->getBank()?>:</td>
    </tr>
    <tr>
        <td><?=$data['tenantClient']->getAccountNumber()?></td>
    </tr>
<?php if (!$data['invoice']->getTenant()->getSettings()->isInVatSystem() && $data['invoice']->getType() === \Fakture\Invoice\Model\Invoice::TYPE_LOCAL): ?>
    <tr><td></td></tr>
    <tr>
        <td>NAPOMENA: <?=$data['tenantClient']->getName()?> nije u sistemu PDV-a, po članu 33. Zakona o PDV</td>
    </tr>
<?php endif; ?>
    <tr><td></td></tr>
    <tr>
        <td>Faktura je važeća bez pečata i potpisa.</td>
    </tr>
    <tr>
        <td><?=$data['invoice']->getDescription()?></td>
    </tr>
</table>

<?php if ($data['user']): ?>
<p>&nbsp;</p>
<table width="100%" align="right">
    <tr>
        <td><?=$data['user']->getFirstName()?> <?=$data['user']->getLastName()?>, <?=$data['user']->getInvoicePosition()?></td>
    </tr>
    <tr>
        <td>Faktura kreirana od strane:</td>
    </tr>
    <tr>
        <td><?=$data['user']->getFirstName()?> <?=$data['user']->getLastName()?></td>
    </tr>
    <tr>
        <td>JMBG: <?=$data['user']->getJmbg()?></td>
    </tr>
    <tr>
        <td><?=$data['user']->getInvoiceAddress()?></td>
    </tr>
    <tr>
        <td>br.l.k. <?=$data['user']->getLk()?></td>
    </tr>
</table>
<?php endif; ?>