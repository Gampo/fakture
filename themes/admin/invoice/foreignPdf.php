<?php
/* @var \Fakture\Invoice\Model\Invoice $data['invoice'] */
/* @var \Fakture\Invoice\Model\InvoiceService $invoiceService */
/* @var \Fakture\Client\Model\Client $data['tenantClient'] */
?>
<?php if (strlen($data['tenantClient']->getLogo())) : ?>
<div style="">
    <img src="/images/<?=$data['invoice']->getTenant()->getId()?>/<?=$data['tenantClient']->getLogo()?>" alt="" />
</div>
<?php endif; ?>

<table width=100%>
    <tr>
        <td align="left">
            <div style="font-size:13px"><b><?=$data['invoice']->getClient()->getName()?><br />
                <?=$data['invoice']->getClient()->getAddress()?><br />
                <?=$data['invoice']->getClient()->getZip()?> <?=$data['invoice']->getClient()->getCity()?> <br/>
                    <?=$data['invoice']->getClient()->getCountry()?></b>
            </div>
        </td>
        <td align="right">
            <div style="font-size:12px"><b><?=$data['tenantClient']->getName()?><br />
                <?=$data['tenantClient']->getAddress()?><br />
                <?=$data['tenantClient']->getZip()?> <?=$data['tenantClient']->getCity()?>,
                <?=$data['tenantClient']->getCountry()?><br />
                MB / ID: <?=$data['tenantClient']->getMb()?><br />
                PIB / VAT: <?=$data['tenantClient']->getPib()?></b>
            </div>
        </td>
    </tr>
</table>

<div>
    <h1 style="text-align: center; margin: 0 auto;">Račun br. / Invoice number <?=$data['invoice']->getInvoiceCode()?></h1>
</div>

<table width=100%>
    <tr>
        <td align="left">
            <div style=""><b><span style="text-decoration: underline">KLIJENT / CLIENT:</span><br />
                <?=$data['invoice']->getClient()->getName()?><br />
                <?=$data['invoice']->getClient()->getAddress()?><br />
                <?=$data['invoice']->getClient()->getZip()?> <?=$data['invoice']->getClient()->getCity()?><br />
                <?php if (strlen($data['invoice']->getClient()->getMb())):?>
                    MB / ID: <?=$data['invoice']->getClient()->getMb()?><br />
                <?php endif; ?>
                <?php if (strlen($data['invoice']->getClient()->getPib()) > 3):?>
                    PIB / VAT: <?=$data['invoice']->getClient()->getPib()?>
                <?php endif; ?>
                </b>
            </div>
        </td>
        <td width="15%"></td>
        <?php
        $invoiceIssueDate = str_replace(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                                        ['januar','februar','mart','april','maj','jun','jul','avgust','septembar','oktobar','novembar','decembar'],
                                        $data['invoice']->getIssueDate()?->format('d. M Y.'));
        $turnoverDate = str_replace(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                                    ['januar','februar','mart','april','maj','jun','jul','avgust','septembar','oktobar','novembar','decembar'],
                                    $data['invoice']->getTurnoverDate()?->format('d. M Y.'));
        $deadline = str_replace(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                                ['januar','februar','mart','april','maj','jun','jul','avgust','septembar','oktobar','novembar','decembar'],
                                $data['invoice']->getDeadlineDate()?->format('d. M Y.'));
        $deadlineDays = $data['invoice']->getDeadlineDate()->diff($data['invoice']->getIssueDate())->days;
        ?>
        <td width="52%">
            <div style="text-align: right">
                Mesto izdavanja računa / Issue Location: <?=$data['invoice']->getIssueLocation()?><br />
                Mesto prometa / Place of traffic: <?=$data['invoice']->getTurnoverLocation()?><br />
                Datum izdavanja računa / Issue date: <?=$invoiceIssueDate?><br />
                Datum prometa / Date of traffic: <?=$turnoverDate?><br />
                Rok plaćanja / Deadline: (<?=$deadlineDays?> dana / days) <?=$deadline?>
            </div>
        </td>
    </tr>
</table>


<table width="100%" border="1" cellpadding="5" align="center">
    <tr>
        <th width="4%"><?=$this->t('#')?></th>
        <th width="26%">Opis / Description</th>
        <th width="7%">Kol. / Qty</th>
        <th width="14%">Jedinična cena / Unit price</th>
        <th width="7%">PDV / VAT</th>
        <th width="14%">Poreska osnovica / Tax base</th>
        <th width="14%">Iznos PDV / Tax amount</th>
        <th width="16%">Ukupno / Subtotal</th>
    </tr>
<?php
$totalNoTax = 0;
$totalTax = 0;
$total = 0;
$advance = $data['invoice']->getAdvanceAmount();
foreach ($data['invoice']->getServices() as $key => $invoiceService):
    //service calculations
    $rowTax = ($invoiceService->getPrice() * ('0.' . $invoiceService->getTax())) * $invoiceService->getQty();
    $serviceTotalNoTax = $invoiceService->getPrice() * $invoiceService->getQty();
    $serviceTotal = $rowTax + ($invoiceService->getPrice() * $invoiceService->getQty());
    //invoice calculations
    $totalNoTax += $serviceTotalNoTax;
    $totalTax += $rowTax;
    $total += $serviceTotal;
    ?>
    <tr>
        <td style="vertical-align:middle"><?=$key+1?></td>
        <td><?=trim($invoiceService->getServiceName())?></td>
        <td><?=$invoiceService->getQty()?></td>
        <td><?=number_format($invoiceService->getPrice(), 2)?></td>
        <td><?=$invoiceService->getTax()?>%</td>
        <td><?=number_format($serviceTotalNoTax, 2)?></td>
        <td><?=number_format($rowTax, 2)?></td>
        <td align="right">
            <?=number_format($serviceTotal, 2)?>
        </td>
    </tr>
<?php endforeach; ?>
</table>

<table>
    <tr>
        <td width="27%"></td>
        <td width="23%"></td>
        <td align="right" width="52%">
            <table width="100%" border="1" cellpadding="5" align="center">
                <tr>
                    <td width="68.6%" align="left">Poreska osnovica / Tax base</td>
                    <td width="31.3%" align="right"><?=number_format($totalNoTax, 2)?></td>
                </tr>
                <tr>
                    <?php // @TODO solve better ?>
                    <td align="left">Iznos PDV-a (<?=$invoiceService->getTax()?>%) / VAT <?=$invoiceService->getTax()?>%</td>
                    <td align="right"><?=number_format($totalTax, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Ukupan iznos sa PDV / Total with tax</td>
                    <td align="right"><?=number_format($total, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Avans / Advance</td>
                    <td align="right"><?=number_format($advance, 2)?></td>
                </tr>
                <tr>
                    <td align="left">Za uplatu / For payment (<?=strtoupper($data['invoice']->getCurrency())?>)</td>
                    <td align="right"><?=number_format($total - $advance, 2)?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td colspan="3"></td></tr>
</table>

<table width="100%">
    <tr>
        <td>Faktura je važeća bez pečata i potpisa. / Invoice is valid without stamp & signature.</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" width="75%">
            <br />
            <?=$data['invoice']->getTenant()->getInvoiceInstructions($data['invoice']->getCurrency())->getInstruction()?>
        </td>
        <td align="right" width="25%">
            <br /><br /><br /><br />
<?php if ($data['user']): ?>
            <?=$data['user']->getFirstName()?> <?=$data['user']->getLastName()?>, <?=$data['user']->getInvoicePosition()?> <br />
            Faktura kreirana od strane: <br />
            Invoice created by: <br />
            <?=$data['user']->getFirstName()?> <?=$data['user']->getLastName()?> <br />
            JMBG: <?=$data['user']->getJmbg()?> <br />
            <?=$data['user']->getInvoiceAddress()?> <br />
            br.l.k. <?=$data['user']->getLk()?>
<?php endif; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3"><?=$data['invoice']->getDescription()?></td>
    </tr>
</table>