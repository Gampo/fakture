<?php
/* @var \Fakture\Tenant\Model\Tenant $tenant */
/* @var \Fakture\Invoice\Model\Invoice $invoice */
/* @var \Fakture\Invoice\Model\InvoiceService $invoiceService */
/* @var \Fakture\Service\Model\Service $service */
/* @var \Fakture\Invoice\Model\InvoiceServiceCollection $services */
$this->layout('layout::standard');
$data['invoiceRecurringId'] = $invoiceId = $data['model']->getId(); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=$pageTitle?></h1>
    </div>
</div>
<form action="/invoice-recurring/<?=($invoiceId > 0) ? 'update/' . $invoiceId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="invoiceRecurringForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="row" style="border-bottom: solid 1px gray">
                <div class="col-lg-2">
                    <div class="form-group divUser">
                    <?php  if ($loggedInRole === 1) : ?>
                        <label class="userLabel"><?= $this->t('Kreiraj u ime:') ?></label>
                        <select class="form-control selectDesign" name="tenantId">
                            <?php foreach ($data['tenants'] as $tenant): ?>
                                <option value="<?= $tenant->getId() ?>" <?php
                                if ($data['model']?->getTenant()?->getId() === $tenant->getId()) {
                                    echo 'selected';
                                } ?>
                                ><?= $tenant->getName() ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                        <label class="userLabel"><?= $this->t('Kreiraj kao:') ?></label>
                        <select class="form-control selectDesign" name="invoiceUser">
                            <?php foreach ($data['users'] as $user): ?>
                                <option value="<?=$user->getId()?>" <?=($user->getId() ===
                                    $data['model']?->getInvoiceUser()?->getId()) ? 'selected':''?>><?=$user->getDisplayName()?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <select readonly class="form-control selectDesign" name="status" disabled style="display:none">
                    </select>
                    <?php if (!$data['model']): ?>
                        <input type="hidden" name="status" value="<?=\Fakture\Invoice\Model\Invoice::STATUS_NEW?>" />
                    <?php else: ?>
                        <input type="hidden" name="status" value="<?=$data['model']?->getStatus()?>" />
                    <?php endif; ?>
                </div>

                <div class="col-lg-2">
                    <label class="userLabel"><?= $this->t('Type:') ?></label>
                    <select class="form-control selectDesign" name="type">
                        <option value="<?=\Fakture\Invoice\Model\Invoice::TYPE_LOCAL?>" <?=($data['model']?->getType() ===
                            \Fakture\Invoice\Model\Invoice::TYPE_LOCAL) ? 'selected':''?>>Domaća</option>
                        <option value="<?=\Fakture\Invoice\Model\Invoice::TYPE_FOREIGN?>" <?=($data['model']?->getType() ===
                            \Fakture\Invoice\Model\Invoice::TYPE_FOREIGN) ? 'selected':''?>>Inostrana</option>
                    </select>
                    <label class="userLabel"><?= $this->t('Currency:') ?></label>
                    <select class="form-control selectDesign" name="currency">
                    <?php foreach (\Fakture\Price\Model\Price::getHrCurrencies() as $id => $currency): ?>
                        <option value="<?= $id ?>"
                            <?= ($id == $data['model']?->getCurrency()) ? 'selected' : ''; ?>><?= strtoupper(
                                $currency
                            ) ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-lg-3">
                    <h4 class="page-header"><?=$this->t('Automatizacija racuna')?></h4>
                    <label for="autoGenerate">Račun automatski generisati:</label>
                    <select name="autoGenerateAt">
                        <option <?=($data['model']?->getAutoGenerateAt() === 1) ? 'selected':''?> value="1">Poslednjeg radnog dana u mesecu</option>
                        <option <?=($data['model']?->getAutoGenerateAt() === 2) ? 'selected':''?> value="2">Poslednjeg dana u mesecu</option>
                        <option <?=($data['model']?->getAutoGenerateAt() === 3) ? 'selected':''?> value="3">X dana pre kraja meseca</option>
                    </select>
                </div>

                <div class="col-lg-3">
                    <h4 class="page-header">Automatsko slanje</h4>
                    <p><label for="autoSend">Automatski slati klijentu na email:</label>
                        <input <?=($data['model']?->getAutoSend()) ? 'checked':'' ?> id="autoSend" type="checkbox" name="autoSend" /></p>
                    <p><label>Račun poslati:</label>
                        <select name="autoSendAt">
                            <option value="1">Poslednjeg radnog dana u mesecu</option>
                            <option value="2">Poslednjeg dana u mesecu</option>
                            <option value="3">X dana pre kraja meseca</option>
                        </select></p>
                </div>
            </div>
            <div class="row" style="border-bottom: solid 1px gray; padding:1rem 0;">
                <div class="col-lg-3 businessInfo">
                    <h3 class="page-header"><?=$this->t('Detalji o primaocu računa')?></h3>
                    <div class="form-group divUser">
                        <!--<label class="userLabel"><?=$this->t('Klijent')?></label>-->
                        <select class="form-control selectDesign invoiceClient" name="clientId" data-price="">
                            <option value="-1">--- <?=$this->t('Izaberite klijenta')?> ---</option>
                            <?php foreach ($data['clients'] as $client): ?>
                                <option value="<?=$client->getId()?>" <?php if ($data['model']?->getClient()?->getId() === $client->getId()) { echo 'selected'; }?>
                                ><?=$client->getName()?></option>
                            <?php endforeach; ?>
                        </select>
                        <p>&nbsp;</p>
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Name')?></label>
                        <input disabled name="clientName" value="<?=$data['model']?->getClient()?->getName()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('PIB')?></label>
                        <input disabled name="clientVat" value="<?=$data['model']?->getClient()?->getPib()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('MB')?></label>
                        <input disabled name="clientVatId" value="<?=$data['model']?->getClient()?->getMb()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Address')?></label>
                        <input disabled name="clientAddress" value="<?=$data['model']?->getClient()?->getAddress()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('City')?></label>
                        <input disabled name="clientCity" value="<?=$data['model']?->getClient()?->getCity()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Country')?></label>
                        <input disabled name="clientCountry" value="<?=$data['model']?->getClient()?->getCountry()?>" class="form-control inputDesign" />
                    </div>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-4 businessInfo">
                    <h3 class="page-header">Podaci o datumima</h3>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <label class="userLabel"><?=$this->t('Datum izdavanja')?></label>
                    <input type="date" name="issueDate" value="<?=$data['model']?->getIssueDate()?->format('Y-m-d')?>" class="form-control inputDesign" />

                    <label class="userLabel"><?=$this->t('Datum prometa')?></label>
                    <input type="date" name="turnoverDate" value="<?=$data['model']?->getTurnoverDate()?->format('Y-m-d')?>" class="form-control inputDesign" />

                    <label class="userLabel"><?=$this->t('Deadline')?> (<span class="deadlineDaysContainer"></span> <?=$this->t('days')?>)</label>
                    <?php
//                    if ($data['tenantClient']?->getDeadline()) {
//                        $data['model']?->getDeadlineDate()->modify(sprintf('+ %s day', $data['tenantClient']?->getDeadline()));
//                    }
                    $deadlineDate = $data['model']?->getDeadlineDate()->format('Y-m-d');
                    ?>
                    <input type="date" name="deadlineDate" value="<?=$data['model']?->getDeadlineDate()?->format('Y-m-d')?>" class="form-control inputDesign" />

                    <label class="userLabel"><?=$this->t('Mesto prometa')?></label>
                    <input name="turnoverLocation" value="<?=$data['model']?->getTurnoverLocation()?>" class="form-control inputDesign" />

                    <label class="userLabel"><?=$this->t('Mesto izdavanja')?></label>
                    <input name="issueLocation" value="<?=$data['model']?->getIssueLocation()?>" class="form-control inputDesign" />

                    <?php if ($data['tenant']->getSettings()->isInVatSystem()): ?>
                        <div class="form-group divUser">
                            <label class="userLabel"><?=$this->t('Vat Calculation Period')?></label>
                            <select class="form-control" name="vatPeriodCalculationType">
                                <option value="-1"><?=$this->t('Izaberite opciju')?></option>
                                <?php foreach(\Fakture\Invoice\Model\Invoice::getHRVatPeriodCalculations() as $key => $value):
                                    $selected = '';
                                    if ($data['model'] && $data['model']->getVatPeriodCalculationType() === $key) {
                                        $selected = 'selected';
                                    }
                                    if (!$data['model'] && $key === \Fakture\Invoice\Model\Invoice::VAT_PERIOD_CALCULATION_BY_TURNOVER_DATE) {
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="<?=$key?>" <?=$selected?>><?=$value?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    <?php else: ?>
                        <input type="hidden" name="vatPeriodCalculationType" value="<?=\Fakture\Invoice\Model\Invoice::VAT_PERIOD_CALCULATION_BY_TURNOVER_DATE?>" />
                    <?php endif; ?>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-3 businessInfo">
                    <h3 class="page-header"><?=$this->t('Podaci o izdavaocu računa')?></h3>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Name')?></label>
                        <input readonly value="<?=$data['tenantClient']?->getName()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('PIB')?></label>
                        <input readonly value="<?=$data['tenantClient']?->getPib()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('MB')?></label>
                        <input readonly value="<?=$data['tenantClient']?->getMb()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Address')?></label>
                        <input readonly value="<?=$data['tenantClient']?->getAddress()?>, <?=$data['tenantClient']?->getCity()?>" class="form-control inputDesign" />
                    </div>
                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Country')?></label>
                        <input readonly value="<?=$data['tenantClient']?->getCountry()?>" class="form-control inputDesign" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group divUser col-lg-4">
                        <label class="userLabel"><?=$this->t('Dodaj usluge')?></label>
                        <select class="form-control selectDesign selectServices" >
                            <option value="0">--- <?=$this->t('Izaberi uslugu')?> ---</option>
                            <?php /* foreach ($data['services'] as $service):
                                $selectedServices = $data['model']?->getServices();
                                $selectedHtml = '';
                                if ($selectedServices && in_array($service->getId(), $selectedServices->column('getServiceId'))) {
                                    $selectedHtml = 'selected';
                                }
                                ?>
                                <option value="<?=$service->getId()?>" <?=$selectedHtml?>><?=$service->getName()?></option>
                            <?php endforeach;  */ ?>
                        </select>
                    </div>

                    <div class="selectedServices">
                        <?php if ($data['model']): ?>
                            <?php
                            $baseTotal = 0;
                            $taxTotal = 0;
                            $total = 0;
                            foreach ($data['model']->getServices() as $invoiceService):
                                $rowTax = $invoiceService->getPrice() * $invoiceService->getTax() / 100;
                                $taxTotal += $rowTax;
                                $baseTotal += $invoiceService->getPrice();
                                ?>
                                <div data-service-id="<?= $invoiceService->getServiceId() ?>"
                                     data-price="<?= $invoiceService->getPrice() ?>"
                                     data-qty="<?= $invoiceService->getQty() ?>"
                                     data-tax="<?= $invoiceService->getTax() ?>"
                                     class="selectedService mainServiceEntityRow">
                                    <div class="serviceNameContainer">
                                        <label><?= $this->t('Naziv Usluge') ?></label>
                                        <input type="text" name="selectedServices[name][]"
                                               value="<?= $invoiceService->getServiceName() ?>"
                                               class="form-control inputDesign"/>
                                    </div>
                                    <div class="serviceInputs">
                                        <div class="qtyContainer">
                                            <label><?= $this->t('Količina') ?></label>
                                            <input type="number" name="selectedServices[qty][]"
                                                   value="<?= $invoiceService->getQty() ?>"
                                                   class="form-control inputDesign qty"/>
                                        </div>
                                        <div class="measureUnitContainer">
                                            <label><?= $this->t('Jedinica mere') ?></label>
                                            <select name="selectedServices[unitOfMeasure][]" class="form-control inputDesign">
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'H87') ? 'selected':''?> value="H87">Komad</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'KGM') ? 'selected':''?>  value="KGM">Kg</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'KMT') ? 'selected':''?>  value="KMT">Km</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'GRM') ? 'selected':''?>  value="GRM">g</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'MTR') ? 'selected':''?>  value="MTR">m</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'LTR') ? 'selected':''?>  value="LTR">l</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'TNE') ? 'selected':''?>  value="TNE">t</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'MTK') ? 'selected':''?>  value="MTK">m2</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'MTQ') ? 'selected':''?>  value="MTQ">m3</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'MIN') ? 'selected':''?>  value="MIN">min</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'HUR') ? 'selected':''?>  value="HUR">hour</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'DAY') ? 'selected':''?>  value="DAY">day</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'MON') ? 'selected':''?>  value="MON">mon</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'ANN') ? 'selected':''?>  value="ANN">god</option>
                                                <option <?=($invoiceService?->getUnitOfMeasure() === 'KWH') ? 'selected':''?>  value="KWH">kWh</option>
                                            </select>
                                            <input type="hidden" name="selectedServices[serviceId][]"
                                                   value="<?= $invoiceService->getServiceId() ?>"/>
                                        </div>
                                        <div>
                                            <label><?= $this->t('Jedinična cena') ?></label>
                                            <input type="text" name="selectedServices[price][]"
                                                   value="<?= number_format($invoiceService->getPrice(), 2) ?>"
                                                   class="form-control inputDesign price"/>
                                        </div>
                                        <div>
                                            <label><?= $this->t('Stopa PDV%') ?></label>
                                            <select name="selectedServices[tax][]" class="tax form-control">
                                                <option selected value="20">20</option>
                                                <option <?php
                                                if ($invoiceService->getTax() === 10) {
                                                    echo 'selected';
                                                } ?> value="10">10
                                                </option>
                                                <option <?php
                                                if ($invoiceService->getTax() === 0) {
                                                    echo 'selected';
                                                } ?> value="0">0
                                                </option>
                                            </select>
                                        </div>
                                        <div>
                                            <label><?= $this->t('Poreska osnovica') ?></label>
                                            <input disabled type="text"
                                                   value="<?= number_format($invoiceService->getPrice(), 2) ?>"
                                                   class="form-control inputDesign basePrice"/>
                                        </div>
                                        <div>
                                            <label><?= $this->t('Iznos PDV') ?></label>
                                            <input readonly value="<?= number_format($rowTax, 2) ?>"
                                                   class="form-control inputDesign rowTax"/>
                                        </div>
                                        <div>
                                            <label><?= $this->t('Ukupno') ?></label>
                                            <input readonly value="<?= number_format(
                                                $invoiceService->getPrice() + $rowTax,
                                                2
                                            ) ?>" class="form-control inputDesign rowTotal"/>
                                        </div>
                                        <div>
                                            <div style="height:32px;"></div>
                                            <span class="removeService">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                     stroke="currentColor" class="w-6 h-6">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="useContractContainer" style="display:none;">
                                            <label><?=$this->t('Use contract deadline')?></label>
                                            <input class="useContract form-control" type="checkbox">
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;
                            $total = $baseTotal + $taxTotal;
                            ?>
                        <?php endif; ?>
                        <div class="selectedServicesTotals">
                            <label><?=$this->t('Poreska osnovica')?></label>
                            <input id="totalWithoutVAT" readonly class="baseTotal form-control inputDesign"
                                   value="<?=number_format($baseTotal,2)?>" />
                            <label><?=$this->t('Ukupno PDV')?></label>
                            <input id="totalVAT" readonly class="taxTotal form-control inputDesign"
                                   value="<?=number_format($taxTotal,2)?>" />
                            <label><?=$this->t('Ukupno sa PDV')?></label>
                            <input id="totalWithVAT" readonly class="total form-control inputDesign"
                                   value="<?=number_format($total,2)?>" />
                            <label><?=$this->t('Avans')?></label>
                            <input id="advance" class="advance form-control inputDesign"
                                   value="" name="advanceAmount" />
                            <label><b><?=$this->t('Ukupno sa PDV')?></b></label>
                            <input id="totalWithVATAfterAdvance" readonly class="grandTotal form-control inputDesign"
                                   value="<?=number_format($total,2)?>" name="amount" />
                        </div>
                    </div>

                    <div class="form-group divUser">
                        <label class="userLabel"><?=$this->t('Opis računa')?></label>
                        <textarea name="description" class="form-control inputDesign"><?=$data['model']?->getDescription()?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group divUser">
                    <label class="userLabel"><?=$this->t('Template')?></label>
                    <select class="form-control selectDesign" name="templateId">
                        <option value="1"><?=$this->t('Standard')?></option>
                    </select>
                </div>

                <div class="form-group">
                    <input type="hidden" name="invoiceRecurringId" value="<?=$data['invoiceRecurringId']?>" />
                    <input type="submit" value="Sačuvaj" class="btn btn-success submitButtonSize" id="submitButton" />
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</form>

<template id="serviceEntityTemplate">
    <div data-service-id="" data-price=""
         data-qty="" data-tax="20" class="selectedService mainServiceEntityRow">
        <div class="serviceNameContainer">
            <label><?=$this->t('Naziv Usluge')?></label>
            <input type="text" name="selectedServices[name][]" value="" class="form-control inputDesign" />
        </div>
        <div class="serviceInputs">
            <div class="qtyContainer">
                <label><?=$this->t('Količina')?></label>
                <input type="number" name="selectedServices[qty][]" value="1" class="form-control inputDesign qty"/>
            </div>
            <div class="measureUnitContainer">
                <label><?= $this->t('Jedinica mere') ?></label>
                <select name="selectedServices[unitOfMeasure][]" class="form-control inputDesign">
                    <option value="H87">Komad</option>
                    <option value="KGM">Kg</option>
                    <option value="KMT">Km</option>
                    <option value="GRM">g</option>
                    <option value="MTR">m</option>
                    <option value="LTR">l</option>
                    <option value="TNE">t</option>
                    <option value="MTK">m2</option>
                    <option value="MTQ">m3</option>
                    <option value="MIN">min</option>
                    <option value="HUR">hour</option>
                    <option value="DAY">day</option>
                    <option value="MON">mon</option>
                    <option value="ANN">god</option>
                    <option value="KWH">kWh</option>
                </select>
                <input type="hidden" name="selectedServices[serviceId][]" value=""/>
            </div>
            <div>
                <label><?= $this->t('Jedinična cena') ?></label>
                <input type="text" name="selectedServices[price][]" value="" class="form-control inputDesign price"/>
            </div>
            <div>
                <label><?= $this->t('Stopa PDV%') ?></label>
                <select name="selectedServices[tax][]" class="tax form-control">
                    <option selected value="20">20</option>
                    <option value="10">10</option>
                    <option value="0">0</option>
                </select>
            </div>
            <div>
                <label><?= $this->t('Poreska osnovica') ?></label>
                <input disabled type="text" value="" class="form-control inputDesign basePrice"/>
            </div>
            <div>
                <label><?= $this->t('Iznos PDV') ?></label>
                <input readonly value="" class="form-control inputDesign rowTax"/>
            </div>
            <div>
                <label><?= $this->t('Ukupno') ?></label>
                <input readonly value="" class="form-control inputDesign rowTotal"/>
            </div>
            <div>
                <div style="height:32px;"></div>
                <span class="removeService">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </span>
            </div>
            <div class="useContractContainer" style="display:none;">
                <label><?=$this->t('Use contract deadline')?></label>
                <input class="useContract form-control" type="checkbox">
            </div>
        </div>
    </div>
</template>
