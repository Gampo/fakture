<?php

namespace Fakture\Invoice\Validator;

use Skeletor\Validator\ValidatorInterface;
use Fakture\Invoice\Repository\InvoiceRecurringRepository;
use Volnix\CSRF\CSRF;

/**
 * Class Client.
 * User validator.
 *
 * @package Fakture\Client\Validator
 */
class InvoiceRecurring implements ValidatorInterface
{
    /**
     * @var InvoiceRecurringRepository
     */
    private $repo;

    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * User constructor.
     *
     * @param InvoiceRecurringRepository $repo
     * @param CSRF $csrf
     */
    public function __construct(InvoiceRecurringRepository $repo, CSRF $csrf)
    {
        $this->repo = $repo;
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;
//        if (strlen($data['clientCountry']) < 3) {
//            $this->messages['clientCountry'][] = 'Invoice country must be at least 3 characters long.';
//            $valid = false;
//        }
        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
