<?php

namespace Fakture\Invoice\Validator;

use Skeletor\Validator\ValidatorInterface;
use Fakture\Invoice\Repository\InvoiceRepository;
use Volnix\CSRF\CSRF;

/**
 * Class Client.
 * User validator.
 *
 * @package Fakture\Client\Validator
 */
class Invoice implements ValidatorInterface
{
    /**
     * @var InvoiceRepository
     */
    private $repo;

    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * User constructor.
     *
     * @param InvoiceRepository $repo
     * @param CSRF $csrf
     */
    public function __construct(InvoiceRepository $repo, CSRF $csrf)
    {
        $this->repo = $repo;
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;
        if (strlen($data['clientAddress']) < 3) {
            $this->messages['clientAddress'][] = 'Invoice address must be at least 3 characters long.';
            $valid = false;
        }
        if (strlen($data['clientCity']) < 3) {
            $this->messages['clientCity'][] = 'Invoice city must be at least 3 characters long.';
            $valid = false;
        }
        if (strlen($data['clientCountry']) < 3) {
            $this->messages['clientCountry'][] = 'Invoice country must be at least 3 characters long.';
            $valid = false;
        }
        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
