<?php
namespace Fakture\Invoice\Repository;

use Skeletor\Repository\CrudRepository;
use Fakture\Invoice\Mapper\Template as Mapper;
use Fakture\Invoice\Model\Template as Model;

class TemplateRepository extends CrudRepository
{
    /**
     * @param Mapper $serviceMapper
     * @param \DateTime $dt
     */
    public function __construct(Mapper $serviceMapper, \DateTime $dt)
    {
        parent::__construct($serviceMapper, $dt);
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
            $data['invoiceTemplateId'],
            $data['label'],
            $data['template'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }
}