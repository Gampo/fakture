<?php
namespace Fakture\Invoice\Repository;

use Fakture\Efakture\Mapper\EfaktureInvoice;
use Fakture\Invoice\Mapper\InvoiceRecurring;
use Fakture\Invoice\Mapper\InvoiceServices;
use Fakture\Invoice\Model\InvoiceService;
use Fakture\Invoice\Model\InvoiceServiceCollection;
use Fakture\Tenant\Model\Tenant;
use Fakture\Tenant\Repository\TenantRepository;
use Fakture\User\Service\User;
use Skeletor\Model\Model;
use Skeletor\TableView\Repository\TableViewRepository;
use Fakture\Invoice\Mapper\Invoice as Mapper;
use Fakture\Invoice\Model\Invoice;
use Fakture\Service\Repository\ServiceRepository as Service;
use Fakture\Client\Repository\ClientRepository as Client;

class InvoiceRepository extends TableViewRepository
{
    private $clientRepo;

    private $serviceRepo;

    private $invoiceServices;

    private $user;

    private $invoiceRecurring;

    private $dateFields = ['createdAt', 'updatedAt', 'sentAt', 'paidAt', 'deadlineDate', 'turnoverDate', 'issueDate'];

    /**
     * bankRepository constructor.
     *
     * @param Mapper $invoiceMapper
     * @param \DateTime $dt
     */
    public function __construct(
        Mapper $invoiceMapper, \DateTime $dt, Client $clientRepo, Service $serviceRepo, InvoiceServices $invoiceServices,
        TenantRepository $tenantRepo, User $user, InvoiceRecurring $invoiceRecurring, private EfaktureInvoice $efaktureInvoice
    ) {
        parent::__construct($invoiceMapper, $dt);
        $this->clientRepo = $clientRepo;
        $this->serviceRepo = $serviceRepo;
        $this->tenantRepo = $tenantRepo;
        $this->invoiceServices = $invoiceServices;
        $this->user = $user;
        $this->invoiceRecurring = $invoiceRecurring;
    }

    private function formatDates($data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                continue;
            }
            if (in_array($key, $this->dateFields)) {
                if (strlen($value) === 0) {
                    $data[$key] = null;
                } else {
                    $dt = clone $this->dt;
                    $data[$key] = $dt->setTimestamp(strtotime($value))->format('Y-m-d H:i:s');
                }
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @return Model
     * @throws \Exception
     */
    public function create($data): Invoice
    {
        $this->mapper->beginTransaction();
        $selectedServices = $data['selectedServices'];
        unset($data['selectedServices']);
        unset($data['invoiceRecurring']);
        unset($data['autoSend']);
//        unset($data['invoiceRecurring']);
        $data = $this->formatDates($data);
        if ($data['issueDate'] && $data['status'] == Invoice::STATUS_NEW) {
            $data['status'] = Invoice::STATUS_PREPARING;
        }
        $data['recurring'] = isset($data['recurring']) ? $data['recurring'] : 0;
        if (!isset($data['tenantId']) && $this->user->getLoggedInTenantId()) {
            $data['tenantId'] = $this->user->getLoggedInTenantId();
        }
        $data['invoiceId'] = $invoiceId = $this->mapper->insert($data);
        foreach ($selectedServices['serviceId'] as $key => $serviceId) {
            $this->invoiceServices->insert([
                'invoiceId' => $invoiceId,
                'serviceId' => $serviceId,
                'serviceName' => $selectedServices['name'][$key],
                'currency' => $data['currency'],
                'unitOfMeasure' => $selectedServices['unitOfMeasure'][$key],
                'qty' => $selectedServices['qty'][$key],
                'tax' => $selectedServices['tax'][$key],
                'price' => $selectedServices['price'][$key]
            ]);
        }
        $this->mapper->commitTransaction();

        return $this->getById($invoiceId);
    }

    /**
     * @param $data
     * @return bool|Invoice
     * @throws \Exception
     */
    public function update($data): Model
    {
        $this->mapper->beginTransaction();
        $data = $this->formatDates($data);
        $selectedServices = $data['selectedServices'];
        unset($data['selectedServices']);
        unset($data['invoiceRecurring']);
        unset($data['autoSend']);
        // step 1, has date, move status to next
        if ($data['issueDate'] && $data['status'] === Invoice::STATUS_NEW) {
            $data['status'] = Invoice::STATUS_PREPARING;
        }
        $invoiceId = $this->mapper->update($data);
        $this->invoiceServices->deleteBy('invoiceId', $invoiceId);
        foreach ($selectedServices['serviceId'] as $key => $serviceId) {
            $this->invoiceServices->insert([
                'invoiceId' => $invoiceId,
                'serviceId' => $serviceId,
                'serviceName' => $selectedServices['name'][$key],
                'currency' => $data['currency'],
                'unitOfMeasure' => $selectedServices['unitOfMeasure'][$key],
                'qty' => $selectedServices['qty'][$key],
                'price' => $selectedServices['price'][$key],
                'tax' => $selectedServices['tax'][$key]
            ]);
        }
        $this->mapper->commitTransaction();

        return $this->getById($invoiceId);
    }

    public function delete(int $id): bool
    {
        $this->invoiceServices->deleteBy('invoiceId', $id);

        return parent::delete($id);
    }

    /**
     * @param $invoiceData
     * @return Invoice
     */
    public function make($invoiceData): Invoice
    {
        $data = [];
        foreach ($invoiceData as $name => $value) {
            if (in_array($name, $this->dateFields)) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $services = [];
        foreach ($this->invoiceServices->getServices((int) $data['invoiceId']) as $serviceData) {
            $services[] = new InvoiceService(
                $serviceData['invoiceServicesId'],
                $serviceData['invoiceId'],
                $serviceData['serviceId'],
                $serviceData['serviceName'],
                $serviceData['price'],
                $serviceData['currency'],
                $serviceData['qty'],
                $serviceData['tax'],
                $serviceData['unitOfMeasure']
            );
        }
        $data['services'] = new InvoiceServiceCollection($services);
        $data['client'] = $this->clientRepo->getById($data['clientId']);
        $data['tenant'] = $this->tenantRepo->getById($data['tenantId']);
        if ($data['invoiceCode']) {
            $data['invoiceCode'] .= '/'. $data['issueDate']->format('Y');
        }
        $data['sentToSef'] = false;

        if ($data['status'] > Invoice::STATUS_READY) {
            $sefInvoice = $this->efaktureInvoice->fetchAll(['invoiceId' => $data['invoiceId']]);
//            var_dump($sefInvoice);
//            die();
            if (count($sefInvoice)) {
                $data['sentToSef'] = true;
            }
        }
        $invoiceUser = null;
        if ($data['invoiceUser']) {
            $invoiceUser = $this->user->getById($data['invoiceUser']);
        }
        if ($data['parentInvoice']) {
            $data['parentInvoice'] = $this->getById($data['parentInvoice']);
        }

        $data['invoiceUser'] = $invoiceUser;
        unset($data['clientId']);
        unset($data['tenantId']);

        return new Invoice(...$data);
    }

    public function getSearchableColumns(): array
    {
        return ['invoiceCode', 'amount', 'recurring'];
    }

    public function getInvoiceOrdinalNumber(Tenant $tenant, $type = 1)
    {
        $number = $this->mapper->getInvoiceOrdinalNumber($tenant->getId(), $type);
        if ($tenant->getSettings()->getCustomStartingNumber()) {
            $number += $tenant->getSettings()->getCustomStartingNumber();
        }

        return $number;
    }

    public function updateStatus($status, $invoiceId)
    {
        return $this->updateField('status', $status, $invoiceId);
    }

    public function setInvoiceCode($invoiceId, $tenant, $type = 1)
    {
        return $this->updateField('invoiceCode', $this->getInvoiceOrdinalNumber($tenant, $type), $invoiceId);
    }

    public function setPaid($invoiceId, $date)
    {
        return $this->updateField('paidAt', $date, $invoiceId);
    }
}