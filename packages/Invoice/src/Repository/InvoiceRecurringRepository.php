<?php
namespace Fakture\Invoice\Repository;

use Fakture\Invoice\Mapper\InvoiceRecurring as Mapper;
use Fakture\Invoice\Mapper\InvoiceRecurringServices;
use Fakture\Invoice\Model\InvoiceRecurringServiceCollection;
use Fakture\Tenant\Repository\TenantRepository;
use Fakture\User\Service\User;
use Skeletor\TableView\Repository\TableViewRepository;
use Fakture\Invoice\Model\InvoiceRecurring as Model;
use Fakture\Service\Repository\ServiceRepository as Service;
use Fakture\Client\Repository\ClientRepository as Client;

class InvoiceRecurringRepository extends TableViewRepository
{
    private $clientRepo;

    private $invoiceRecurringServices;

    private $user;

    private $dateFields = ['createdAt', 'updatedAt', 'deadlineDate', 'turnoverDate', 'issueDate'];

    /**
     * bankRepository constructor.
     *
     * @param Mapper $invoiceMapper
     * @param \DateTime $dt
     */
    public function __construct(
        Mapper $invoiceMapper, \DateTime $dt, Client $clientRepo, Service $serviceRepo, InvoiceRecurringServices $invoiceRecurringServices,
        TenantRepository $tenantRepo, User $user
    ) {
        parent::__construct($invoiceMapper, $dt);
        $this->clientRepo = $clientRepo;
        $this->serviceRepo = $serviceRepo;
        $this->tenantRepo = $tenantRepo;
        $this->invoiceRecurringServices = $invoiceRecurringServices;
        $this->user = $user;
    }

    private function formatDates($data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                continue;
            }
            if (in_array($key, $this->dateFields)) {
                if (strlen($value) === 0) {
                    $data[$key] = null;
                } else {
                    $dt = clone $this->dt;
                    $data[$key] = $dt->setTimestamp(strtotime($value))->format('Y-m-d H:i:s');
                }
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @return bool|string
     * @throws \Exception
     */
    public function create($data): Model
    {
        $this->mapper->beginTransaction();
        $selectedServices = $data['selectedServices'];
        unset($data['selectedServices']);
        unset($data['invoiceRecurring']);
        $data = $this->formatDates($data);
        $data['tenantId'] = $this->user->getLoggedInTenantId();
        $data['autoSend'] = (isset($data['autoSend']) && $data['autoSend'] === 'on') ? 1:0;
        $invoiceId = $this->mapper->insert($data);
        foreach ($selectedServices['serviceId'] as $key => $serviceId) {
            $this->invoiceRecurringServices->insert([
                'invoiceRecurringId' => $invoiceId,
                'serviceId' => $serviceId,
                'serviceName' => $selectedServices['name'][$key],
                'currency' => $data['currency'],
                'qty' => $selectedServices['qty'][$key],
                'unitOfMeasure' => $selectedServices['unitOfMeasure'][$key],
                'tax' => $selectedServices['tax'][$key],
                'price' => $selectedServices['price'][$key]
            ]);
        }
        $this->mapper->commitTransaction();

        return $this->getById($invoiceId);
    }

    /**
     * @param $data
     * @return bool|Invoice
     * @throws \Exception
     */
    public function update($data): Model
    {
        $this->mapper->beginTransaction();
        $data = $this->formatDates($data);
        $selectedServices = $data['selectedServices'];
        unset($data['selectedServices']);
        if ($data['autoSend'] === 'on') {
            $data['autoSend'] = 1;
        }
        // step 1, has date, move status to next
//        if ($data['issueDate'] && $data['status'] === Invoice::STATUS_NEW) {
//            $data['status'] = Invoice::STATUS_PREPARING;
//        }

        $invoiceId = $this->mapper->update($data);
        $this->invoiceRecurringServices->deleteBy('invoiceRecurringId', $invoiceId);
        foreach ($selectedServices['serviceId'] as $key => $serviceId) {
            $this->invoiceRecurringServices->insert([
                'invoiceRecurringId' => $invoiceId,
                'serviceId' => $serviceId,
                'serviceName' => $selectedServices['name'][$key],
                'currency' => $data['currency'],
                'qty' => $selectedServices['qty'][$key],
                'unitOfMeasure' => $selectedServices['unitOfMeasure'][$key],
                'price' => $selectedServices['price'][$key],
                'tax' => $selectedServices['tax'][$key]
            ]);
        }
        $this->mapper->commitTransaction();

        return $this->getById($invoiceId);
    }

    public function delete(int $id): bool
    {
        $this->invoiceRecurringServices->deleteBy('invoiceRecurringId', $id);

        return parent::delete($id);
    }

    /**
     * @param $invoiceData
     * @return Invoice
     */
    public function make($invoiceData): Model
    {
        $data = [];
        foreach ($invoiceData as $name => $value) {
            if (in_array($name, $this->dateFields)) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $services = [];
        foreach ($this->invoiceRecurringServices->getServices((int) $data['invoiceRecurringId']) as $serviceData) {
            $services[] = new \Fakture\Invoice\Model\InvoiceRecurringServices(
                $serviceData['invoiceRecurringServicesId'],
                $serviceData['invoiceRecurringId'],
                $serviceData['serviceId'],
                $serviceData['serviceName'],
                $serviceData['price'],
                $serviceData['currency'],
                $serviceData['qty'],
                $serviceData['tax'],
                $serviceData['unitOfMeasure']
            );
        }
        $invoiceUser = null;
        if ($data['invoiceUser']) {
            $invoiceUser = $this->user->getById($data['invoiceUser']);
        }
        $data['invoiceUser'] = $invoiceUser;
        $data['services'] = new InvoiceRecurringServiceCollection($services);
        $data['client'] = $this->clientRepo->getById($data['clientId']);
        $data['tenant'] = $this->tenantRepo->getById($data['tenantId']);
        unset($data['clientId']);
        unset($data['tenantId']);

        return new Model(...$data);
    }

    public function getSearchableColumns(): array
    {
        return ['name'];
    }
}