<?php
namespace Fakture\Invoice\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Mapper\PDOWrite;

/**
 * Class Invoice
 * @package Fakture\Invoice\Mapper
 */
class InvoiceRecurring extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'invoiceRecurring', 'invoiceRecurringId');
    }

}