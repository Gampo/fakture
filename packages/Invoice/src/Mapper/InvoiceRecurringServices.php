<?php
namespace Fakture\Invoice\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Mapper\PDOWrite;

/**
 * Class InvoiceServices
 * @package Fakture\Invoice\Mapper
 */
class InvoiceRecurringServices extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'invoiceRecurringServices',);
    }

    public function getServices($invoiceId)
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE `invoiceRecurringId` = $invoiceId";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}