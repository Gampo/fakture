<?php
namespace Fakture\Invoice\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Mapper\PDOWrite;

/**
 * Class Template
 * @package Fakture\Invoice\Mapper
 */
class Template extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'invoiceTemplate', 'invoiceTemplateId');
    }

}