<?php
namespace Fakture\Invoice\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Mapper\PDOWrite;

/**
 * Class Invoice
 * @package Fakture\Invoice\Mapper
 */
class Invoice extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'invoice', 'invoiceId');
    }

    public function getInvoiceOrdinalNumber($tenantId, $type = 1)
    {
        $typeCond = " type IN (1,3)";
        if ($type === 2) {
            $typeCond = " type = 2";
        }
        $year = date('Y');
        $sql = "SELECT COUNT(*) as count FROM `{$this->tableName}` WHERE `invoiceCode` <> '' AND `tenantId` = {$tenantId} 
            AND YEAR(issueDate) = {$year} AND " . $typeCond;
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();
        $item = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $item['count'] + 1;
    }
}