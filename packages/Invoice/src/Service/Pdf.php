<?php

namespace Fakture\Invoice\Service;

class Pdf extends \TCPDF
{
    private $footerText;

    public function Header()
    {
        $this->setX(0);
        $this->setY(0);
        $this->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP-15);
    }

    public function Footer()
    {
        $this->setTextColorArray($this->footer_text_color);
        //set style for cell border
        $line_width = (0.85 / $this->k);
        $this->setLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));
        // Position at 15 mm from bottom
        $this->SetY(-15);
        $this->SetFont('dejavusanscondensed', 'I', 7);
        $content = '';
        if ($this->footerText) {
            $content = $this->footerText;
        }
        $this->writeHTML($content);
    }

    public function overrideFooterText($text)
    {
        $this->footerText = $text;
    }

    public function setHeaderLogo($logo)
    {
        $this->headerLogo = $logo;
    }
}