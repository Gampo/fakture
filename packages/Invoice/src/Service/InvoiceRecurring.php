<?php
namespace Fakture\Invoice\Service;

use Carbon\Carbon;
use Fakture\Client\Model\Client;
use Fakture\Client\Repository\ClientRepository;
use Fakture\Invoice\Model\InvoiceRecurringServices;
use Fakture\Invoice\Model\RecurringInvoice;
use Fakture\Invoice\Repository\InvoiceRecurringRepository as InvoiceRecurringRepo;
use Fakture\Invoice\Repository\InvoiceRepository as InvoiceRepo;
use Fakture\Price\Service\Price;
use Fakture\Service\Service\Service;
use Skeletor\TableView\Model\Column;
use Skeletor\TableView\Service\Table as TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Skeletor\User\Service\User;

class InvoiceRecurring extends TableView
{
    private $invoiceRepo;

    private $clientRepo;

    private $price;

    /**
     * @param InvoiceRecurringRepo $repo
     * @param User $user
     * @param Logger $logger
     * @param TenantRepo $tenantRepo
     * @param ActivityRepository $activity
     */
    public function __construct(
        InvoiceRecurringRepo $repo, User $user, Logger $logger, TenantRepo $tenantRepo, ActivityRepository $activity,
        \Fakture\Invoice\Filter\InvoiceRecurring $filter, InvoiceRepo $invoiceRepo, ClientRepository $client, Price $price,
        private Invoice $invoiceService, private Service $service
    ) {
        parent::__construct($repo, $user, $logger, $tenantRepo, $filter, null);
        $this->invoiceRepo = $invoiceRepo;
        $this->clientRepo = $client;
        $this->price = $price;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        if(!$order) {
            $order = [
                'order' => 'DESC',
                'orderBy' => 'createdAt'
            ];
        }
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        if ($data['count'] === "0") {
            return [
                'count' => 0,
                'entities' => [],
            ];
        }
        $items = [];
        foreach ($data['entities'] as $invoice) {
            $action = sprintf('<a target="_blank" href="/invoice-recurring/createInvoice/%s/">Kreiraj sledeću</a>', $invoice->getId());
            $item = [
                'id' => [
                    'value' => $invoice->getId(),
                    'editColumn' => true,
                ],
                'client' => $invoice->getClient()->getName(),
                'type' => \Fakture\Invoice\Model\Invoice::getHrType($invoice->getType()),
                'status' => \Fakture\Invoice\Model\Invoice::getHrStatus($invoice->getStatus()),
                'updatedAt' => $invoice->getCreatedAt()->format('d.m.Y'),
                'createdAt' => $invoice->getCreatedAt()->format('d.m.Y'),
                'tmp' => $action,
            ];
            if ($this->user->isAdminLoggedIn()) {
                $item['tenantId'] = $invoice->getTenant()->getName();
            }
            $items[] = [
                'columns' => $item,
                'id' => $invoice->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'id', 'label' => '#'],
            ['name' => 'client', 'label' => 'Client'],
            ['name' => 'type', 'label' => 'Type'],
            ['name' => 'status', 'label' => 'Status'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'tmp', 'label' => 'Action'],
        ];
        if ($this->user->isAdminLoggedIn()) {
            $columnDefinitions[] = ['name' => 'tenantId', 'label' => 'Tenant'];
        }

        return $columnDefinitions;
    }

    public function createInvoice(\Fakture\Invoice\Model\InvoiceRecurring $recurringInvoice)
    {
        $data = $recurringInvoice->toArray();
        $data['invoiceId'] = null;
        $data['recurring'] = $recurringInvoice->getId();
        $data['tenantId'] = $data['tenant']['id'];
        $data['clientId'] = $data['client']['id'];
        $autoSettings = [
            'autoSend' => $data['autoSend'],
            'autoSendStatus' => $data['autoSendStatus'],
            'autoSendAt' => $data['autoSendAt'],
        ];
        /* @var Client $client */
        $client = $this->clientRepo->getById($data['clientId']);
        if (!$client->isActive()) {
            return;
        }

        $selectedServices = $data['services'];
        unset($data['id']);
        unset($data['autoSend']);
        unset($data['autoSendStatus']);
        unset($data['autoSendAt']);
        unset($data['autoGenerateAt']);
        unset($data['tenant']);
        unset($data['client']);
        unset($data['services']);
        unset($data['createdAt']);
        unset($data['updatedAt']);
        $dt = new \Carbon\Carbon();
        $dt->locale('sr');

        $months = [
            'januar', 'februar', 'mart', 'april', 'maj', 'jun', 'jul', 'avgust', 'septembar', 'oktobar', 'novembar', 'decembar'
        ];

        $dl = Carbon::createFromFormat('d.m.Y', $data['deadlineDate']);
        $issueDate = Carbon::createFromFormat('d.m.Y', $data['issueDate']);
        $data['issueDate'] = $dt->format('d.m.Y');
        $data['turnoverDate'] = $dt->format('d.m.Y');
        $deadline = $dl->diff($issueDate)->d;

        $data['clientName'] = $client->getName();
        $data['clientAddress'] = $client->getAddress();
        $data['clientCity'] = $client->getCity();
        $data['clientCountry'] = $client->getCountry();
        $data['clientVat'] = $client->getPib();
        $data['clientVatId'] = $client->getMb();

        $amount = 0;
        /* @var InvoiceRecurringServices $service */
        foreach ($selectedServices as $service) {
            $realService = $this->service->getById($service->getServiceId());
            $data['selectedServices']['serviceId'][] = $service->getServiceId();
            $serviceName = $service->getServiceName();
            $month = $months[$dt->format('m') - 1];
            $year = $dt->format('Y') .'.';
            // @TODO translation
            if ($recurringInvoice->getClient()->getContractInfo($service->getServiceId())) {
                $deadline = $recurringInvoice->getClient()->getContractInfo($service->getServiceId())->getDeadline();
                $contractInfo = 'po ugovoru ' . $recurringInvoice->getClient()->getContractInfo($service->getServiceId())->getNumber();
                $serviceName = str_replace('{contractInfo}', $contractInfo, $serviceName);
            }
            $serviceName = str_replace(['{month}', '{year}'], [$month, $year], $serviceName);
            $data['selectedServices']['name'][] = $serviceName;
            $data['selectedServices']['qty'][] = $service->getQty();
            $data['selectedServices']['unitOfMeasure'][] = $service->getUnitOfMeasure();
            $data['selectedServices']['tax'][] = $service->getTax();
            $price = $this->price->convert($realService->getPrice(), $recurringInvoice->getCurrency());
            if ($client->getContractInfo($service->getServiceId())?->getPrice()->getAmount()) {
                $price = $this->price->convert($client->getContractInfo($service->getServiceId())->getPrice(), $recurringInvoice->getCurrency());
            }
//            if ($client->getContractInfo($service->getServiceId())?->getDeadline()) {
//                $deadline = $client->getContractInfo($service->getServiceId())->getDeadline();
//            }
            $data['selectedServices']['price'][] = $price;

            $tax = '1.' . $service->getTax();
            $amount += $service->getQty() * $price * $tax;
        }
        $dt->modify(sprintf('+%s day', $deadline));
        $data['deadlineDate'] = $dt->format('d.m.Y');
        $data['amount'] = $amount;
        $data['invoiceUser'] = $recurringInvoice->getInvoiceUser()->getId();
        $data['status'] = \Fakture\Invoice\Model\Invoice::STATUS_PREPARING;

        // status is 2
        $invoice = $this->invoiceRepo->create($data);
        //status is 3
        $this->invoiceService->advanceFlow($invoice);
        if ($autoSettings['autoSend']) {
            //fetch fresh
//            $invoice = $this->invoiceService->getById($invoice->getId());
            // status is 4, locked & ready to send
//            $this->invoiceService->advanceFlow($invoice);

            //@todo send mail
        }
    }
}