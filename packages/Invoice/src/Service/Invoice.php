<?php
namespace Fakture\Invoice\Service;

use Fakture\Client\Repository\ClientRepository;
use Fakture\Efakture\Mapper\EfaktureInvoice;
use Fakture\Efakture\Service\Api;
use Fakture\Invoice\Model\InvoiceService;
use Fakture\Invoice\Repository\InvoiceRepository as InvoiceRepo;
use Fakture\Price\Service\Price;
use Fakture\Service\Repository\ServiceRepository;
use Skeletor\Mapper\NotFoundException;
use Skeletor\TableView\Model\Column;
use Skeletor\TableView\Service\Table as TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Skeletor\User\Service\User;
use Fakture\Invoice\Filter\Invoice as InvoiceFilter;
use Tamtamchik\SimpleFlash\Flash;

class Invoice extends TableView
{

    /**
     * @param InvoiceRepo $repo
     * @param User $user
     * @param Logger $logger
     * @param TenantRepo $tenantRepo
     * @param ActivityRepository $activity
     */
    public function __construct(
        InvoiceRepo $repo, User $user, Logger $logger, TenantRepo $tenantRepo, ActivityRepository $activity,
        InvoiceFilter $filter, private \DateTime $dt, private \League\Plates\Engine $engine, private Api $api,
        private EfaktureInvoice $efaktureInvoice, private Flash $flash, private ClientRepository $client,
        private ServiceRepository $service, private Price $price
    ) {
        parent::__construct($repo, $user, $logger, $tenantRepo, $filter, null);
    }

    public function parseDataForPdf(\Fakture\Invoice\Model\Invoice $invoice, \Fakture\Client\Model\Client $client)
    {
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $title = 'Račun br. ' . $invoice->getInvoiceCode();
        if ($invoice->getType() === \Fakture\Invoice\Model\Invoice::TYPE_FOREIGN) {
            $title = 'Račun br. / Invoice No.' . $invoice->getInvoiceCode();
        }

        $pdf->setCreator('Gampo Fakture');
        $pdf->setAuthor($client->getName());
        $pdf->setTitle($title);
        $pdf->setSubject($title);
        $pdf->setHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, '', array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setHeaderLogo('');

        // set default monospaced font
        $pdf->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->setMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);
        $pdf->setHeaderMargin(5);
        $pdf->setFooterMargin(5);
        $pdf->setAutoPageBreak(TRUE, 10);
        $pdf->AddPage(); // add first page
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $footer = sprintf('<table>
<tr>
    <td align="left">
        <a href="%s">%s</a>
    </td>
    <td align="right">
        %s, %s, %s, %s
    </td>
</tr>
</table>', $client->getWebsite(), $client->getWebsite(), $client->getName(), $client->getAddress(), $client->getCity(), $client->getCountry());

        $pdf->overrideFooterText($footer);
        $pdf->setFontSubsetting(true); // kozgopromedium dejavusanscondensed , dejavuserifcondensed
        $pdf->setFont('dejavusanscondensed', '', 8, '', true);

        // hack to enable css margin
        $tagvs = [
            'h1' => array(0 => array('h' => 0, 'n' => 0.05), 1 => array('h' => 0, 'n' => 0)),
            'div' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
            'table' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
            'td' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
            'tr' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
            'img' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
            'b' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
        ];
        $pdf->setHtmlVSpace($tagvs);

        return $pdf;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        if(!$order) {
            $order = [
                'order' => 'DESC',
                'orderBy' => 'createdAt'
            ];
        }
//        $filter = ['tenantId' => null];
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        if ($data['count'] === "0") {
            return [
                'count' => 0,
                'entities' => [],
            ];
        }
        $items = [];
        /* @var \Fakture\Invoice\Model\Invoice $invoice */
        foreach ($data['entities'] as $invoice) {
            $pdf = sprintf('<a target="_blank" title="Preuzmi pdf" href="/invoice/pdf/%s/">%s</a>', $invoice->getId(), 'Pdf');
            $flowAction = '';
            if ($invoice->getType() < 3) {
                switch ($invoice->getStatus()) {
                    case \Fakture\Invoice\Model\Invoice::STATUS_PREPARING:
                        $flowAction = " | " . sprintf('<a href="/invoice/flow/%s/">%s</a>', $invoice->getId(), 'Spremi');
                        break;
                    case \Fakture\Invoice\Model\Invoice::STATUS_READY:
                        $flowAction = " | " . sprintf('<a href="/invoice/flow/%s/">%s</a>', $invoice->getId(), 'Pošalji');
                        break;
                    case \Fakture\Invoice\Model\Invoice::STATUS_WAITING_PAYMENT:
                        $flowAction = " | " . sprintf('<a href="/invoice/flow/%s/">%s</a>', $invoice->getId(), 'Plaćeno');
                        break;
                }
                if ($invoice->getStatus() > \Fakture\Invoice\Model\Invoice::STATUS_READY && $invoice->getStatus() !== \Fakture\Invoice\Model\Invoice::STATUS_VOIDED) {
                    $flowAction .= " | " . sprintf('<a href="/invoice/void/%s/">%s</a>', $invoice->getId(), 'Storniraj');
                }
            }

            $itemData = [
                'id' => $invoice->getId(),
                'invoiceCode' =>  [
                    'value' => $invoice->getInvoiceCode(),
                    'editColumn' => true,
                ],
                'clientId' => $invoice->getClient()->getName(),
                'amount' => number_format($invoice->getAmount(), 2, ',', '.') .' '. strtoupper($invoice->getCurrency()),
                'recurring' => ($invoice->getRecurring()) ?: 'Ne',
                'type' => $invoice->getHrType($invoice->getType()),
                'status' => \Fakture\Invoice\Model\Invoice::getHrStatus($invoice->getStatus()),
                'paidAt' => ($invoice->getPaidAt()) ? $invoice->getPaidAt()->format('d.m.Y') : '',
                'sentToSef' => ($invoice->isSentToSef()) ? "Da":"Ne",
                'createdAt' => $invoice->getCreatedAt()->format('d.m.Y'),
                'updatedAt' => $invoice->getUpdatedAt()->format('d.m.Y'),
                'tmp' => $pdf . $flowAction,
            ];
            if ($this->user->isAdminLoggedIn()) {
                $itemData['tenantId'] = $invoice->getTenant()->getName();
            }
//            $itemData['action'] = $pdf . $flowAction;
            $items[] = [
                'columns' => $itemData,
                'id' => $invoice->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $clients = [];
        foreach ($this->client->fetchAll(['tenantId' => $this->user->getLoggedInTenantId()]) as $client) {
            $clients[$client->getId()] = $client->getName();
        }
        $columnDefinitions = [
            ['name' => 'invoiceCode', 'label' => 'Number'],
            ['name' => 'clientId', 'label' => 'Client', 'filterData' => $clients],
            ['name' => 'amount', 'label' => 'Amount'],
            ['name' => 'type', 'label' => 'Type', 'filterData' => \Fakture\Invoice\Model\Invoice::getHrTypes()],
            ['name' => 'recurring', 'label' => 'Recurring'],
            ['name' => 'status', 'label' => 'Status', 'filterData' => \Fakture\Invoice\Model\Invoice::getHrStatuses()],
            ['name' => 'sentToSef', 'label' => 'Sent to SEF'],
            ['name' => 'paidAt', 'label' => 'Paid at'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'tmp', 'label' => 'Action'],
        ];
        if ($this->user->isAdminLoggedIn()) {
            $columnDefinitions[] = ['name' => 'tenantId', 'label' => 'Tenant'];
        }

        return $columnDefinitions;
    }

    public function advanceFlow(\Fakture\Invoice\Model\Invoice $invoice)
    {
        switch ($invoice->getStatus()) {
            case \Fakture\Invoice\Model\Invoice::STATUS_PREPARING:
                $status = \Fakture\Invoice\Model\Invoice::STATUS_READY;
                $this->repo->setInvoiceCode($invoice->getId(), $invoice->getTenant(), $invoice->getType());
                $this->repo->updateStatus($status, $invoice->getId());
                $this->flash->success('Faktura uspešno spremljena.');
                break;
            case \Fakture\Invoice\Model\Invoice::STATUS_READY:
                $status = \Fakture\Invoice\Model\Invoice::STATUS_WAITING_PAYMENT;
                $this->repo->updateStatus($status, $invoice->getId());
                $invoice = $this->repo->getById($invoice->getId());
                $this->createPdfInvoice($invoice);
                if ($invoice->getTenant()->getSettings()->useSef() && $invoice->getType() === \Fakture\Invoice\Model\Invoice::TYPE_LOCAL) {
                    if (!$this->createEfakturaInvoice($invoice)) {
                        $this->flash->error('Došlo je do neočekivane greške prilikom slanja fakture na SEF.');
                        $this->repo->updateStatus(\Fakture\Invoice\Model\Invoice::STATUS_READY, $invoice->getId());
                    }
                }
                $this->flash->success('Faktura uspešno poslata.');

                // sentAt
                break;
//            case \Fakture\Invoice\Model\Invoice::STATUS_LOCKED:
//                $status = \Fakture\Invoice\Model\Invoice::STATUS_WAITING_PAYMENT;
//                break;
            case \Fakture\Invoice\Model\Invoice::STATUS_WAITING_PAYMENT:
                $status = \Fakture\Invoice\Model\Invoice::STATUS_FINISHED;
                $this->repo->setPaid($invoice->getId(), $this->dt->format('Y-m-d H:i:s'));
                $this->repo->updateStatus($status, $invoice->getId());
                break;
            default:
                throw new \Exception('Invalid current status cannot advance workflow step.');
        }

    }

    /**
     * Returns available pdf invoice to download.
     *
     * @param \Fakture\Invoice\Model\Invoice $invoice
     * @param \Psr\Http\Message\MessageInterface $response
     * @return \Psr\Http\Message\MessageInterface|void
     */
    public function returnPdfToDownloadFromInvoice(
        \Fakture\Invoice\Model\Invoice $invoice, \Psr\Http\Message\MessageInterface $response
    ) {
        if ($invoice->getStatus() > \Fakture\Invoice\Model\Invoice::STATUS_LOCKED) {
            $fileName = $this->compilePdfFilename($invoice);
            $clientFileName = $this->compilePdfFilenameForDownload($invoice);
            $filePath = sprintf('%s/pdf/%s/%s',DATA_PATH, $invoice->getId(), $fileName);
            $response = $response->withAddedHeader('Content-Transfer-Encoding', 'binary');
            $response = $response->withAddedHeader('Content-Disposition', 'attachment; filename="'. $clientFileName .'"');
            $response = $response->withAddedHeader('Content-Type', 'application/pdf');
            $response->getBody()->write(file_get_contents($filePath));
            $response->getBody()->rewind();

            return $response;
        }

        $this->outputPdfFromInvoice($invoice);
    }

    private function compilePdfFilenameForDownload(\Fakture\Invoice\Model\Invoice $invoice)
    {
        $separator = '-';
        $parts = [];
        foreach (explode($separator, $invoice->getTenant()->getSettings()->getFilenamePattern()) as $field) {
            $method = 'get' . ucfirst($field);
            $parts[] = $invoice->{$method}();
        }
        $fileName = implode($separator, $parts);

        return str_replace('/', '-', $fileName) . '.pdf';
    }

    private function compilePdfFilename(\Fakture\Invoice\Model\Invoice $invoice)
    {
        return str_replace('/', '-', $invoice->getInvoiceCode()) . '.pdf';
    }

    private function createPdfInvoice(\Fakture\Invoice\Model\Invoice $invoice)
    {
        $fileName = $this->compilePdfFilename($invoice);
        $filePath = sprintf('%s/pdf/%s/',DATA_PATH, $invoice->getId());
        if (!is_dir($filePath)) {
            mkdir($filePath);
        }
        $filePath = sprintf('%s/%s',$filePath, $fileName);
//        if (!is_file($filePath)) {
            $pdf = $this->parsePdf($invoice);
            file_put_contents($filePath, $pdf->Output($fileName, 'S'));
//        }
    }

    /**
     * Return tmp pdf file for download / preview
     *
     * @param \Fakture\Invoice\Model\Invoice $invoice
     * @return void
     */
    private function outputPdfFromInvoice(\Fakture\Invoice\Model\Invoice $invoice)
    {
        $pdf = $this->parsePdf($invoice);
        // will return response to download file
        $pdf->Output($this->compilePdfFilenameForDownload($invoice), 'I');
    }

    private function parsePdf(\Fakture\Invoice\Model\Invoice $invoice)
    {
        $tenantClient = $this->client->getById($invoice->getTenant()->getClientId());
        $pdf = $this->parseDataForPdf($invoice, $tenantClient);
        $tpl = 'invoice/pdf';
        if ($invoice->getType() === \Fakture\Invoice\Model\Invoice::TYPE_FOREIGN) {
            $tpl = 'invoice/foreignPdf';
        }
        $pdf->writeHTML($this->engine->render($tpl, ['data' => [
            'invoice' => $invoice, 'tenantClient' => $tenantClient, 'user' => $invoice->getInvoiceUser()
        ]]));
//        echo $this->engine->render($tpl, ['data' => [
//            'invoice' => $invoice, 'tenantClient' => $tenantClient, 'user' => $invoice->getInvoiceUser()
//        ]]);
//        die();

        return $pdf;
    }

    private function createEfakturaInvoice(\Fakture\Invoice\Model\Invoice $invoice)
    {
        try {
            $client = $this->client->getById($invoice->getTenant()->getClientId());
            $result = $this->api->sendUblInvoice($invoice, $client);
            $this->efaktureInvoice->insert([
                'invoiceId' => $invoice->getId(),
                'sefInvoiceId' => $result->InvoiceId,
                'sefPurchaseInvoiceId' => $result->PurchaseInvoiceId,
                'sefSalesInvoiceId' => $result->SalesInvoiceId,
                'status' => 'Sent',
            ]);
            $this->flash->success('Faktura uspešno poslata na Sistem Elektronskih Faktura.');
            return true;
        } catch (NotFoundException $e) {
            $this->flash->error('Korisnik nije registrovan na SEF servisu, faktura nije poslata na SEF servis.');
            // @TODO add selection setting
            if (false) { // want to use this option
                // report pdv
            } else {
                $this->flash->error('Prijava PDV-a nije uradjena.');
            }

            return true;
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
            return false;
        }
    }

    public function voidInvoice(\Fakture\Invoice\Model\Invoice $invoice)
    {
        if ($invoice->getType() > 2) {
            throw new \InvalidArgumentException('Faktura ima pogresan tip, nije je moguce stornirati.');
        }
        $validStatus = [
            \Fakture\Invoice\Model\Invoice::STATUS_LOCKED,
            \Fakture\Invoice\Model\Invoice::STATUS_WAITING_PAYMENT,
            \Fakture\Invoice\Model\Invoice::STATUS_FINISHED,
        ];
        if (!in_array($invoice->getStatus(), $validStatus)) {
            throw new \InvalidArgumentException('Faktura ima pogresan status, nije je moguce stornirati.');
        }
        try {
            $this->repo->updateStatus(\Fakture\Invoice\Model\Invoice::STATUS_VOIDED, $invoice->getId());
            /* @var \Fakture\Invoice\Model\Invoice $invoice */
            $invoice = $this->repo->getById($invoice->getId());
            $this->createPdfInvoice($invoice);
//            if ($invoice->getTenant()->getSettings()->useSef()) {
//                try {
//                    $sefInvoiceData = $this->efaktureInvoice->fetchAll(['invoiceId' => $invoice->getId()]);
//                    //check if invoice is registered with SEF
//                    if (isset($sefInvoiceData[0])) {
//                        $sefInvoiceId = $sefInvoiceData[0]['sefSalesInvoiceId'];
//                        $this->api->voidInvoice($invoice->getTenant(), $sefInvoiceId);
//                    }
//                    // @TODO maybe log if not found ?
//                } catch (\Exception $e) {
//                    $this->flash->error('Storniranje fakture na SEF servisu nije uspelo: ' . $e->getMessage());
//                }
//            }
        } catch (\Exception $e) {
            $this->flash->error('Storniranje nije uspelo: ' . $e->getMessage());
        }
    }

    /**
     * creates a new storno invoice
     *
     * @param \Fakture\Invoice\Model\Invoice $invoice
     * @return void
     * @throws NotFoundException
     */
    public function createStornoInvoice(\Fakture\Invoice\Model\Invoice $invoice)
    {
        if ($invoice->getType() > 2) {
            throw new \InvalidArgumentException('Faktura ima pogresan tip, nije je moguce stornirati.');
        }
        $validStatus = [
            \Fakture\Invoice\Model\Invoice::STATUS_LOCKED,
            \Fakture\Invoice\Model\Invoice::STATUS_WAITING_PAYMENT,
            \Fakture\Invoice\Model\Invoice::STATUS_FINISHED,
        ];
        if (!in_array($invoice->getStatus(), $validStatus)) {
            throw new \InvalidArgumentException('Faktura ima pogresan status, nije je moguce stornirati.');
        }

        $data = $invoice->toArray();
        // continue creating void
        $data['invoiceId'] = null;
        $data['tenantId'] = $invoice->getTenant()->getId();
        $data['clientId'] = $invoice->getClient()->getId();
        unset($data['tenant']);
        unset($data['client']);
        unset($data['services']);
        unset($data['id']);
        unset($data['createdAt']);
        unset($data['updatedAt']);

        $dt = new \Carbon\Carbon();

        $data['issueDate'] = $dt->format('d.m.Y');
        $data['turnoverDate'] = $dt->format('d.m.Y');
        $deadline = 0;

        $data['clientName'] = $invoice->getClient()->getName();
        $data['clientAddress'] = $invoice->getClient()->getAddress();
        $data['clientCity'] = $invoice->getClient()->getCity();
        $data['clientCountry'] = $invoice->getClient()->getCountry();
        $data['clientVat'] = $invoice->getClient()->getPib();
        $data['clientVatId'] = $invoice->getClient()->getMb();

        $amount = 0;
        /* @var InvoiceService $service */
        foreach ($invoice->getServices() as $service) {
            $realService = $this->service->getById($service->getServiceId());
            $data['selectedServices']['serviceId'][] = $service->getServiceId();
            $serviceName = $service->getServiceName();
            if ($invoice->getClient()->getContractInfo($service->getServiceId())) {
                $serviceName .= ' po ugovoru ' . $invoice->getClient()->getContractInfo($service->getServiceId())->getNumber();
            }
            $data['selectedServices']['name'][] = $serviceName;
            $data['selectedServices']['qty'][] = $service->getQty();
            $data['selectedServices']['tax'][] = $service->getTax();
            $price = $this->price->convert($realService->getPrice(), $invoice->getCurrency());
            if ($invoice->getClient()->getContractInfo($service->getServiceId())?->getPrice()->getAmount()) {
                $price = $this->price->convert($invoice->getClient()->getContractInfo($service->getServiceId())->getPrice(), $invoice->getCurrency());
            }
            if ($invoice->getClient()->getContractInfo($service->getServiceId())?->getDeadline()) {
                $deadline = $invoice->getClient()->getContractInfo($service->getServiceId())->getDeadline();
            }
            $data['selectedServices']['price'][] = -$price;

            $tax = '1.' . $service->getTax();
            $amount -= $service->getQty() * $price * $tax;
        }
        $dt->modify(sprintf('+%s day', $deadline));
        $data['deadlineDate'] = $dt->format('d.m.Y');
        $data['amount'] = $amount;
        $data['invoiceUser'] = $invoice->getInvoiceUser()->getId();
        $data['status'] = \Fakture\Invoice\Model\Invoice::STATUS_FINISHED;
        $data['parentInvoice'] = $invoice->getId();
        $data['invoiceCode'] = '';
        $data['type'] = \Fakture\Invoice\Model\Invoice::TYPE_VOID;

        try {
            $storno = $this->repo->create($data);
            $this->repo->setInvoiceCode($storno->getId(), $storno->getTenant(), $storno->getType());
            $storno = $this->repo->getById($storno->getId());
            $this->repo->updateStatus(\Fakture\Invoice\Model\Invoice::STATUS_VOIDED, $invoice->getId());
            $this->createPdfInvoice($storno);
        } catch (\Exception $e) {
            $this->flash->error('Storniranje nije uspelo: ' . $e->getMessage());
        }
        if ($invoice->getTenant()->useSef()) {
            try {
                $sefInvoiceData = $this->efaktureInvoice->fetchAll(['invoiceId' => $invoice->getId()]);
                //check if invoice is registered with SEF, when client was not registered at SEF
                if (isset($sefInvoiceData[0])) {
                    $sefInvoiceId = $sefInvoiceData[0]['sefSalesInvoiceId'];
                    $this->api->voidInvoice($invoice->getTenant(), $sefInvoiceId);
                }
            } catch (\Exception $e) {
                $this->flash->error('Storniranje fakture na SEF servisu nije uspelo: ' . $e->getMessage());
            }
        }
    }
}