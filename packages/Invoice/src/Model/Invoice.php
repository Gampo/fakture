<?php
namespace Fakture\Invoice\Model;

use Fakture\Client\Model\Client;
use Fakture\Tenant\Model\Tenant;
use Fakture\User\Model\User;
use Skeletor\Model\Model;

class Invoice extends Model
{
    const STATUS_NEW = 1;
    const STATUS_PREPARING = 2;
    const STATUS_READY = 3;
    const STATUS_LOCKED = 4; // not needed
    const STATUS_WAITING_PAYMENT = 5;
    const STATUS_FINISHED = 6;
    const STATUS_VOIDED = 7;
//    const STATUS_UNSENT = 5; // maybe use for mail queue

    const TYPE_LOCAL = 1;
    const TYPE_FOREIGN = 2;
    const TYPE_VOID = 3;

    const VAT_PERIOD_CALCULATION_BY_TURNOVER_DATE = 35;
    const VAT_PERIOD_CALCULATION_BY_PAYMENT_DATE = 432;
    const VAT_PERIOD_CALCULATION_BY_ISSUE_DATE = 3;



    /**
     * @param int $invoiceId
     * @param InvoiceServiceCollection $services
     * @param Client $client
     * @param float $amount
     * @param string $description
     * @param string $clientName
     * @param string $clientAddress
     * @param string $clientCity
     * @param string $clientCountry
     * @param string $clientVat
     * @param string $clientVatId
     * @param int $templateId
     * @param string $currency
     * @param int $type
     * @param int|null $recurring
     * @param int $status
     * @param string|null $sentAt
     * @param \DateTime|null $paidAt
     * @param string $issueLocation
     * @param string $turnoverLocation
     * @param \DateTime|null $deadlineDate
     * @param \DateTime|null $turnoverDate
     * @param \DateTime|null $issueDate
     * @param Tenant $tenant
     * @param string|null $invoiceCode
     * @param float|null $advanceAmount
     * @param User|null $invoiceUser
     * @param bool $sentToSef
     * @param ?Invoice $parentInvoice
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct(
        private int $invoiceId, private InvoiceServiceCollection $services, private Client $client, private float $amount,
        private string $description, private string $clientName, private string $clientAddress, private string $clientCity,
        private string $clientCountry, private string $clientVat, private string $clientVatId,
        private int $templateId, private string $currency, private int $type, private ?int $recurring, private int $status,
        private ?string $sentAt, private ?\DateTime $paidAt, private string $issueLocation, private string $turnoverLocation,
        private ?\DateTime $deadlineDate, private ?\DateTime $turnoverDate, private ?\DateTime $issueDate, private Tenant $tenant,
        private ?string $invoiceCode, private ?float $advanceAmount, private ?User $invoiceUser, private bool $sentToSef,
        private ?int $vatPeriodCalculationType,
        private ?Invoice $parentInvoice, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return int|null
     */
    public function getVatPeriodCalculationType(): ?int
    {
        return $this->vatPeriodCalculationType;
    }

    /**
     * @return Invoice|null
     */
    public function getParentInvoice(): ?Invoice
    {
        return $this->parentInvoice;
    }

    /**
     * @return bool
     */
    public function isSentToSef(): bool
    {
        return $this->sentToSef;
    }

    /**
     * @return User|null
     */
    public function getInvoiceUser(): ?User
    {
        return $this->invoiceUser;
    }

    public function getInvoiceCode()
    {
        if (!strlen($this->invoiceCode)) {
            return 'N/A';
        }

        if ($this->type === self::TYPE_FOREIGN && strlen($this->invoiceCode)) {
            return 'IF-' . $this->invoiceCode;
        }
        return $this->invoiceCode;
    }

    public static function getHrType($type)
    {
        return static::getHrTypes()[$type];
    }

    public static function getHrTypes()
    {
        return [
            static::TYPE_LOCAL => 'Domaći',
            static::TYPE_FOREIGN => 'Inostrani',
            static::TYPE_VOID => 'Storno',
        ];
    }

    public static function getHRVatPeriodCalculation($calculation)
    {
        return static::getHRVatPeriodCalculations()[$calculation];
    }

    public static function getHRVatPeriodCalculations()
    {
        return [
            static::VAT_PERIOD_CALCULATION_BY_PAYMENT_DATE => 'Prema datumu plaćanja',
            static::VAT_PERIOD_CALCULATION_BY_TURNOVER_DATE => 'Prema datumu prometa',
            static::VAT_PERIOD_CALCULATION_BY_ISSUE_DATE => 'Prema datumu izdavanja'
        ];
    }

    public static function getHrStatus($status)
    {
        return static::getHrStatuses()[$status];
    }

    public static function getHrStatuses()
    {
        return [
            static::STATUS_NEW => 'Nov',
            static::STATUS_PREPARING => 'U pripremi',
            static::STATUS_READY => 'Spreman',
//            static::STATUS_LOCKED => 'Zakljucana',
            static::STATUS_WAITING_PAYMENT => 'Čeka se uplata',
            static::STATUS_FINISHED => 'Finalizovano',
            static::STATUS_VOIDED => 'Stornirano',
        ];
    }



    /**
     * @return Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->invoiceId;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @return mixed
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * @return mixed
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * @return mixed
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * @return mixed
     */
    public function getClientVat()
    {
        return $this->clientVat;
    }

    public function getClientVatId()
    {
        return $this->clientVatId;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getRecurring()
    {
        return $this->recurring;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @return mixed
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * @return mixed
     */
    public function getIssueLocation()
    {
        return $this->issueLocation;
    }

    /**
     * @return mixed
     */
    public function getTurnoverLocation()
    {
        return $this->turnoverLocation;
    }

    /**
     * @return mixed
     */
    public function getDeadlineDate()
    {
        return $this->deadlineDate;
    }

    /**
     * @return mixed
     */
    public function getTurnoverDate()
    {
        return $this->turnoverDate;
    }

    /**
     * @return mixed
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * @return mixed
     */
    public function getAdvanceAmount()
    {
        return $this->advanceAmount;
    }

    /**
     * @return int
     */
    public function getAutoSend(): int
    {
        return $this->autoSend;
    }

    /**
     * @return int
     */
    public function getAutoSendStatus(): int
    {
        return $this->autoSendStatus;
    }

    /**
     * @return string
     */
    public function getAutoSendAt(): ?string
    {
        return $this->autoSendAt;
    }

    /**
     * @return string
     */
    public function getAutoGenerateAt(): ?string
    {
        return $this->autoGenerateAt;
    }
}