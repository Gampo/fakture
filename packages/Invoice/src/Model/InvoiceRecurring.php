<?php
namespace Fakture\Invoice\Model;

use Fakture\Client\Model\Client;
use Fakture\Tenant\Model\Tenant;
use Fakture\User\Model\User;
use Skeletor\Model\Model;

class InvoiceRecurring extends Model
{
    const TIMESLOT_LAST_WORK_DAY = 1;
    const TIMESLOT_LAST_DAY = 2;
    const TIMESLOT_DAYS_BEFORE = 3;

    /**
     * @param int $invoiceRecurringId
     * @param array $invoices
     * @param Tenant $tenant
     * @param int $autoSend
     * @param int $autoSendStatus
     * @param int $autoSendAt
     * @param int $autoGenerateAt
     * @param InvoiceServiceCollection $services
     * @param Client $client
     * @param string $description
     * @param int $templateId
     * @param string $currency
     * @param int $type
     * @param int $status
     * @param string $issueLocation
     * @param string $turnoverLocation
     * @param \DateTime|null $deadlineDate
     * @param \DateTime|null $turnoverDate
     * @param \DateTime|null $issueDate
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct(
        private int $invoiceRecurringId, private InvoiceRecurringServiceCollection $services, private Tenant $tenant,
        private int $autoSend, private int $autoSendStatus, private int $autoSendAt,
        private int $autoGenerateAt, private Client $client,
        private string $description, private int $templateId, private string $currency, private int $type, private int $status,
        private string $issueLocation, private string $turnoverLocation, private ?User $invoiceUser,
        private ?int $vatPeriodCalculationType,
        private ?\DateTime $deadlineDate, private ?\DateTime $turnoverDate, private ?\DateTime $issueDate,

        $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return int|null
     */
    public function getVatPeriodCalculationType(): ?int
    {
        return $this->vatPeriodCalculationType;
    }

    /**
     * @return array
     */
//    public function getInvoices(): array
//    {
//        return $this->invoices;
//    }

    public function getInvoiceUser(): ?User
    {
        return $this->invoiceUser;
    }

    /**
     * @return Tenant
     */
    public function getTenant(): Tenant
    {
        return $this->tenant;
    }

    /**
     * @return int
     */
    public function getAutoSend(): int
    {
        return $this->autoSend;
    }

    /**
     * @return int
     */
    public function getAutoSendStatus(): int
    {
        return $this->autoSendStatus;
    }

    /**
     * @return int
     */
    public function getAutoSendAt(): int
    {
        return $this->autoSendAt;
    }

    /**
     * @return int
     */
    public function getAutoGenerateAt(): int
    {
        return $this->autoGenerateAt;
    }

    public function getId(): int
    {
        return $this->invoiceRecurringId;
    }

    /**
     * @return InvoiceRecurringServiceCollection
     */
    public function getServices(): InvoiceRecurringServiceCollection
    {
        return $this->services;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getTemplateId(): int
    {
        return $this->templateId;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getIssueLocation(): string
    {
        return $this->issueLocation;
    }

    /**
     * @return string
     */
    public function getTurnoverLocation(): string
    {
        return $this->turnoverLocation;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeadlineDate(): ?\DateTime
    {
        return $this->deadlineDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getTurnoverDate(): ?\DateTime
    {
        return $this->turnoverDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getIssueDate(): ?\DateTime
    {
        return $this->issueDate;
    }
}