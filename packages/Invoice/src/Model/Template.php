<?php
declare(strict_types = 1);
namespace Fakture\Invoice\Model;

use Skeletor\Model\Model;

class Template extends Model
{
    private $invoiceTemplateId;

    private $label;

    private $template;

    /**
     * @param $invoiceTemplateId
     * @param $label
     * @param $template
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct($invoiceTemplateId, $label, $template, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->invoiceTemplateId = $invoiceTemplateId;
        $this->label = $label;
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->invoiceTemplateId;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

}