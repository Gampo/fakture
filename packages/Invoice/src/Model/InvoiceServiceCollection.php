<?php
namespace Fakture\Invoice\Model;

class InvoiceServiceCollection extends \Ramsey\Collection\AbstractCollection
{
    public function getType(): string
    {
        return 'Fakture\\Invoice\\Model\\InvoiceService';
    }
}