<?php
declare(strict_types = 1);
namespace Fakture\Invoice\Model;

use Skeletor\Model\Model;

class RecurringInvoice extends Model
{
    private $recurringInvoiceId;

    private $services;

    private $amount;

    private $description;

    private $clientName;

    private $clientAddress;

    private $clientCity;

    private $clientCountry;

    private $clientVat;

    private $client;

    private $templateId;

    private $currency;

    private $type;

    private $recurring;

    private $status;

    private $sentAt;

    private $paidAt;

    private $clientVatId;

    private $issueLocation;

    private $turnoverLocation;

    private $deadlineDate;

    private $turnoverDate;

    private $issueDate;

    private $tenant;

    /**
     * @param $invoiceId
     * @param $services
     * @param $amount
     * @param $description
     * @param $clientName
     * @param $clientAddress
     * @param $clientCity
     * @param $clientCountry
     * @param $clientVat
     * @param $clientId
     * @param $templateId
     * @param $currency
     * @param $type
     * @param $recurring
     * @param $status
     * @param $sentAt
     * @param $paidAt
     */
    public function __construct(
        $invoiceId, $services, $client, $amount, $description, $clientName, $clientAddress, $clientCity, $clientCountry,
        $clientVat, $clientVatId, $templateId, $currency, $type, $recurring, $status, $sentAt, $paidAt, $issueLocation,
        $turnoverLocation, $deadlineDate, $turnoverDate, $issueDate, $tenant, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->invoiceId = $invoiceId;
        $this->services = $services;
        $this->amount = $amount;
        $this->description = $description;
        $this->clientName = $clientName;
        $this->clientAddress = $clientAddress;
        $this->clientCity = $clientCity;
        $this->clientCountry = $clientCountry;
        $this->clientVat = $clientVat;
        $this->clientVatId = $clientVatId;
        $this->client = $client;
        $this->templateId = $templateId;
        $this->currency = $currency;
        $this->type = $type;
        $this->recurring = $recurring;
        $this->status = (int) $status;
        $this->sentAt = $sentAt;
        $this->paidAt = $paidAt;
        $this->issueLocation = $issueLocation;
        $this->turnoverLocation = $turnoverLocation;
        $this->deadlineDate = $deadlineDate;
        $this->turnoverDate = $turnoverDate;
        $this->issueDate = $issueDate;
        $this->tenant = $tenant;
    }

    public static function getHrType($type)
    {
        return static::getHrTypes()[$type];
    }

    public static function getHrTypes()
    {
        return [
            static::TYPE_LOCAL => 'Domaće',
            static::TYPE_FOREIGN => 'Strane',
        ];
    }

    public static function getHrStatus($status)
    {
        return static::getHrStatuses()[$status];
    }

    public static function getHrStatuses()
    {
        return [
            static::STATUS_NEW => 'Nova',
            static::STATUS_PREPARING => 'U pripremi',
            static::STATUS_READY => 'Spremna',
            static::STATUS_LOCKED => 'Zakljucana',
            static::STATUS_WAITING_PAYMENT => 'Čeka se uplata',
            static::STATUS_FINISHED => 'Kompletirana',
        ];
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->invoiceId;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @return mixed
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * @return mixed
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * @return mixed
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * @return mixed
     */
    public function getClientVat()
    {
        return $this->clientVat;
    }

    public function getClientVatId()
    {
        return $this->clientVatId;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getRecurring()
    {
        return $this->recurring;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @return mixed
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * @return mixed
     */
    public function getIssueLocation()
    {
        return $this->issueLocation;
    }

    /**
     * @return mixed
     */
    public function getTurnoverLocation()
    {
        return $this->turnoverLocation;
    }

    /**
     * @return mixed
     */
    public function getDeadlineDate()
    {
        return $this->deadlineDate;
    }

    /**
     * @return mixed
     */
    public function getTurnoverDate()
    {
        return $this->turnoverDate;
    }

    /**
     * @return mixed
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

}