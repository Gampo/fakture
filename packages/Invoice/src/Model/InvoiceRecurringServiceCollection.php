<?php
namespace Fakture\Invoice\Model;

class InvoiceRecurringServiceCollection extends \Ramsey\Collection\AbstractCollection
{
    public function getType(): string
    {
        return 'Fakture\\Invoice\\Model\\InvoiceRecurringServices';
    }
}