<?php
declare(strict_types = 1);
namespace Fakture\Invoice\Model;

class InvoiceBlank
{

    private $invoiceId;

    private $services;

    private $amount;

    private $description;

    private $clientName;

    private $clientAddress;

    private $clientCity;

    private $clientCountry;

    private $clientVat;

    private $client;

    private $templateId;

    private $currency;

    private $type;

    private $recurring;

    private $status;

    private $sentAt;

    private $paidAt;

    private $clientVatId;

    private $issueLocation;

    private $turnoverLocation;

    private $deadlineDate;

    private $turnoverDate;

    private $issueDate;

    private $tenant;

    private $createdAt;

    private $updatedAt;




    /**
     * @param $invoiceId
     * @param $services
     * @param $amount
     * @param $description
     * @param $clientName
     * @param $clientAddress
     * @param $clientCity
     * @param $clientCountry
     * @param $clientVat
     * @param $clientId
     * @param $templateId
     * @param $currency
     * @param $type
     * @param $recurring
     * @param $status
     * @param $sentAt
     * @param $paidAt
     */
    public function __construct(
        $invoiceId = 0, $services = [], $client = null, $amount = 0, $description = '', $clientName = '', $clientAddress = '',
        $clientCity = '', $clientCountry = '', $clientVat = '', $clientVatId = '', $templateId = '', $currency = '',
        $type = 1, $recurring = 0, $status = 1, $sentAt = null, $paidAt = null, $issueLocation = '',
        $turnoverLocation = '', $deadlineDate = null, $turnoverDate = null, $issueDate = null, $tenant = null,
        $createdAt = null, $updatedAt = null, $advanceAmount = null,
        $vatPeriodCalculationType = null,
    ) {
        $this->invoiceId = $invoiceId;
        $this->services = $services;
        $this->amount = $amount;
        $this->description = $description;
        $this->clientName = $clientName;
        $this->clientAddress = $clientAddress;
        $this->clientCity = $clientCity;
        $this->clientCountry = $clientCountry;
        $this->clientVat = $clientVat;
        $this->clientVatId = $clientVatId;
        $this->client = $client;
        $this->templateId = $templateId;
        $this->currency = $currency;
        $this->type = $type;
        $this->recurring = $recurring;
        $this->status = (int) $status;
        $this->sentAt = $sentAt;
        $this->paidAt = $paidAt;
        $this->issueLocation = $issueLocation;
        $this->turnoverLocation = $turnoverLocation;
        $this->deadlineDate = $deadlineDate;
        $this->turnoverDate = $turnoverDate;
        $this->issueDate = $issueDate;
        $this->tenant = $tenant;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->advanceAmount = $advanceAmount;
        $this->vatPeriodCalculationType = $vatPeriodCalculationType;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->invoiceId;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @return mixed
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * @return mixed
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * @return mixed
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * @return mixed
     */
    public function getClientVat()
    {
        return $this->clientVat;
    }

    public function getClientVatId()
    {
        return $this->clientVatId;
    }

    /**
     * @return int|null
     */
    public function getVatPeriodCalculationType(): ?int
    {
        return $this->vatPeriodCalculationType;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getRecurring()
    {
        return $this->recurring;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @return mixed
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * @return mixed
     */
    public function getIssueLocation()
    {
        return $this->issueLocation;
    }

    /**
     * @return mixed
     */
    public function getTurnoverLocation()
    {
        return $this->turnoverLocation;
    }

    /**
     * @return mixed
     */
    public function getDeadlineDate()
    {
        return $this->deadlineDate;
    }

    /**
     * @return mixed
     */
    public function getTurnoverDate()
    {
        return $this->turnoverDate;
    }

    /**
     * @return mixed
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getAdvanceAmount()
    {
        return $this->advanceAmount;
    }

    public function getAutoGenerateAt()
    {
        return 0;
    }

    public function getAutoSend()
    {
        return 0;
    }

    public function getInvoiceUser()
    {
        return null;
    }

}