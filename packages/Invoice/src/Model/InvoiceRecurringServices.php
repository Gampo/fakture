<?php
declare(strict_types = 1);
namespace Fakture\Invoice\Model;

use Skeletor\Model\Model;

class InvoiceRecurringServices extends Model
{
    private $invoiceRecurringServicesId;

    private $invoiceRecurringId;

    private $serviceId;

    private $serviceName;

    private $price;

    private $currency;

    private $qty;

    private $tax;

    /**
     * @param $invoiceServicesId
     * @param $invoiceId
     * @param $serviceId
     * @param $serviceName
     * @param $price
     * @param $currency
     * @param $qty
     */
    public function __construct($invoiceServicesId, $invoiceId, $serviceId, $serviceName, $price, $currency, int $qty, int $tax, $unitOfMeasure)
    {
        $this->invoiceServicesId = $invoiceServicesId;
        $this->invoiceId = $invoiceId;
        $this->serviceId = $serviceId;
        $this->serviceName = $serviceName;
        $this->price = $price;
        $this->currency = $currency;
        $this->qty = $qty;
        $this->tax = $tax;
        $this->unitOfMeasure = $unitOfMeasure;
    }

    /**
     * @return mixed
     */
    public function getUnitOfMeasure()
    {
        return $this->unitOfMeasure;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->invoiceServicesId;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @return mixed
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }

}