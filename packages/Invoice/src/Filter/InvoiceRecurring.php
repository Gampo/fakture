<?php
namespace Fakture\Invoice\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Skeletor\Filter\FilterInterface;
use Volnix\CSRF\CSRF;
use Fakture\Invoice\Validator\Invoice as InvoiceValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class InvoiceRecurring implements FilterInterface
{
    /**
     * @var InvoiceValidator
     */
    private $validator;

    public function __construct(\Fakture\Invoice\Validator\InvoiceRecurring $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();
        $type = $postData['type'] ?? null;

        foreach ($postData['selectedServices']['price'] as $key => $price) {
            $postData['selectedServices']['price'][$key] = str_replace(',', '', $price);
        }
        if($postData['vatPeriodCalculationType'] === '-1') {
            $postData['vatPeriodCalculationType'] = null;
        }

        $data = [
            'invoiceRecurringId' => (isset($postData['invoiceRecurringId'])) ? $int->filter($postData['invoiceRecurringId']) : null,
            'clientId' => $int->filter($postData['clientId']),
            'status' => $int->filter($postData['status']),
            'type' => $type ? $int->filter($postData['type']) : null,
            'currency' => $postData['currency'] ?? null,
            'issueDate' => $postData['issueDate'],
            'turnoverDate' => $postData['turnoverDate'],
            'deadlineDate' => $postData['deadlineDate'],
            'turnoverLocation' => $postData['turnoverLocation'],
            'issueLocation' => $postData['issueLocation'],
            'selectedServices' => $postData['selectedServices'],
            'invoiceUser' => $postData['invoiceUser'],
            'autoGenerateAt' => $postData['autoGenerateAt'],
            'autoSend' => $postData['autoSend'] ?? 0,
            'autoSendStatus' => 0,
            'autoSendAt' => $postData['autoSendAt'],
            'description' => $postData['description'],
            'templateId' => $int->filter($postData['templateId']),
            'vatPeriodCalculationType' => $postData['vatPeriodCalculationType'],
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            throw new ValidatorException();
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }

}