<?php
namespace Fakture\Invoice\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Skeletor\Filter\FilterInterface;
use Volnix\CSRF\CSRF;
use Fakture\Invoice\Validator\Invoice as InvoiceValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class Invoice implements FilterInterface
{
    /**
     * @var InvoiceValidator
     */
    private $validator;

    public function __construct(InvoiceValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();

        foreach ($postData['selectedServices']['price'] as $key => $price) {
            $postData['selectedServices']['price'][$key] = str_replace(',', '', $price);
        }
        $postData['advanceAmount'] = str_replace(',', '', $postData['advanceAmount']);
        $postData['amount'] = str_replace(',', '', $postData['amount']);

        if($postData['vatPeriodCalculationType'] === '-1') {
            $postData['vatPeriodCalculationType'] = null;
        }

        $data = [
            'invoiceId' => (isset($postData['invoiceId'])) ? $int->filter($postData['invoiceId']) : null,
            'clientId' => $int->filter($postData['clientId']),
            'clientName' => $postData['clientName'],
            'clientVat' => $alnum->filter($postData['clientVat']),
            'clientVatId' => $alnum->filter($postData['clientVatId']),
            'clientAddress' => $alnum->filter($postData['clientAddress']),
            'clientCity' => $postData['clientCity'],
            'clientCountry' => $postData['clientCountry'],
            'status' => $int->filter($postData['status']),
            'type' => $int->filter($postData['type']),
            'currency' => $postData['currency'] ?? null,
            'issueDate' => $postData['issueDate'],
            'turnoverDate' => $postData['turnoverDate'],
            'deadlineDate' => $postData['deadlineDate'],
            'turnoverLocation' => $postData['turnoverLocation'],
            'issueLocation' => $postData['issueLocation'],
            'selectedServices' => $postData['selectedServices'],
            'advanceAmount' => $postData['advanceAmount'],
            'amount' => $postData['amount'],
            'invoiceUser' => $postData['invoiceUser'],
            'description' => $postData['description'],
            'templateId' => $int->filter($postData['templateId']),
            'invoiceRecurring' => $postData['invoiceRecurring'] ?? null,
            'vatPeriodCalculationType' => $postData['vatPeriodCalculationType'],
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            throw new ValidatorException();
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }

}