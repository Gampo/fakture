<?php
namespace Fakture\Client\Repository;

use Fakture\Client\Mapper\ClientContract as Mapper;
use Fakture\Client\Mapper\InvoiceInfo;
use Fakture\Client\Model\ClientContract as Model;
use Fakture\Price\Model\Price;
use Fakture\Price\Repository\PriceRepository;
use Fakture\Service\Repository\ServiceRepository;
use Skeletor\TableView\Repository\TableViewRepository;
use Skeletor\Tenant\Repository\TenantRepository;

class ClientContractRepository extends TableViewRepository
{
    /**
     * @param Mapper $clientMapper
     * @param \DateTime $dt
     * @param TenantRepository $tenantRepo
     * @param InvoiceInfo $infoMapper
     */
    public function __construct(
        Mapper $mapper, \DateTime $dt,
        private PriceRepository $price,
        private ServiceRepository $service
    ) {
        parent::__construct($mapper, $dt);
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt', 'validUntil', 'signedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $data['service'] = null;
        if ($data['serviceId']) {
            $data['service'] = $this->service->getById($data['serviceId']);
        }
//        $data['price'] = $this->price->getByEntity($data['id'], Price::ENTITY_CLIENT, $data['tenantId'])[0];
        $data['price'] = $this->price->getById($data['priceId']);
        unset($data['priceId']);
        unset($data['serviceId']);

        return new Model(...$data);
    }

    public function getSearchableColumns(): array
    {
        return ['name', 'clientId', 'serviceId'];
    }

    /**
     * @param $data
     * @return bool|string
     * @throws \Exception
     */
    public function create($data): \Skeletor\Model\Model
    {
        if (isset($data['price'])) {
            $price = [
                'amount' => (int) $data['price']['amount'],
                'currency' => $data['price']['currency'],
                'tenantId' => $data['tenantId'],
                'entityType' => Price::ENTITY_CLIENT,
            ];
        }
        unset($data['price']);
        $clientContractId = $this->mapper->insert($data);
        $price['entityId'] = $clientContractId;
        if (isset($price['amount'])) {
            $priceId = $this->price->create($price)->getId();
        }
        $data['priceId'] = $priceId;
        $data['id'] = $clientContractId;
        $this->mapper->update($data);

        return $this->getById($clientContractId);
    }

    /**
     * @param $data
     * @return Model
     * @throws \Exception
     */
    public function update($data): Model
    {
        if (isset($data['price'])) {
            $price = $data['price'];
            unset($data['price']);
        }

        $contractId = $this->mapper->update($data);
        if (isset($price) && (int) $price['amount'] > 0 ) {
            $price['entityId'] = $contractId;
            $price['tenantId'] = $data['tenantId'];
            $price['entityType'] = Price::ENTITY_CLIENT;
            if ($price['priceId']) {
                $this->price->update($price);
            } else {
                $this->price->create($price);
            }
        }
        if (isset($price['amount'], $price['priceId']) && $price['amount'] === '') {
            $this->price->delete($price['priceId']);
        }

        return $this->getById($contractId);
    }
}
