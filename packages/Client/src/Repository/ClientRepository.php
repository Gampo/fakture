<?php
namespace Fakture\Client\Repository;

use Fakture\Client\Mapper\InvoiceInfo;
use Fakture\Client\Mapper\Client as Mapper;
use Fakture\Client\Model\Client as Model;
use Skeletor\TableView\Repository\TableViewRepository;
use Fakture\Tenant\Repository\TenantRepository;

class ClientRepository extends TableViewRepository
{
    /**
     * @param Mapper $clientMapper
     * @param \DateTime $dt
     * @param TenantRepository $tenantRepo
     * @param InvoiceInfo $infoMapper
     */
    public function __construct(
        private TenantRepository $tenantRepo,
        private ClientContractRepository $clientContractRepo,
        Mapper $clientMapper, \DateTime $dt, )
    {
        parent::__construct($clientMapper, $dt);
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $data['tenant'] = null;
        if ($data['tenantId']) {
            $data['tenant'] = $this->tenantRepo->getById($data['tenantId']);
        }
        $data['contractInfo'] = $this->clientContractRepo->fetchAll(['clientId' => $data['clientId'], 'tenantId' => $data['tenantId']]);
        unset($data['tenantId']);

        return new Model(...$data);
    }

    public function getSearchableColumns(): array
    {
        return ['name'];
    }

    /**
     * @param $data
     * @return bool|string
     * @throws \Exception
     */
    public function create($data): \Skeletor\Model\Model
    {
        if(!$this->mapper->inTransaction()) {
            $this->mapper->beginTransaction();
        }
        if(isset($data['clientContract']) && $data['clientContract']) {
            foreach ($data['clientContract']['serviceId'] as $key => $serviceId) {
                $contractData['serviceId'] = $serviceId;
                $contractData['tenantId'] = $data['tenantId'];
                $contractData['clientId'] = $data['clientId'];
                $contractData['deadline'] = (int) $data['clientContract']['deadline'][$key];
                $contractData['info'] = $data['clientContract']['info'][$key];
                $contractData['number'] = $data['clientContract']['number'][$key];
                $contractData['price']['amount'] = $data['clientContract']['price']['amount'][$key];
                $contractData['price']['currency'] = $data['clientContract']['price']['currency'][$key];

                $this->clientContractRepo->create($contractData);
            }
        }
        unset($data['clientContract']);
        $clientId = $this->mapper->insert($data);
        if($this->mapper->inTransaction()) {
            $this->mapper->commitTransaction();
        }

        return $this->getById($clientId);
    }

    /**
     * @param $data
     * @return Model
     * @throws \Exception
     */
    public function update($data): Model
    {
        $this->mapper->beginTransaction();
        if(isset($data['clientContract'])) {
            foreach ($data['clientContract']['serviceId'] as $key => $serviceId) {
                $contractData['serviceId'] = $serviceId;
                $contractData['tenantId'] = $data['tenantId'];
                $contractData['clientId'] = $data['clientId'];
                $contractData['deadline'] = (int) $data['clientContract']['deadline'][$key];
                $contractData['info'] = $data['clientContract']['info'][$key];
                $contractData['number'] = $data['clientContract']['number'][$key];
                $contractData['price']['amount'] = $data['clientContract']['price']['amount'][$key];
                $contractData['price']['currency'] = $data['clientContract']['price']['currency'][$key];

                if (isset($data['clientContract']['id'][$key])) {
                    $contractData['id'] = $data['clientContract']['id'][$key];
                    $contractData['price']['priceId'] = $data['clientContract']['price']['priceId'][$key];
                    $this->clientContractRepo->update($contractData);
                } else {
                    $this->clientContractRepo->create($contractData);
                }
            }
        }
        unset($data['clientContract']);
        $clientId = $this->mapper->update($data);
        $this->mapper->commitTransaction();

        return $this->getById($clientId);
    }
}
