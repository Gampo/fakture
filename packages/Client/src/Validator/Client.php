<?php
namespace Fakture\Client\Validator;

use Skeletor\Validator\ValidatorInterface;
use Volnix\CSRF\CSRF;

/**
 * Class Client.
 * User validator.
 *
 * @package Fakture\Client\Validator
 */
class Client implements ValidatorInterface
{
    private $messages = [];

    /**
     * @param CSRF $csrf
     */
    public function __construct(private CSRF $csrf) {}

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $emailValidator = new \Laminas\Validator\EmailAddress();
        $valid = true;

        if (strlen($data['name']) < 3) {
            $this->messages['name'][] = 'Name must be at least 3 characters long.';
            $valid = false;
        }
        if (strlen($data['address']) < 3) {
            $this->messages['address'][] = 'Invoice address must be at least 3 characters long.';
            $valid = false;
        }
        if (isset($data['email']) && strlen($data['email']) > 0 && !$emailValidator->isValid($data['email'])) {
            $this->messages['email'][] = 'Email you entered is not valid.';
            $valid = false;
        }
        if (strlen($data['city']) < 3) {
            $this->messages['city'][] = 'Invoice city must be at least 3 characters long.';
            $valid = false;
        }
//        if (strlen($data['zip']) < 5) {
//            $this->messages['zip'][] = 'Zip must be at least 5 characters long.';
//            $valid = false;
//        }
        if (strlen($data['country']) < 3) {
            $this->messages['country'][] = 'Invoice country must be at least 3 characters long.';
            $valid = false;
        }
        if ($data['type'] === \Fakture\Client\Model\Client::TYPE_LEGAL && strlen($data['pib']) < 9) {
            $this->messages['pib'][] = 'Pib must be exactly 9 characters long.';
            $valid = false;
        }
        if ($data['mb'] !== '' && strlen($data['mb']) < 8) {
            $this->messages['mb'][] = 'MB must be at least 8 characters long.';
            $valid = false;
        }

        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
