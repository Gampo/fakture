<?php
namespace Fakture\Client\Model;

use Skeletor\Model\Model;

class InvoiceInfo extends Model
{
    private $clientInvoiceInfoId;

    private $clientId;

    private $contractInfo;

    private $deadline;

    private $note;

    /**
     * @param $clientInvoiceInfoId
     * @param $clientId
     * @param $contractInfo
     * @param $deadline
     * @param $note
     */
    public function __construct($clientInvoiceInfoId, $clientId, $contractInfo, $deadline, $note) {
        $this->clientInvoiceInfoId = $clientInvoiceInfoId;
        $this->clientId = $clientId;
        $this->contractInfo = $contractInfo;
        $this->deadline = $deadline;
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->clientInvoiceInfoId;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return mixed
     */
    public function getContractInfo()
    {
        return $this->contractInfo;
    }

    /**
     * @return mixed
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }
}