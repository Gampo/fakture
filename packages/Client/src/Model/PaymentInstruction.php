<?php
namespace Fakture\Client\Model;

use Skeletor\Model\Model;

class PaymentInstruction extends Model
{
    /**
     * @param int $id
     * @param string $instruction
     * @param string $currency
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct(
        private int $id, private string $instruction, private string $currency, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInstruction(): string
    {
        return $this->instruction;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

}