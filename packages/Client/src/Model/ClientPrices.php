<?php
namespace Fakture\Client\Model;

use Fakture\Service\Model\Service;
use Fakture\Tenant\Model\Tenant;
use Skeletor\Model\Model;

class ClientPrices extends Model
{
    /**
     * @param $clientId
     * @param $name
     * @param $createdAt
     * @param $updatedAt
     * @param $invoiceInfo
     * @param $pib
     * @param $mb
     * @param $tenant
     * @param $type
     * @param $website
     */
    public function __construct(
        private int $clientPriceId,
        private Client $client,
        private string $currency,
        private Tenant $tenant,
        private float $amount,
        private ?Service $service,
        $createdAt,
        $updatedAt,
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getTenant()
    {
        return (int) $this->tenant;
    }

    public function getId()
    {
        return $this->clientPriceId;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

}