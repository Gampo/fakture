<?php
namespace Fakture\Client\Model;

use Skeletor\Model\Model;

class Client extends Model
{
    const TYPE_LEGAL = 1;

    const TYPE_PERSON = 2;

    private $clientId;

    private $name;

    private $website;

    private $pib;

    private $mb;

    private $type;

    private $tenant;

    private $address;

    private $city;

    private $zip;

    private $country;

    private $note;

    private $email;

    private $accountNumber;

    private $bank;

    private $logo;

    private $isActive;

    /**
     * @param $clientId
     * @param $name
     * @param $createdAt
     * @param $updatedAt
     * @param $invoiceInfo
     * @param $pib
     * @param $mb
     * @param $tenant
     * @param $type
     * @param $website
     */
    public function __construct(
        $clientId, $name, $createdAt, $updatedAt, $pib, $mb, $tenant, $type, $website, $address, $city,
        $zip, $country, $note, $email, $accountNumber, $logo, $bank, $isActive, array $contractInfo
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->clientId = $clientId;
        $this->name = $name;
        $this->pib = $pib;
        $this->address = $address;
        $this->city = $city;
        $this->zip = $zip;
        $this->country = $country;
        $this->mb = $mb;
        $this->type = $type;
        $this->tenant = $tenant;
        $this->website = $website;
        $this->note = $note;
        $this->email = $email;
        $this->accountNumber = $accountNumber;
        $this->logo = $logo;
        $this->bank = $bank;
        $this->isActive = $isActive;
        $this->contractInfo = $contractInfo;
    }

    /**
     * @return [ClientContract]
     */
    public function getContractInfo(int $serviceID = null)
    {
        if ($serviceID) {
            foreach ($this->contractInfo as $contract) {
                if ($contract->getService()->getId() === $serviceID) {
                    return $contract;
                }
            }
            return null;
        }
        return $this->contractInfo;
    }


    /**
     * @return mixed
     */
    public function isActive()
    {
        return (bool) $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return (int) $this->type;
    }

    /**
     * @return string[]
     */
    public static function getHrTypes(): array
    {
        return [
            self::TYPE_LEGAL => 'Pravno lice',
            self::TYPE_PERSON => 'Fizičko lice',
        ];
    }

    /**
     * @param $type
     * @return string
     */
    public static function getHrType($type): string
    {
        return self::getHrTypes()[$type];
    }

    public function getId()
    {
        return $this->clientId;
    }

    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPib()
    {
        return $this->pib;
    }

    /**
     * @return mixed
     */
    public function getMb()
    {
        return $this->mb;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }
}