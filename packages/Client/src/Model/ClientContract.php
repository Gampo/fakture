<?php
namespace Fakture\Client\Model;

use Fakture\Price\Model\Price;
use Fakture\Service\Model\Service;
use Skeletor\Model\Model;

class ClientContract extends Model
{
    /**
     * @param int $id
     * @param int $clientId
     * @param Service $service
     * @param int $deadline
     * @param Price $price
     * @param string $number
     * @param \DateTime|null $signedAt
     * @param \DateTime $validUntil
     * @param $tenantId
     * @param string $info
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct(
        private int $id, private int $clientId, private Service $service, private int $deadline, private Price $price,
        private string $number, private ?\DateTime $signedAt, private ?\DateTime $validUntil, private $tenantId,
        private string $info, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @return mixed
     */
    public function getTenantId()
    {
        return $this->tenantId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }

    /**
     * @return Service
     */
    public function getService(): Service
    {
        return $this->service;
    }

    /**
     * @return int
     */
    public function getDeadline(): int
    {
        return $this->deadline;
    }

    /**
     * @return Price
     */
    public function getPrice(): Price
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @return \DateTime|null
     */
    public function getSignedAt(): ?\DateTime
    {
        return $this->signedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getValidUntil(): ?\DateTime
    {
        return $this->validUntil;
    }
}