<?php
namespace Fakture\Client\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Skeletor\Filter\FilterInterface;
use Volnix\CSRF\CSRF;
use Fakture\Client\Validator\Client as ClientValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class Client implements FilterInterface
{
    /**
     * @var ClientValidator
     */
    private $validator;

    public function __construct(ClientValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();
        $data = [
            'clientId' => (isset($postData['clientId'])) ? $int->filter($postData['clientId']) : null,
            'name' => $postData['name'],
            'address' => $postData['address'],
            'email' => $postData['email'],
            'city' => $postData['city'],
            'country' => $postData['country'],
            'tenantId' => $int->filter($postData['tenantId']),
            'pib' => $postData['pib'],
            'mb' => $postData['mb'],
            'type' => $postData['type'],
            'website' => $postData['website'],
            'zip' => $postData['zip'],
            'note' => $postData['note'],
            'isActive' => $int->filter($postData['isActive']),
            'clientContract' => $postData['clientContract'] ?? null,
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            throw new ValidatorException();
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }

}