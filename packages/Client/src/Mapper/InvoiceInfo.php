<?php
namespace Fakture\Client\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Mapper\PDOWrite;

class InvoiceInfo extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'clientInvoiceInfo', 'clientInvoiceInfoId');
    }

    public function getByClientId(int $clientId)
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE `clientId` = :clientId";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute(['clientId' => $clientId]);
        $item = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$item) {
            throw new NotFoundException('InvoiceInfo Entity not found: ' . $clientId);
        }

        return $item;
    }
}