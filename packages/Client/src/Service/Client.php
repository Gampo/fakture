<?php
namespace Fakture\Client\Service;

use Fakture\Apr\Service\ClientSearch as Search;
use Fakture\Client\Repository\ClientRepository as ClientRepo;
use Fakture\Price\Service\ExchangeRate;
use Fakture\Price\Service\Price;
use Skeletor\TableView\Model\Column;
use Skeletor\TableView\Service\Table as TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Skeletor\User\Service\User;
use Fakture\Client\Filter\Client as ClientFilter;

class Client extends TableView
{
    /**
     * @param ClientRepo $clientRepo
     * @param User $user
     * @param Logger $logger
     * @param TenantRepo $tenantRepo
     * @param Search $clientSearch
     * @param ClientFilter $filter
     * @param ActivityRepository $activity
     */
    public function __construct(
        ClientRepo $clientRepo, User $user, Logger $logger, TenantRepo $tenantRepo, private Search $clientSearch,
        ClientFilter $filter, ActivityRepository $activity, private ExchangeRate $exchangeRate
    ) {
        parent::__construct($clientRepo, $user, $logger, $tenantRepo, $filter, null);
    }

    public function searchApr(string $query)
    {
        if (strlen(trim($query)) === 0) {
            throw new \InvalidArgumentException('Query cannot be empty');
        }
        $data = [];
        $clientData = $this->clientSearch->search($query);
        foreach ($clientData as $datum) {
            $data[] = [
                'vat' => $datum['vat'],
                'name' => $datum['name'],
                'mb' => $datum['mb'],
                'address' => $datum['address'],
                'city' => $datum['city'],
                'country' => 'Srbija',
            ];
        }

        return $data;
    }

    public function getEntityData(int $id, $currency = 'rsd')
    {
        /* @var \Fakture\Client\Model\Client $client */
        $client = $this->repo->getById($id);
        $contractInfo = [];
        foreach($client->getContractInfo() as $clientContract) {
            $cInfo = $clientContract->toArray();
            $cInfo['price']['amount'] = $this->exchangeRate->convert($clientContract->getPrice(), $currency);
            $cInfo['price']['currency'] = $currency;

            $contractInfo[] = $cInfo;
        }
        return [
            'clientVat' => $client->getPib(),
            'clientId' => $client->getMb(),
            'clientName' => $client->getName(),
            'clientAddress' => $client->getAddress(),
            'clientCity' => $client->getCity(),
            'clientCountry' => $client->getCountry(),
            'contract' => $contractInfo,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
//            ['name' => 'id', 'label' => '#'],
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'pib', 'label' => 'PIB'],
            ['name' => 'email', 'label' => 'Email'],
            ['name' => 'mb', 'label' => 'MB'],
            ['name' => 'type', 'label' => 'Type'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
            ['name' => 'createdAt', 'label' => 'Created at'],
        ];
        if ($this->user->isAdminLoggedIn()) {
            $columnDefinitions[] = ['name' => 'tenantId', 'label' => 'Tenant'];
        }

        return $columnDefinitions;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        if (!$this->user->isAdminLoggedIn()) {
            $filter = ['tenantId' => $this->user->getLoggedInTenantId()];
        }
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        if ($data['count'] === "0") {
            return [
                'count' => 0,
                'entities' => [],
            ];
        }
        $items = [];
        /* @var \Fakture\Client\Model\Client $client */
        foreach ($data['entities'] as $client) {
            $itemData = [
                'id' => $client->getId(),
                'name' =>  [
                    'value' => $client->getName(),
                    'editColumn' => true,
                ],
                'pib' => (string) $client->getPib(),
                'email' => (string) $client->getEmail(),
                'mb' => (string) $client->getMb(),
                'type' => $client->getHrTypes()[$client->getType()],
                'createdAt' => $client->getCreatedAt()->format('d.m.Y'),
                'updatedAt' => $client->getCreatedAt()->format('d.m.Y'),
            ];
            if ($this->user->isAdminLoggedIn()) {
                $itemData['tenantId'] = $client->getTenant()?->getName();
            }
            $items[] = [
                'columns' => $itemData,
                'id' => $client->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function delete(int $id)
    {
        $model = $this->repo->getById($id);
        if (!$this->user->isAdminLoggedIn() && $id === $model->getTenant()->getClientId()) {
            throw new \Exception('Ne mozete obrisati sopstvenu firmu.');
        }
        $this->createActivity(null, $model);
        $this->repo->delete($id);
    }
}