<?php
namespace Fakture\Tenant\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\PDOWrite;

class InvoiceInstruction extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'tenantForeignPaymentInstruction', 'id');
    }

    public function deleteForEntity(int $id, int $type): bool
    {
        $sql = "DELETE FROM `{$this->tableName}` WHERE `entityId` = {$id} AND `entityType` = {$type}";

        return $this->driver->prepare($sql)->execute();
    }

    public function getByEntity(int $id, int $type, int $tenantId): array
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE `entityId` = {$id} AND `entityType` = {$type}
        AND tenantId = {$tenantId}";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return  $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}