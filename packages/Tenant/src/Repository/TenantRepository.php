<?php
namespace Fakture\Tenant\Repository;

use Fakture\Client\Mapper\Client;
use Fakture\Client\Repository\ClientRepository;
use Fakture\Tenant\Model\Settings;
use Skeletor\Mapper\NotFoundException;
use Skeletor\Tenant\Mapper\Tenant as Mapper;
use Fakture\Tenant\Model\Tenant as Model;
use Laminas\Config\Config;
use Skeletor\TableView\Repository\TableViewRepository;

class TenantRepository extends TableViewRepository
{
    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var \DateTime
     */
    protected $dt;

    private $instructionRepo;

    private $clientMapper;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(
        Mapper $mapper, \DateTime $dt, InvoiceInstructionRepository $instructionRepo, Client $clientMapper
    ) {
        parent::__construct($mapper, $dt);
        $this->instructionRepo = $instructionRepo;
        $this->clientMapper = $clientMapper;
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $data['settings'] = Settings::fromJson($data['settings']);

        $data['invoiceInstructions'] = $this->instructionRepo->getByTenant($data['tenantId']);

        return new Model(...$data);
    }

    /**
     * @param $id
     * @return \Fakture\Tenant\Model\Tenant
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    public function update($data, $addressData = null): Model
    {
        //@TODO need check if transaction started
//        $this->mapper->beginTransaction();
        try {
            $clientData = $data['client'];
            $invoiceInstruction = $data['invoiceInstruction'];
            unset($data['client']);
            unset($data['invoiceInstruction']);
            $data['settings'] = json_encode($data['settings']);
            $existingIds = [];
            foreach ($this->instructionRepo->getByTenant($data['tenantId']) as $instruction) {
                $existingIds[$instruction->getId()] = $instruction->getId();
            }
            if (isset($invoiceInstruction['instruction'])) {
                foreach ($invoiceInstruction['instruction'] as $key => $instruction) {
                    $instructionData = [
                        'tenantId' => $data['tenantId'],
                        'currency' => $invoiceInstruction['currency'][$key],
                        'instruction' => nl2br($instruction),
                    ];
                    if (strlen($invoiceInstruction['id'][$key]) > 0) {
                        $instructionData['id'] = $invoiceInstruction['id'][$key];
                        $this->instructionRepo->update($instructionData);
                        unset($existingIds[$instructionData['id']]);
                    } else {
                        $this->instructionRepo->create($instructionData);
                    }
                }
            }
            foreach ($existingIds as $id) {
                $this->instructionRepo->delete($id);
            }
            $tenant = parent::update($data);
            $this->clientMapper->update($clientData);
            $tenant = $this->getById($tenant->getId());
        } catch (\Exception $e) {
//            $this->mapper->rollBackTransaction();
            throw $e;
        }
//        $this->mapper->commitTransaction();

        return $tenant;
    }

    public function create($data, $addressData = null): Model
    {
        if(!$this->mapper->inTransaction()) {
            $this->mapper->beginTransaction();
        }
        try {
            $clientData = $data['client'];
            $clientData['tenantId'] = 0;
            $invoiceInstruction = $data['invoiceInstruction'];
            $data['settings'] = json_encode($data['settings']);
            $clientId = $this->clientMapper->insert($clientData);
            unset($data['client']);
            unset($data['invoiceInstruction']);
            $data['clientId'] = $clientId;
            $tenant = parent::create($data);
            if (isset($invoiceInstruction['instruction'])) {
                foreach ($invoiceInstruction['instruction'] as $key => $instruction) {
                    $instructionData = [
                        'tenantId' => $data['tenantId'],
                        'currency' => $invoiceInstruction['currency'][$key],
                        'instruction' => nl2br($instruction),
                    ];
                    $this->instructionRepo->create($instructionData);
                }
            }
            $tenant = $this->getById($tenant->getId());
        } catch (\Exception $e) {
            if($this->mapper->inTransaction()) {
                $this->mapper->rollBackTransaction();
            }
            throw $e;
        }
        if($this->mapper->inTransaction()){
            $this->mapper->commitTransaction();
        }
        return $tenant;
    }

    public function delete($tenantId): bool
    {
        if ($tenantId === 1) {
            throw new \Exception('This item cannot be deleted.');
        }
        $this->mapper->beginTransaction();
        try {
            $tenant = $this->getById($tenantId);
            $this->clientMapper->delete($tenant->getId());
            $this->mapper->delete($tenantId);
        } catch (\Exception $e) {
            $this->mapper->rollBackTransaction();
            throw $e;
        }
        $this->mapper->commitTransaction();
        return true;
    }

    public function getSearchableColumns(): array
    {
        return ['name'];
    }
}
