<?php
namespace Fakture\Tenant\Repository;

use Skeletor\Mapper\NotFoundException;
use Fakture\Tenant\Mapper\InvoiceInstruction as Mapper;
use Fakture\Tenant\Model\InvoiceInstruction as Model;
use Laminas\Config\Config;
use Skeletor\TableView\Repository\TableViewRepository;

class InvoiceInstructionRepository extends TableViewRepository
{
    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var \DateTime
     */
    protected $dt;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $mapper, \DateTime $dt)
    {
        parent::__construct($mapper, $dt);
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(...$data);
    }

    /**
     * @param $id
     * @return \Fakture\Tenant\Model\Tenant
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    public function getByTenant(int $tenantId)
    {
        return $this->fetchAll(['tenantId' => $tenantId]);
    }

    public function getSearchableColumns(): array
    {
        return ['instruction'];
    }
}
