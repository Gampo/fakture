<?php
namespace Fakture\Tenant\Service;

use Fakture\Tenant\Repository\TenantRepository as TenantRepo;
use GuzzleHttp\Psr7\ServerRequest as Request;
use Skeletor\TableView\Model\Column;
use Skeletor\TableView\Service\Table as TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\User\Service\User;

class Tenant extends TableView
{

    /**
     * @param TenantRepo $repo
     * @param User $user
     * @param Logger $logger
     * @param TenantRepo $tenantRepo
     * @param ActivityRepository $activity
     */
    public function __construct(
        TenantRepo $repo, User $user, Logger $logger, ActivityRepository $activity, \Skeletor\Image\Service\Image $image,
        \Fakture\Tenant\Filter\Tenant $filter
    ) {
        parent::__construct($repo, $user, $logger, null, $filter, $activity);
        $this->image = $image;
    }

//    public function getEntityData(int $id)
//    {
//        $client = $this->repo->getById($id);
//
//        return [
//            'clientVat' => $client->getPib(),
//            'clientId' => $client->getMb(),
//        ];
//    }

    public function update(Request $request)
    {
        $data = $this->filter->filter($request);
        $oldModel = $this->repo->getById((int) $request->getAttribute('id'));
        if (isset($request->getUploadedFiles()['client']['logo'])) {
            if ($request->getUploadedFiles()['client']['logo']->getSize() !== 0) {
                $image = $this->image->processUploadedFile($request->getUploadedFiles()['client']['logo'], $oldModel->getId());
                $data['client']['logo'] = $image;
            }
        }
        $model = $this->repo->update($data);
        if ($this->activity) {
//            $this->activity->create('update', $model, $this->user->getLoggedInUserId(), $oldModel);
        }
        return $model;
    }

    public function create(Request $request)
    {
        $data = $this->filter->filter($request);
        $model = $this->repo->create($data);
//        if ($this->activity) {
//            $this->activity->create('create', $model, $this->user->getLoggedInUserId(), null);
//        }
        return $model;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
//        $filter = ['tenantId' => null];
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        if ($data['count'] === "0") {
            return [
                'count' => 0,
                'entities' => [],
            ];
        }
        $items = [];
        foreach ($data['entities'] as $tenant) {
            $item = [
                'columns' => [
                    'id' => $tenant->getId(),
                    'name' =>  [
                        'value' => $tenant->getName(),
                        'editColumn' => true,
                    ],
                    'createdAt' => $tenant->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $tenant->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $tenant->getId()
            ];
            $items[] = $item;
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'id', 'label' => '#'],
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
            ['name' => 'createdAt', 'label' => 'Created at'],
        ];

        return $columnDefinitions;
    }

}