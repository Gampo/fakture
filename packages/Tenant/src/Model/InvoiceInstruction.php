<?php
namespace Fakture\Tenant\Model;

class InvoiceInstruction extends \Skeletor\Model\Model
{
    public function __construct(
        private int $id, private int $tenantId, private string $currency, private string $instruction, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTenantId(): int
    {
        return $this->tenantId;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getInstruction(): string
    {
        return $this->instruction;
    }
}