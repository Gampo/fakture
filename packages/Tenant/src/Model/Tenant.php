<?php
namespace Fakture\Tenant\Model;

class Tenant extends \Skeletor\Tenant\Model\Tenant
{
    public function __construct(
        private ?int $clientId, private array $invoiceInstructions, private Settings $settings, $tenantId, $name,
        $description, $createdAt, $updatedAt
    ) {
        parent::__construct($tenantId, $name, $description, $createdAt, $updatedAt);
    }

    /**
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return $this->settings;
    }

    public function getInvoiceInstructions($currency = null)
    {
        if ($currency) {
            foreach ($this->invoiceInstructions as $instruction) {
                if ($currency === $instruction->getCurrency()) {
                    return $instruction;
                }
            }
            throw new \Exception('No instruction set for currency ' . $currency);
        }
        return $this->invoiceInstructions;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

}