<?php
namespace Fakture\Tenant\Model;

use Skeletor\Behaviors\Arrayable;
use Skeletor\Model\Model;

class Settings
{
    use Arrayable;

    const DEFAULT_VALUES = [
//                'tenantId' => 0,
        'useSef' => false, 'useSefToManageInput' => false, 'sefApiKey' => '', 'useSefForForeign' => false,
        'inVatSystem' => false, 'useForeignInvoice' => false, 'customStartingNumber' => 0, 'filenamePattern' => 'invoiceCode-clientName',
        'invoiceCodePattern' => 'invoiceCode-year', 'foreignInvoiceCodePattern' => 'IF-invoiceCode-year'
    ];

    public function __construct(
        private bool $useSef, private bool $useSefToManageInput, private string $sefApiKey, private bool $useSefForForeign,
        private bool $inVatSystem, private bool $useForeignInvoice, private string $filenamePattern, private string $invoiceCodePattern,
        private string $foreignInvoiceCodePattern, private int $customStartingNumber = 0,
        private ?string $vatExemption = null
    ) {

    }

    // hack to use arrayable
    public function getId()
    {
        return null;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public static function fromJson($json)
    {
        if (!$json || !strlen($json)) {
            return new self(...static::DEFAULT_VALUES);
        }
        $data = (array) json_decode($json);
        $diff = array_diff_key(static::DEFAULT_VALUES, $data);
        if (count($diff)) {
            $data = array_merge($data, $diff);
        }

        return new self(...$data);
    }

    /**
     * @return string
     */
    public function getInvoiceCodePattern(): string
    {
        return $this->invoiceCodePattern;
    }

    /**
     * @return string
     */
    public function getForeignInvoiceCodePattern(): string
    {
        return $this->foreignInvoiceCodePattern;
    }

    /**
     * @return string
     */
    public function getFilenamePattern(): string
    {
        return $this->filenamePattern;
    }

    /**
     * @return bool
     */
    public function useSefForForeign(): bool
    {
        return $this->useSefForForeign;
    }

    /**
     * @return bool
     */
    public function useForeignInvoice()
    {
        return $this->useForeignInvoice;
    }

    /**
     * @return int|null
     */
    public function getCustomStartingNumber(): ?int
    {
        return $this->customStartingNumber;
    }

    /**
     * @return bool
     */
    public function isInVatSystem(): bool
    {
        return $this->inVatSystem;
    }

    /**
     * @return mixed
     */
    public function getSefApiKey()
    {
        return $this->sefApiKey;
    }

    /**
     * @return mixed
     */
//    public function getClientId()
//    {
//        return $this->clientId;
//    }

    /**
     * @return bool
     */
    public function useSef(): bool
    {
        return $this->useSef;
    }

    /**
     * @return bool
     */
    public function useSefToManageInput(): bool
    {
        return $this->useSefToManageInput;
    }

    /**
     * @return mixed
     */
    public function getVatExemption()
    {
        return $this->vatExemption;
    }


}