<?php
namespace Fakture\Tenant\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Skeletor\Filter\FilterInterface;
use Volnix\CSRF\CSRF;
use Fakture\Tenant\Validator\Tenant as TenantValidator;
use Fakture\Client\Validator\Client as ClientValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class Tenant implements FilterInterface
{
    /**
     * @var TenantValidator
     */
    private $validator;

    private $clientValidator;

    public function __construct(TenantValidator $validator, ClientValidator $clientValidator)
    {
        $this->validator = $validator;
        $this->clientValidator = $clientValidator;
    }

    public function getErrors()
    {
        return array_merge($this->validator->getMessages(), $this->clientValidator->getMessages());
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();
        $data = [
            'tenantId' => $postData['tenantId'],
            'name' => $postData['name'],
            'description' => $postData['description'],
            'settings' => [
                'useSef' => (isset($postData['settings']['useSef']) && $postData['settings']['useSef'] === 'on') ? 1 : 0,
                'useForeignInvoice' => (isset($postData['settings']['useForeignInvoice']) && $postData['settings']['useForeignInvoice'] === 'on') ? 1 : 0,
                'useSefToManageInput' => (isset($postData['settings']['useSefToManageInput']) && $postData['settings']['useSefToManageInput'] === 'on') ? 1 : 0,
                'sefApiKey' => $postData['settings']['sefApiKey'],
                'invoiceCodePattern' => $postData['settings']['invoiceCodePattern'],
                'foreignInvoiceCodePattern' => $postData['settings']['foreignInvoiceCodePattern'],
                'filenamePattern' => $postData['settings']['filenamePattern'],
                'inVatSystem' => (isset($postData['settings']['inVatSystem']) && $postData['settings']['inVatSystem'] === 'on') ? 1 : 0,
                'customStartingNumber' => $int->filter($postData['settings']['customStartingNumber']),
            ],
            'client' => $postData['client'],
            'invoiceInstruction' => isset($postData['invoiceInstruction']) ? $postData['invoiceInstruction'] : [],
        ];
        if((isset($postData['settings']['vatExemption']) && $postData['settings']['vatExemption'] !== '')) {
            $data['settings']['vatExemption'] = $postData['settings']['vatExemption'];
        }
        $data['client'][CSRF::TOKEN_NAME] = $postData[CSRF::TOKEN_NAME];
        $valid = true;
        if (!$this->clientValidator->isValid($data['client'])) {
            $valid = false;
        }
        if (!$this->validator->isValid($data)) {
            $valid = false;
        }
        if (!$valid) {
            throw new ValidatorException();
        }
        unset($data['client'][CSRF::TOKEN_NAME]);

        return $data;
    }

}