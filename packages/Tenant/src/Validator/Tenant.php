<?php

namespace Fakture\Tenant\Validator;

use Skeletor\Validator\ValidatorInterface;
use Fakture\Tenant\Repository\TenantRepository;
use Volnix\CSRF\CSRF;

/**
 * Class Tenant.
 * User validator.
 *
 * @package Fakture\Tenant\Validator
 */
class Tenant implements ValidatorInterface
{
    /**
     * @var TenantRepository
     */
    private $repo;

    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * User constructor.
     *
     * @param TenantRepository $repo
     * @param CSRF $csrf
     */
    public function __construct(TenantRepository $repo, CSRF $csrf)
    {
        $this->repo = $repo;
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $emailValidator = new \Laminas\Validator\EmailAddress();
        $valid = true;

        if (strlen($data['name']) < 3) {
            $this->messages['name'][] = 'Name must be at least 3 characters long.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
