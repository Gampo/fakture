<?php
namespace Fakture\Price\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\PDOWrite;

class ExchangeRate extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'exchangeRates', 'id');
    }

    public function getByDate($date, $currency)
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE DATE(createdAt) = '{$date}' AND currency = '{$currency}'";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}