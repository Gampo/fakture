<?php
namespace Fakture\Price\Service;

use \Fakture\Price\Model\Price as PriceModel;

class Price
{
    public function __construct(private ExchangeRate $exchangeRate)
    {}

    public function convert(PriceModel $price, $toCurrency = 'rsd')
    {
        if ($price->getCurrency() === $toCurrency) {
            return $price->getAmount();
        }

        return $this->exchangeRate->convert($price, $toCurrency);
    }

    public function calculatePrice(PriceModel $price, $date = null)
    {
        throw new \Exception('Deprecated call.');
        if ($price->getCurrency() === 'rsd') {
            return $price->getAmount();
        }

        return $this->mockExchange($price, $date);
    }

    private function mockExchange(PriceModel $price, $date = null)
    {
        return $this->exchangeRate->convertForDate($price->getCurrency(), $price->getAmount(), $date);
    }
}