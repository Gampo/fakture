<?php

namespace Fakture\Price\Service;

use Fakture\Price\Model\Price as PriceModel;
use Fakture\Price\Repository\ExchangeRateRepository;
use GuzzleHttp\Client;

class ExchangeRate
{
    const REDIS_KEY = 'fakture#exchangeRatesNbs';

    private $rates = [];

    public function __construct(
        private Client $httpClient, private \Redis $redis, private \DateTime $dt, private ExchangeRateRepository $exchangeRateRepo
    ) {
        $this->fetchData();
    }

//    public function fetchRates()
//    {
//        $this->rates = $this->redis->get(self::REDIS_KEY);
//        if ($this->rates === false) {
//            $this->fetchData();
//            $this->redis->set(self::REDIS_KEY, serialize($this->rates), 60*60*6);
//        } else {
//            $this->rates = unserialize($this->rates);
//        }
//        return $this->rates;
//    }

    private function fetchData($date = null)
    {
        if (!$date) {
            $date = $this->dt->format('Y-m-d');
            if ($this->dt->format('H') < 8) {
                $this->dt->modify('-1 day');
                $date = $this->dt->format('Y-m-d');
            }
        }
//        else {
//            $date = $date->format('Y-m-d');
//        }
        $foundRate = $this->exchangeRateRepo->getByDate($date, 'eur');

        if (!$foundRate) {
//            $source = 'https://webappcenter.nbs.rs/WebApp/ExchangeRate/ExchangeRate/MiddleExchangeRate?';
            $source = 'https://www.kamatica.com/kursna-lista/nbs';
            try {
                $response = $this->httpClient->get($source);
                $dom = new \DOMDocument();
                $html = $response->getBody()->getContents();
                $internalErrors = \libxml_use_internal_errors(true);
                $dom->loadHTML($html);
                foreach (\Fakture\Price\Model\Price::getCurrencies() as $cur) {
                    if (strtoupper($cur) === 'RSD') {
                        continue;
                    }
                    $this->rates[strtolower($cur)] = $dom->getElementById('c_' . strtoupper($cur))->textContent;
                }
                foreach ($this->rates as $currency => $rate) {
                    $this->exchangeRateRepo->create([
                        'currency' => $currency,
                        'rate' => $rate,
                    ]);
                    $this->rates[$currency] = $rate;
                }
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                die();
            }
        } else {
            foreach (\Fakture\Price\Model\Price::getCurrencies() as $currency) {
                if (in_array($currency, ['rsd'])) {
                    continue;
                } // @TODO
                $foundRate = $this->exchangeRateRepo->getByDate($date, $currency);
                $this->rates[$currency] = $foundRate[0]['rate'];
            }
        }
    }

    /**
     * @TODO remove date param
     *
     * @param $date
     * @param $source
     * @param $amount
     * @return float
     */
    public function convertForDate($source, $amount, $date = null)
    {
        if ($source === 'rsd') {
            return $amount;
        }
        if (!$date) {
            $date = $this->dt->format('Y-m-d');
        }
        $foundRate = $this->rates[$source];
//        $converted = (float) str_replace(',','.', $foundRate['rate']) * $amount;
        $converted = (float) str_replace(',','.', $foundRate) * $amount;

        return round($converted, 2);
    }

    /**
     *
     * @param $source
     * @param $amount
     * @return float
     */
    public function convert(PriceModel $price, $toCurrency = 'rsd')
    {
        $fromCurrency = strtolower($price->getCurrency());
        if ($toCurrency === $fromCurrency) {
            return round($price->getAmount(), 2);
        }
        if ($toCurrency === 'rsd' && !isset($this->rates[$fromCurrency]) ||
            $fromCurrency === 'rsd' && !isset($this->rates[$toCurrency])) {
            throw new \Exception(sprintf('Rate for exchange from %s to %s not found: ', $fromCurrency, $toCurrency));
        }
        if ($toCurrency === 'rsd') {
            $foundRate = (float) str_replace(',','.', $this->rates[$fromCurrency]);

            return round($price->getAmount() * $foundRate, 2);
        }
        if ($fromCurrency === 'rsd') {
            $foundRate = (float) str_replace(',','.', $this->rates[$toCurrency]);

            return round($price->getAmount() / $foundRate, 2);
        }
        // convert to rsd from source
        $foundRate = (float) str_replace(',','.', $this->rates[$fromCurrency]);
        $priceInRsd = round($price->getAmount() * $foundRate, 2);

        // convert to target from rsd
        $foundRate = (float) str_replace(',','.', $this->rates[$toCurrency]);

        return round($priceInRsd / $foundRate, 2);
    }
}