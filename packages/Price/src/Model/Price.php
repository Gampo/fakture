<?php
namespace Fakture\Price\Model;

use Skeletor\Model\Model;

class Price extends Model
{
    CONST ENTITY_SERVICE = 1;
    CONST ENTITY_CLIENT = 2;

    const CURRENCY_RSD = 'rsd';
    const CURRENCY_EUR = 'eur';
    const CURRENCY_USD = 'usd';
    const CURRENCY_GBP = 'gbp';

    private $priceId;

    private $currency;

    private $amount;

    private $description;

    private $tenant;

    private $entityId;

    private $entityType;

    /**
     * @param $priceId
     * @param $currency
     * @param $amount
     * @param $description
     * @param $tenant
     * @param $serviceId
     */
    public function __construct($priceId, $currency, $amount, $description, $tenant, $entityId, $entityType, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->priceId = $priceId;
        $this->currency = $currency;
        $this->amount = $amount;
        $this->description = $description;
        $this->tenant = $tenant;
        $this->entityId = $entityId;
        $this->entityType = $entityType;
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @return mixed
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int) $this->priceId;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    public static function getHrCurrencies()
    {
        return [
            static::CURRENCY_RSD => 'RSD',
            static::CURRENCY_EUR => 'EUR',
            static::CURRENCY_USD => 'USD',
            static::CURRENCY_GBP => 'GBP',
        ];
    }

    public static function getCurrencies()
    {
        return [
            static::CURRENCY_RSD, static::CURRENCY_EUR, static::CURRENCY_USD, static::CURRENCY_GBP,
        ];
    }
}