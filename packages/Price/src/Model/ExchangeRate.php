<?php
namespace Fakture\Price\Model;

use Skeletor\Model\Model;

class ExchangeRate extends Model
{
    /**
     * @param $priceId
     * @param $currency
     * @param $amount
     * @param $description
     * @param $tenant
     * @param $serviceId
     */
    public function __construct(
        private $id, private $currency, private $rate, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

}