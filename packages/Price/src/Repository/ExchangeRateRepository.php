<?php
namespace Fakture\Price\Repository;

use Skeletor\Repository\RepositoryInterface;
use Skeletor\Mapper\NotFoundException;
use Fakture\Price\Mapper\ExchangeRate as Mapper;
use Fakture\Price\Model\ExchangeRate as Model;
use Skeletor\User\Service\User;
use Laminas\Config\Config;

class ExchangeRateRepository implements RepositoryInterface
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     */
    public function __construct(Mapper $mapper, \DateTime $dt)
    {
        $this->mapper = $mapper;
        $this->dt = $dt;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $id
     * @return Model
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data, $addressData = null): Model
    {
        $this->mapper->update($data);

        return $this->getById($data['id']);
    }

    public function create($data, $addressData = null): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt', 'date'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(...$data);
    }

    /**
     * Deletes a single entity.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id): bool
    {
        return $this->mapper->delete($id);
    }

    public function getByDate($date, $currency)
    {
        return $this->mapper->getByDate($date, $currency);
    }

}
