<?php
namespace Fakture\Price\Repository;

use Fakture\Price\Model\Price;
use Skeletor\Repository\RepositoryInterface;
use Skeletor\Mapper\NotFoundException;
use Fakture\Price\Mapper\Price as Mapper;
use Fakture\Price\Model\Price as Model;
use Skeletor\Tenant\Repository\TenantRepository;
use Skeletor\User\Service\User;
use Laminas\Config\Config;

class PriceRepository implements RepositoryInterface
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    private $tenantRepo;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $serviceMapper, \DateTime $dt, Config $config, TenantRepository $tenantRepo)
    {
        $this->mapper = $serviceMapper;
        $this->dt = $dt;
        $this->config = $config;
        $this->tenantRepo = $tenantRepo;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $id
     * @return Model
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data, $addressData = null): Model
    {
        $this->mapper->update($data);

        return $this->getById($data['priceId']);
    }

    public function create($data, $addressData = null): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $data['tenant'] = null;
        if ($data['tenantId']) {
            $data['tenant'] = $this->tenantRepo->getById($data['tenantId']);
        }
        unset($data['tenantId']);

        return new Model(...$data);
    }

    /**
     * Deletes a single entity.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id): bool
    {
        return $this->mapper->delete($id);
    }

    public function getByEntity($entityId, $entityType, $tenantId)
    {
        $prices = [];
        foreach ($this->mapper->getByEntity($entityId, $entityType, $tenantId) as $priceData) {
            $priceData['tenant'] = $this->tenantRepo->getById($tenantId);
            unset($priceData['tenantId']);
            $prices[] = new Price(...$priceData);
        }
        return $prices;
    }

    public function deleteForEntity($entityId, $entityType)
    {
        return $this->mapper->deleteForEntity($entityId, $entityType);
    }
}
