<?php
namespace Fakture\Service\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Skeletor\Filter\FilterInterface;
use Volnix\CSRF\CSRF;
use Fakture\Service\Validator\Service as ServiceValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class Service implements FilterInterface
{
    /**
     * @var ServiceValidator
     */
    private $validator;

    public function __construct(ServiceValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();
        $data = [
            'serviceId' => (isset($postData['serviceId'])) ? $int->filter($postData['serviceId']) : null,
            'name' => $postData['name'],
            'description' => $postData['description'],
            'type' => $postData['type'],
            'groupId' => $alnum->filter($postData['groupId']),
            'price' => $postData['price'],
            'currency' => $postData['currency'],
            'tenantId' => $int->filter($postData['tenantId']),
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            throw new ValidatorException();
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }

}