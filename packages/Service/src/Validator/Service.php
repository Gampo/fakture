<?php

namespace Fakture\Service\Validator;

use Skeletor\Validator\ValidatorInterface;
use Fakture\Service\Repository\ServiceRepository;
use Volnix\CSRF\CSRF;

/**
 * Class Service.
 * User validator.
 *
 * @package Fakture\Service\Validator
 */
class Service implements ValidatorInterface
{
    /**
     * @var ServiceRepository
     */
    private $repo;

    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * User constructor.
     *
     * @param ServiceRepository $repo
     * @param CSRF $csrf
     */
    public function __construct(ServiceRepository $repo, CSRF $csrf)
    {
        $this->repo = $repo;
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;

        if (strlen($data['name']) < 3) {
            $this->messages['name'][] = 'Name must be at least 3 characters long.';
            $valid = false;
        }

        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
