<?php
namespace Fakture\Service\Model;

use Skeletor\Model\Model;

class Group extends Model
{
    private $groupId;

    private $description;

    private $name;

    private $tenant;

    /**
     * @param $groupId
     * @param $description
     * @param $name
     * @param $tenant
     */
    public function __construct($groupId, $description, $name, $tenant, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->groupId = $groupId;
        $this->description = $description;
        $this->name = $name;
        $this->tenant = $tenant;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return (int) $this->groupId;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }

}