<?php
namespace Fakture\Service\Model;

use Fakture\Price\Model\Price;
use Skeletor\Model\Model;

class Service extends Model
{
    const TYPE_STANDARD = 1;

    private $serviceId;

    private $description;

    private $type;

    private $name;

    private $group;

    private $tenant;

    private $price;

    /**
     * @param $serviceId
     * @param $description
     * @param $type
     * @param $name
     * @param $group
     * @param $tenant
     */
    public function __construct(
        $serviceId, $description, $type, $name, $group, $tenant, ?Price $price, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->serviceId = $serviceId;
        $this->description = $description;
        $this->type = $type;
        $this->name = $name;
        $this->group = $group;
        $this->tenant = $tenant;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int) $this->serviceId;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return (int) $this->type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @return mixed
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

}