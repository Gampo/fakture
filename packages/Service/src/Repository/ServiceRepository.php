<?php
namespace Fakture\Service\Repository;

use Fakture\Price\Model\Price;
use Fakture\Service\Mapper\Service as Mapper;
use Fakture\Service\Model\Service as Model;
use Skeletor\TableView\Repository\TableViewRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Fakture\Price\Repository\PriceRepository as PriceRepo;

class ServiceRepository extends TableViewRepository
{
    private $tenantRepo;

    private $priceRepo;

    private $groupRepo;

    /**
     * @param Mapper $serviceMapper
     * @param \DateTime $dt
     * @param TenantRepo $tenantRepo
     * @param PriceRepo $priceRepo
     * @param GroupRepository $groupRepo
     */
    public function __construct(
        Mapper $serviceMapper, \DateTime $dt, TenantRepo $tenantRepo, PriceRepo $priceRepo, GroupRepository $groupRepo
    ) {
        parent::__construct($serviceMapper, $dt);
        $this->tenantRepo = $tenantRepo;
        $this->priceRepo = $priceRepo;
        $this->groupRepo = $groupRepo;
    }

    public function update($data, $addressData = null): Model
    {
        $serviceId = $this->mapper->update([
            'serviceId' => $data['serviceId'],
            'name' => $data['name'],
            'description' => $data['description'],
            'groupId' => $data['groupId'],
            'type' => $data['type'],
            'tenantId' => $data['tenantId']
        ]);
        $this->priceRepo->deleteForEntity($serviceId, Price::ENTITY_SERVICE);
        $priceData = [
            'currency' => $data['currency'],
            'amount' => $data['price'],
            'description' => '',
            'tenantId' => $data['tenantId'],
            'entityId' => $serviceId,
            'entityType' => Price::ENTITY_SERVICE,
        ];
        $this->priceRepo->create($priceData);

        return $this->getById($data['serviceId']);
    }

    public function create($data, $addressData = null): Model
    {
        $serviceId = $this->mapper->insert([
            'name' => $data['name'],
            'description' => $data['description'],
            'groupId' => $data['groupId'],
            'type' => $data['type'],
            'tenantId' => $data['tenantId']
        ]);
        $priceData = [
            'currency' => $data['currency'],
            'amount' => $data['price'],
            'description' => '',
            'tenantId' => $data['tenantId'],
            'entityId' => $serviceId,
            'entityType' => Price::ENTITY_SERVICE,
        ];
        $this->priceRepo->create($priceData);

        return $this->getById($serviceId);
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $tenant = null;
        if ($data['tenantId']) {
            $tenant = $this->tenantRepo->getById($data['tenantId']);
        }
        $price = null;
        $prices = $this->priceRepo->fetchAll(['entityId' => $data['serviceId'], 'tenantId' => $data['tenantId'], 'entityType' => Price::ENTITY_SERVICE]);
        if (isset($prices[0])) {
            $price = $prices[0];
        }
        $group = null;
        if ($data['groupId']) {
            $group = $this->groupRepo->getById($data['groupId']);
        }

        return new Model(
            $data['serviceId'],
            $data['description'],
            $data['type'],
            $data['name'],
            $group,
            $tenant,
            $price,
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function getSearchableColumns(): array
    {
        return ['name', 'description'];
    }

}
