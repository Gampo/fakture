<?php
namespace Fakture\Service\Repository;

use Fakture\Service\Mapper\Group as Mapper;
use Fakture\Service\Model\Group as Model;
use Skeletor\TableView\Repository\TableViewRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Laminas\Config\Config;

class GroupRepository extends TableViewRepository
{
    private $tenantRepo;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $groupMapper, \DateTime $dt, TenantRepo $tenantRepo) {
        parent::__construct($groupMapper, $dt);
        $this->tenantRepo = $tenantRepo;
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $tenant = null;
        if ($data['tenantId']) {
            $tenant = $this->tenantRepo->getById($data['tenantId']);
        }

        return new Model(
            $data['serviceGroupId'],
            $data['description'],
            $data['name'],
            $tenant,
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function getSearchableColumns(): array
    {
        return ['name', 'description'];
    }
}
