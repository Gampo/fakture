<?php
namespace Fakture\Service\Service;

use Fakture\Price\Model\Price;
use Fakture\Service\Repository\GroupRepository as GroupRepo;
use Fakture\Service\Repository\ServiceRepository as ServiceRepo;
use Skeletor\TableView\Model\Column;
use Skeletor\TableView\Service\Table as TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Skeletor\User\Service\User;
use Fakture\Service\Filter\Service as ServiceFilter;

class Service extends TableView
{
    private $groupRepo;

    private $price;

    /**
     * @param ServiceRepo $serviceRepo
     * @param User $user
     * @param Logger $logger
     * @param TenantRepo $tenantRepo
     * @param ServiceFilter $filter
     * @param ActivityRepository $activity
     */
    public function __construct(
        ServiceRepo $serviceRepo, User $user, Logger $logger, TenantRepo $tenantRepo, GroupRepo $groupRepo,
        ServiceFilter $filter, ActivityRepository $activity, \Fakture\Price\Service\Price $price, private \DateTime $dt
    ) {
        parent::__construct($serviceRepo, $user, $logger, $tenantRepo, $filter, null);
        $this->groupRepo = $groupRepo;
        $this->price = $price;
    }

    public function getGroups()
    {
        $filter = [];
        if ($this->user->isAdminLoggedIn()) {
            $filter = ['tenantId' => $this->user->getLoggedInTenantId()];
        }

        return $this->groupRepo->fetchAll($filter);
    }

    public function getDataForInvoice($serviceIds)
    {
        $data = [];
        foreach (explode(',', $serviceIds) as $serviceId) {
            $service = $this->getById((int) $serviceId);
            $data[] = [
                'name' => $service->getName(),
                'price' => $this->price->calculatePrice($service->getPrice()),
                'serviceId' => $service->getId(),
            ];
        }

        return $data;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        if ($data['count'] === "0") {
            return [
                'count' => 0,
                'entities' => [],
            ];
        }
        $items = [];
        foreach ($data['entities'] as $service) {
            $item = [
                'id' => $service->getId(),
                'name' =>  [
                    'value' => $service->getName(),
                    'editColumn' => true,
                ],
                'group' => (string) $service->getGroup()?->getName(),
                'price' => $service->getPrice()?->getAmount(),
                'currency' => $service->getPrice()?->getCurrency(),
                'updatedAt' => $service->getCreatedAt()->format('d.m.Y'),
            ];
            if ($this->user->isAdminLoggedIn()) {
                $item['tenantId'] = $service->getTenant()?->getName();
            }
            $items[] = [
                'columns' => $item,
                'id' => $service->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
//            ['name' => 'id', 'label' => '#'],
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'price', 'label' => 'Price'],
            ['name' => 'currency', 'label' => 'Currency'],
            ['name' => 'group', 'label' => 'Group'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
        ];
        if ($this->user->isAdminLoggedIn()) {
            $columnDefinitions[] = ['name' => 'tenantId', 'label' => 'Tenant'];
        }

        return $columnDefinitions;
    }

    public function getEntities($filter = [], $limit = null, $order = null, $currency = 'rsd')
    {
        $items = [];
        if (isset($this->tenantRepo) && !$this->user->isAdminLoggedIn()) {
            $filter['tenantId'] = $this->user->getLoggedInTenantId();
        }
        foreach ($this->repo->fetchAll($filter, $limit, $order) as $entity) {
            $data = $entity->toArray();
            $data['price'] = $this->price->convert($entity->getPrice(), $currency);
            $data['currency'] = $currency;
            $items[] = $data;
        }

        return $items;
    }
}