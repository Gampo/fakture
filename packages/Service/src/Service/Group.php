<?php
namespace Fakture\Service\Service;

use Fakture\Price\Model\Price;
use Fakture\Service\Repository\GroupRepository as GroupRepo;
use Skeletor\TableView\Model\Column;
use Skeletor\TableView\Service\Table as TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\Tenant\Repository\TenantRepository as TenantRepo;
use Skeletor\User\Service\User;

class Group extends TableView
{
    /**
     * @param GroupRepo $serviceRepo
     * @param User $user
     * @param Logger $logger
     * @param TenantRepo $tenantRepo
     * @param ActivityRepository $activity
     */
    public function __construct(
        GroupRepo $groupRepo, User $user, Logger $logger, TenantRepo $tenantRepo, ActivityRepository $activity
    ) {
        parent::__construct($groupRepo, $user, $logger, $tenantRepo, null, $activity);
    }

    public function getDataForInvoice($serviceIds)
    {
        // @todo calculate price :)
        $data = [];
        foreach (explode(',', $serviceIds) as $serviceId) {
            $service = $this->getById((int) $serviceId);
            $data[] = [
                'name' => $service->getName(),
                'price' => $this->calculatePrice($service),
                'serviceId' => $service->getId(),
            ];
        }

        return $data;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        if ($data['count'] === "0") {
            return [
                'count' => 0,
                'entities' => [],
            ];
        }
        $items = [];
        foreach ($data['entities'] as $group) {
            $item = [
                'name' =>  [
                    'value' => $group->getName(),
                    'editColumn' => true,
                ],
                'description' => (string) $group->getDescription(),
            ];
            if ($this->user->isAdminLoggedIn()) {
                $item['tenantId'] = $group->getTenant()?->getName();
            }
            $items[] = [
                'columns' => $item,
                'id' => $group->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }


    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'description', 'label' => 'Description'],
        ];
        if ($this->user->isAdminLoggedIn()) {
            $columnDefinitions[] = ['name' => 'tenantId', 'label' => 'Tenant'];
        }

        return $columnDefinitions;
    }
}