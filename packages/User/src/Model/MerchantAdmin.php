<?php
namespace Fakture\User\Model;

use Fakture\Tenant\Model\Tenant;

/**
 * Class MerchantAdmin.
 * Represents merchant admin user dto.
 *
 * @package Fakture\User\Model
 */
class MerchantAdmin extends User
{
    protected $redirectPath = '/invoice/view/';

    public function __construct(...$data) {
        parent::__construct(...$data);
    }
}