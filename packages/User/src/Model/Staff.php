<?php
namespace Fakture\User\Model;

/**
 * Class Staff.
 * Represents admin user dto.
 *
 * @package Fakture\User\Model
 */
class Staff extends User
{
    protected $redirectPath = '/invoice/view/';

    public function __construct(...$data) {
        parent::__construct(...$data);
    }
}