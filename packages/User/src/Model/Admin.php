<?php
namespace Fakture\User\Model;

/**
 * Class Admin.
 * Represents admin user dto.
 *
 * @package Skeletor\User\Model
 */
class Admin extends User
{
    protected $redirectPath = '/user/view/';

    public function __construct(...$data) {
        parent::__construct(...$data);
    }
}