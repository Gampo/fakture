<?php
namespace Fakture\User\Model;

use Fakture\Tenant\Model\Tenant;
use Skeletor\User\Model\User as BaseUser;

abstract class User extends BaseUser
{
    const ROLE_GUEST = 0;
    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;
    const ROLE_MERCHANT_ADMIN = 3;

    private $firstName;

    private $lastName;

    /* @var Tenant */
    private $tenant;

    private $jmbg;

    private $invoiceAddress;

    private $invoicePosition;

    private $lk;

    public function __construct(
        $userId, $password, $email, $createdAt, $updatedAt, $role, $isActive, $displayName, $firstName, $lastName,
        $ipv4, $lastLogin, Tenant $tenant, $jmbg = '', $invoiceAddress = '', $lk = '', $invoicePosition = ''
    ) {
        parent::__construct($userId, $password, $email, $createdAt, $updatedAt, $role, $isActive, $displayName, $ipv4, $lastLogin);
        $this->firstName = (string) $firstName;
        $this->lastName = (string) $lastName;
        $this->tenant = $tenant;
        $this->jmbg = $jmbg;
        $this->invoiceAddress = $invoiceAddress;
        $this->lk = $lk;
        $this->invoicePosition = $invoicePosition;
    }

    public static function getHrTypes()
    {
        return [
            static::ROLE_ADMIN => 'Admin',
            static::ROLE_STAFF => 'Staff',
            static::ROLE_MERCHANT_ADMIN => 'Merchant Admin',
        ];
    }

    /**
     * @return Tenant
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed|string
     */
    public function getJmbg(): mixed
    {
        return $this->jmbg;
    }

    /**
     * @return mixed|string
     */
    public function getInvoiceAddress(): mixed
    {
        return $this->invoiceAddress;
    }

    /**
     * @return mixed|string
     */
    public function getLk(): mixed
    {
        return $this->lk;
    }

    /**
     * @return mixed|string
     */
    public function getInvoicePosition(): mixed
    {
        return $this->invoicePosition;
    }

}
