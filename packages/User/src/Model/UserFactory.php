<?php
namespace Fakture\User\Model;

use Skeletor\User\Model\UserFactory as Factory;
use Skeletor\User\Model\UserInterface;
use Fakture\Tenant\Repository\TenantRepository;

class UserFactory extends Factory
{
    /**
     * @var TenantRepository
     */
    private $tenantRepo;

    public function __construct(\DateTime $dt, TenantRepository $tenantRepo)
    {
        parent::__construct($dt);
        $this->dt = $dt;
        $this->tenantRepo = $tenantRepo;
    }

    public function make($userData, $createdBy = null): UserInterface
    {
        $data = [];
        foreach ($userData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->getDt();
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        // @TODO use interface
        $haveAddress = false;
        switch ($data['role'])
        {
            case User::ROLE_ADMIN:
                $user = Admin::class;
                break;

            case User::ROLE_STAFF:
                $user = Staff::class;
                break;

            case User::ROLE_MERCHANT_ADMIN:
                $user = MerchantAdmin::class;
                break;

            default:
                throw new \InvalidArgumentException('Invalid role provided: ' . $data['role']);
                break;
        }

        $data['tenant'] = null;
        if ((int) $data['tenantId']) {
            $data['tenant'] = $this->tenantRepo->getById($data['tenantId']);
        }
        unset($data['tenantId']);
        $data['isActive'] = (int) $data['isActive'];

        return new $user(...$data);
    }
}