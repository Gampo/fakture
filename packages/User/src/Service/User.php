<?php

namespace Fakture\User\Service;

use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Activity\Repository\ActivityRepository;
use Skeletor\Address\Service\Address;
use Skeletor\TableView\Model\Column;
use Skeletor\User\Repository\UserRepositoryInterface as UserRepo;

class User extends \Skeletor\User\Service\User
{
    const INVALID_USER_TYPE = 'Doslo je do greske. Tip korisnika nema pristup.';

    public function __construct(
        UserRepo $repo,
        Session $session,
        Config $config, Logger $logger, ?Address $address
        , ActivityRepository $activity,
        \Fakture\User\Filter\User $filter
    ) {
        parent::__construct($repo, $session, $config, $logger, $address, $activity, $filter);
    }

    public function login($data)
    {
        try {
            $user = $this->getByEmail($data['email']);
            if (!password_verify($data['password'], $user->getPassword())) {
                $this->messages[] = self::LOGIN_ERROR_INVALID;
                return false;
            }
        } catch (\Exception $e) {
            $this->messages[] = self::LOGIN_ERROR_NO_EMAIL;
            return false;
        }

        $this->messages[] = self::LOGIN_SUCCESS;
        $this->session->getStorage()->offsetSet('redirectPath', $user->getRedirectPath());
        $this->session->getStorage()->offsetSet('loggedIn', $user->getId());
        $this->session->getStorage()->offsetSet('tenantId', $user->getTenant()->getId());
        $this->session->getStorage()->offsetSet('loggedInRole', $user->getRole());
        $this->session->getStorage()->offsetSet('loggedInEmail', $user->getEmail());
        if ($data['rememberMe']) {
            $this->session->rememberMe();
        }
//        $this->session->getStorage()->offsetSet('redirectPath', $this->config->redirectUri);
        //@TODO think of a better way to have per role redirect path

        return true;
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        $items = [];
        /* @var \Skeletor\User\Model\User $user */
        foreach ($data['entities'] as $user) {
            $createActivity = $this->activity->fetchByEntityIdAndAction($user->getId(), $user::class, 'create');
            $createdBy = 'N/A';
            if (!empty($createActivity)) {
                $createdBy = $createActivity[0]->getUser()->getFirstName() .' '. $createActivity[0]->getUser()->getLastName();
            }
            $editRoute = sprintf("<a href='/user/form/%s/' title='Edit user'>%s</a>", $user->getId(), $user->getEmail());
            $items[] = [
                'columns' => [
                    'id' => $user->getId(),
                    'name' => $editRoute,
                    'isActive' => $user->getIsActive(),
                    'role' => $user::hrLevels($user->getRole()),
                    'createdBy' => $createdBy,
                    'createdAt' => $user->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $user->getUpdatedAt()->format('d.m.Y'),
                ],
                'id' => $user->getId()
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'userId', 'label' => '#'],
            ['name' => 'email', 'label' => 'Email'],
            ['name' => 'isActive', 'label' => 'Is active'],
            ['name' => 'role', 'label' => 'Role'],
            ['name' => 'createdBy', 'label' => 'Created by'],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
        ];
//        if ($this->user->isAdminLoggedIn()) {
//            $merchants = [];
//            foreach ($this->merchant->getEntities() as $merchant) {
//                $merchants[$merchant->getId()] = $merchant->getName();
//            }
//            $columnDefinitions[2] = (new Column('client_id', 'Client name'))->addViewParam('filterable', true)
//                ->addViewParam('filterValues', $merchants);
//        }

        return $columnDefinitions;
    }
}