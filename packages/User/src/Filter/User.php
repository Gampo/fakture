<?php
namespace Fakture\User\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Volnix\CSRF\CSRF;
use Skeletor\User\Validator\User as UserValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class User extends \Skeletor\User\Filter\User
{
    /**
     * @var UserValidator
     */
    protected $validator;

    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();
        $data = [
            'userId' => (isset($postData['userId'])) ? $postData['userId'] : null,
            'password' => (isset($postData['password'])) ? $postData['password'] : null,
            'password2' => (isset($postData['password2'])) ? $postData['password2'] : null,
            'tenantId' => $postData['tenantId'],
            'email' => $postData['email'],
            'role' => $postData['role'],
//            'pib' => (isset($postData['pib']) && strlen($postData['pib']) > 1) ? $postData['pib'] : 0,
//            'companyName' => (isset($postData['companyName'])) ? $postData['companyName'] : '',
            'isActive' => $int->filter($postData['isActive']),
            'displayName' => $alnum->filter($postData['firstName'] . ' ' . $postData['lastName']),
            'firstName' => $alnum->filter($postData['firstName']),
            'lastName' => $alnum->filter($postData['lastName']),
            'jmbg' => $alnum->filter($postData['jmbg']),
            'lk' => $alnum->filter($postData['lk']),
            'invoiceAddress' => $postData['invoiceAddress'],
            'invoicePosition' => $alnum->filter($postData['invoicePosition']),
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            throw new ValidatorException();
        }
        unset($data[CSRF::TOKEN_NAME]);
        unset($data['password2']);

        return $data;
    }

}