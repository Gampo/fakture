<?php
namespace Fakture\Efakture\Service;

use Fakture\Efakture\Model\Measure;
use Fakture\Efakture\Model\TaxClasses;
use Fakture\Invoice\Model\Invoice;
use Fakture\Invoice\Model\InvoiceService;
use Fakture\Tenant\Model\Tenant;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Laminas\Config\Config;
use NumNum\UBL\Generator;
use NumNum\UBL\LegalEntity;
use NumNum\UBL\PartyTaxScheme;
use NumNum\UBL\TaxScheme;
use Psr\Log\LoggerInterface;
use Skeletor\Mapper\NotFoundException;

class Api
{
    private $endpoint;

    public function __construct(private Client $client, private Config $config, private LoggerInterface $logger)
    {
        $this->endpoint = $config->offsetGet('efakture')->endpoint;
    }

    public function getInputInvoiceIds(Tenant $tenant, $status)
    {
        // dateFrom, dateTo optional
        $uri = '/purchase-invoice/ids?status=' . $status;
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'POST', $uri);
    }

    public function rejectInputInvoice(Tenant $tenant, $eInvoiceId)
    {
        $uri = '/purchase-invoice/acceptRejectPurchaseInvoice';
        $data = [
            'invoiceId' => $eInvoiceId,
            'accepted' => false,
            'comment' => '',
        ];
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'POST', $uri, $data);
    }

    public function acceptInputInvoice(Tenant $tenant, $eInvoiceId)
    {
        $uri = '/purchase-invoice/acceptRejectPurchaseInvoice';
        $data = [
            'invoiceId' => $eInvoiceId,
            'accepted' => true,
            'comment' => '',
        ];
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'POST', $uri, $data);
    }

    public function getSignedInputInvoice(Tenant $tenant, $eInvoiceId)
    {
        $uri = '/purchase-invoice/signature?invoiceId=' . $eInvoiceId;
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'GET', $uri);
    }

    public function getSignedExitInvoice(Tenant $tenant, $eInvoiceId)
    {
        $uri = '/sales-invoice/signature?invoiceId=' . $eInvoiceId;
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'GET', $uri);

    }

    /**
     *
     * Only documents with status 'nacrt'
     * @return void
     */
    public function deleteInvoice(Tenant $tenant, $eInvoiceId)
    {
        $uri = '/sales-invoice/' . $eInvoiceId;
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'DELETE', $uri);
    }

    public function getMeasures(Tenant $tenant)
    {
        $uri = '/get-unit-measures';
        $list = [];
        foreach ($this->send($tenant->getSettings()->getSefApiKey(), 'GET', $uri, ['Content-Type' => 'application/json']) as $measureData) {
            $list[] = new Measure(... (array) $measureData);
        }

        return $list;
    }

    public function checkIfCompanyRegistered(Tenant $tenant)
    {
        $uri = '/Company/CheckIfCompanyRegisteredOnEfaktura';
        $data = [
//            'registrationNumber' => $tenant->getClient()->getMb(),
            'registrationNumber' => "17862146",
//            'vatNumber' => $tenant->getClient()->getPib(),
            'vatNumber' => '108213413',
            'jbkjs' => '10520'
        ];

        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'GET', $uri, $data);
        // does not work
        var_dump($response);
        die();
    }



    /**
     *
     * Only documents with status 'Odobreno'
     * @return void
     */
    public function voidInvoice(Tenant $tenant, $eInvoiceId, $comment = '')
    {
        $sefInvoiceData = $this->getOutputInvoice($tenant, $eInvoiceId);
        // @TODO check statuses
        if ($sefInvoiceData->Status === 'Sent') {
            $this->cancelInvoice($tenant, $eInvoiceId, $comment = '');
            return;
        }
        if ($sefInvoiceData->Status !== 'Approved') {
            throw new \Exception('Nije moguće stornirati fakturu na SEF sistemu jer ima pogrešan status, očekivan je odobreno, a ova faktura ima status: ' . $sefInvoiceData->Status);
        }
        $uri = '/sales-invoice/storno';
        $data = [
            'invoiceId' => $eInvoiceId,
            'stornoNumber' => $eInvoiceId . '-storno',
            'stornoComment' => $comment,
        ];
        $data = json_encode($data);
        $header = [
            'Accept' => 'text/plain',
            'Content-Type' => 'application/json',
        ];
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'POST', $uri, $header, $data);
    }

    public function getOutputInvoice(Tenant $tenant, $eInvoiceId)
    {
        $uri = '/sales-invoice?invoiceId=' . $eInvoiceId;
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'GET', $uri);

        return $response;
    }

    public function getOutputInvoiceIds(Tenant $tenant, $status = 'xxxx')
    {
//        $uri = '/sales-invoice/ids?status=' . $status;
        $uri = '/sales-invoice/ids';
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'POST', $uri);

        return $response->SalesInvoiceIds;
    }

    /**
     *
     * Only documents with status 'Poslato' or 'Slanje'
     * @return void
     */
    public function cancelInvoice(Tenant $tenant, $eInvoiceId, $comment = '')
    {
        $uri = '/sales-invoice/cancel';
        $data = [
            'invoiceId' => $eInvoiceId,
            'cancelComments' => $comment,
        ];
        $data = json_encode($data);
        $header = [
            'Accept' => 'text/plain',
            'Content-Type' => 'application/json',
        ];
        $response = $this->send($tenant->getSettings()->getSefApiKey(), 'POST', $uri, $header, $data);
    }

    public function sendUblInvoice(Invoice $invoice, $tenantClient)
    {
        $requestId = time(); // must be unique per tenant / pib
        $uri = '/sales-invoice/ubl/upload?sendToCir=No&requestId=' . $requestId;

        $compiled = $this->compileUblInvoice($invoice, $tenantClient);
        $generator = Generator::invoice($compiled['invoice'], 'RSD');
//        echo $generator;
//        die();

        $fileName = sprintf('/tmp/%s.xml', md5($generator));
        file_put_contents($fileName, $generator);
        $file = fopen($fileName, 'r');
        $options = [
            'multipart' => [
            [
                'name' => 'ublFile',
                'contents' => $file,
                'filename' => basename($fileName),
                'headers'  => [
                    'Content-Type' => 'multipart/form-data;'
                ]
            ]
        ]];
//        echo $generator;
//        die();

//        try {
            /**
             * InvoiceId
             * PurchaseInvoiceId
             * SalesInvoiceId
             */
            $result = $this->send($invoice->getTenant()->getSettings()->getSefApiKey(), 'POST', $uri, [], null, $options);
//        } catch (NotFoundException $e) { // receiver not registered on SEF
            // register vat with SEF
//            if ($invoice->getTenant()->isInVatSystem()) {
//                throw new
//                if (!$this->sendSingleVatRecording($invoice, $compiled['totalTax'])) {
//                    throw new \Exception('There was an error reporting PDV.');
//                }
//            }
//            throw $e;
//        }

        return $result;
    }

    public function sendSingleVatRecording(Invoice $invoice, $totalTax)
    {
        $data = [
            'DocumentNumber' => $invoice->getInvoiceCode(),
            'TurnoverDate' => $invoice->getTurnoverDate()->format('Y-m-d\TH:i:s').'Z',
            'PaymentDate' => $invoice->getDeadlineDate()->format('Y-m-d\TH:i:s').'Z',
            'TotalAmount' => $invoice->getAmount(),
            'TurnoverAmount' => $invoice->getAmount(),

            'VatAmount' => number_format($totalTax, 2),
            'VatBaseAmount20' => number_format($invoice->getAmount() - $totalTax, 2),
            'VatBaseAmount10' => 0,
            'DocumentDirection' => 'Outbound',
            'VatPeriod' => $invoice->getIssueDate()->format('F'),

            'CalculationNumber' => $invoice->getInvoiceCode(),
            'CompanyId' => null,
            'DocumentType' => 'Invoice',
            'ForeignDocument' => false,
            'IndividualVatId' => 0,
            'InternalInvoiceOption' => null,
            'VatDeductionRight' => null,
            'RelatedDocuments' => [],
            'RelatedPartyIdentifier' => $invoice->getClientVat(),
            'TurnoverDescription20' => "",
            'VatRecordingStatus' => "Draft",
            'VatRecordingVersion' => "Second",
        ];
//        var_dump($data);
//        die();
        $data = json_encode($data);
        $uri = '/vat-recording/individual';
        $header = [
            'Accept' => 'text/plain',
            'Content-Type' => 'application/json',
        ];
        $result = $this->send($invoice->getTenant()->getSettings()->getSefApiKey(), 'POST', $uri, $header, $data);
        if (strlen($result->IndividualVatId) > 0 && $result->IndividualVatId && $result->VatRecordingStatus == 'Recorded') {
            // passed
            return true;
        }
        $this->logger->error('There was an error registering PDV:' . json_encode($result));

        return false;
    }

    private function send($apiKey, $method, $uri, $headers = [], $data = null, $options = [])
    {
        $url = $this->endpoint . $uri;
        $body = null;
        if ($data) {
            $body = $data;
            if (empty($headers)) {
                $body = json_encode($data);
            }
        }
        $headers = array_merge([
            'ApiKey' => $apiKey,
        ], $headers);
        $this->logger->debug(sprintf('Sending SEF request to: %s with data: %s', $url, $body));

        try {
            $response = $this->client->send(new Request($method, $url, $headers, $body), $options);
        } catch (\Exception $e) {
            if ($e->getCode() === 400) {
                if (strpos($e->getMessage(), 'Company with identifier') !== false &&
                    strpos($e->getMessage(), 'not found') !== false) {
                    throw new NotFoundException('Target not registered.');
                }
            }
            echo $e->getMessage();
            die();
        }
        $json = $response->getBody()->getContents();
        $this->logger->debug(sprintf('Got following response: %s ', $json));

        return json_decode($json);
    }

    private function compileUblInvoice(Invoice $invoice, $tenantClient)
    {
        $country = (new \NumNum\UBL\Country())
            ->setIdentificationCode('RS');
        $isPdvUser = $invoice->getTenant()->getSettings()->isInVatSystem();

        $supplier = $tenantClient;
        $addressParts = explode(' ', $supplier->getAddress());
        $streetNumber = $addressParts[count($addressParts)-1];
//        $streetNumber = str_replace('/', '-', $addressParts[count($addressParts)-1]);
        unset($addressParts[count($addressParts)-1]);
        $streetName = implode(' ', $addressParts);

        // Full address
        $address = (new \NumNum\UBL\Address())
            ->setStreetName($streetName .' '. $streetNumber)
            ->setBuildingNumber($streetNumber)
            ->setCityName($supplier->getCity())
            ->setPostalZone($supplier->getZip())
            ->setCountry($country);

        // Supplier company node
        $taxScheme = (new TaxScheme())
            ->setId('VAT');
        $supplierTaxScheme = (new PartyTaxScheme())
            ->setTaxScheme($taxScheme)
            ->setCompanyId('RS' . $supplier->getPib());
        $partyLegalEntity = (new LegalEntity())
            ->setCompanyId($supplier->getMb())
            ->setRegistrationName($supplier->getName());
        $supplierCompany = (new \NumNum\UBL\Party())
            ->setLegalEntity($partyLegalEntity)
            ->setPartyTaxScheme($supplierTaxScheme)
            ->setEndpointID($supplier->getPib(), TaxClasses::NO_PDV_SCHEME_ID)
            ->setName($supplier->getName())
            ->setPostalAddress($address);
        if ($isPdvUser) {
            $supplierCompany->setPartyTaxScheme($supplierTaxScheme);
        } else {
            // @TODO What goes here ?
        }

        // Client contact node
        $clientContact = (new \NumNum\UBL\Contact())
            ->setName($invoice->getClientName())
            ->setElectronicMail($invoice->getClient()->getEmail());

        $addressParts = explode(' ', $invoice->getClientAddress());
        $streetNumber = str_replace('/', '-', $addressParts[count($addressParts)-1]);
        unset($addressParts[count($addressParts)-1]);
        $streetName = implode(' ', $addressParts);

        $address = (new \NumNum\UBL\Address())
            ->setStreetName(str_replace('/', '-', $invoice->getClientAddress()))
            ->setBuildingNumber($streetNumber)
            ->setCityName($invoice->getClientCity())
            ->setPostalZone($invoice->getClient()->getZip())
            ->setCountry($country);

        $taxScheme = (new TaxScheme())
            ->setId('VAT');
        $clientTaxScheme = (new PartyTaxScheme())
            ->setTaxScheme($taxScheme)
            ->setCompanyId('RS' . $invoice->getClient()->getPib());
        $partyLegalEntity = (new LegalEntity())
            ->setCompanyId($invoice->getClient()->getMb())
            ->setRegistrationName($invoice->getClient()->getName());

        // Client company node
        $clientCompany = (new \NumNum\UBL\Party())
            ->setLegalEntity($partyLegalEntity)
            ->setPartyTaxScheme($clientTaxScheme)
            ->setEndpointID($invoice->getClientVat(), TaxClasses::NO_PDV_SCHEME_ID)
            ->setName($invoice->getClientName())
            ->setPostalAddress($address)
            ->setContact($clientContact);

        $taxScheme = (new \NumNum\UBL\TaxScheme())
            ->setId('VAT');

        $subTotal = 0;
        $totalTax = 0;
        $taxPercent = 0;
        $invoiceLines = [];
        /* @var InvoiceService $service */
        foreach ($invoice->getServices() as $key => $service) {
            $taxCategoryId = TaxClasses::NO_PDV;
            if ($service->getTax()) {
                $taxPercent = max($taxPercent, $service->getTax());
                $taxCategoryId = TaxClasses::PDV;
            }
            if ($invoice->getType() === Invoice::TYPE_FOREIGN) {
                $taxCategoryId = TaxClasses::FOREIGN;
            }

            $classifiedTaxCategory = (new \NumNum\UBL\ClassifiedTaxCategory())
                ->setId($taxCategoryId)
                ->setPercent($service->getTax())
                ->setTaxScheme($taxScheme);

            $productItem = (new \NumNum\UBL\Item())
                ->setName($service->getServiceName())
//                ->setDescription($service->getServiceName())
                ->setClassifiedTaxCategory($classifiedTaxCategory);
//                ->setSellersItemIdentification($service->getServiceName() .'#'. $service->getServiceId());

            $price = (new \NumNum\UBL\Price())
                ->setBaseQuantity($service->getQty())
                ->setUnitCode($service->getUnitOfMeasure())
                ->setUnitCodeListId($service->getUnitOfMeasure())
                ->setPriceAmount($service->getPrice());

            $subTotal += $service->getPrice() * $service->getQty();
            $totalTax += $service->getTax() * $service->getPrice() * $service->getQty() / 100;

            $invoiceLines[] = (new \NumNum\UBL\InvoiceLine())
                ->setId($key+1)
                ->setItem($productItem)
                ->setPrice($price)
                ->setLineExtensionAmount($service->getPrice() * $service->getQty())
                ->setUnitCode($service->getUnitOfMeasure())
                ->setUnitCodeListId($service->getUnitOfMeasure())
                ->setInvoicedQuantity($service->getQty());
        }

        // Total Taxes
        if ($isPdvUser) {
            $taxCategory = (new \NumNum\UBL\TaxCategory())
                ->setId($taxCategoryId)
                ->setPercent($taxPercent)
                ->setTaxScheme($taxScheme);
        } else {
            $taxCategory = (new \NumNum\UBL\TaxCategory())
                ->setId($taxCategoryId)
                ->setPercent($taxPercent)
                ->setTaxExemptionReason('Broj resenja o oslobodjenju placanja PDV')
                ->setTaxExemptionReasonCode('PDV-RS-33')
                ->setTaxScheme($taxScheme);
        }

        $taxableAmount = $subTotal;

        $taxSubTotal = (new \NumNum\UBL\TaxSubTotal())
            ->setTaxableAmount($taxableAmount)
            ->setTaxAmount($totalTax)
            ->setTaxCategory($taxCategory);

        $taxTotal = (new \NumNum\UBL\TaxTotal())
            ->addTaxSubTotal($taxSubTotal)
            ->setTaxAmount($totalTax);

        $delivery = (new \NumNum\UBL\Delivery())
            ->setActualDeliveryDate($invoice->getTurnoverDate());

        $payeeFinancialAccount = (new \NumNum\UBL\PayeeFinancialAccount())
            ->setId($supplier->getAccountNumber());

        $means =  (new \NumNum\UBL\PaymentMeans())
//            ->setPaymentMeansCode(30)  // @todo check this
            ->setPaymentId($invoice->getInvoiceCode())
            ->setPayeeFinancialAccount($payeeFinancialAccount);

        $invoicePeriod = (new \Fakture\Efakture\Model\InvoicePeriod())
            ->setDescriptionCode($invoice->getVatPeriodCalculationType());

        if ($isPdvUser) {
            $legalMonetaryTotal = (new \NumNum\UBL\LegalMonetaryTotal())
                ->setPayableAmount($invoice->getAmount())
                ->setLineExtensionAmount($subTotal)
                ->setTaxExclusiveAmount(0)
                ->setTaxInclusiveAmount($invoice->getAmount())
                ->setAllowanceTotalAmount(0);
        } else {
            $legalMonetaryTotal = (new \NumNum\UBL\LegalMonetaryTotal())
                ->setPayableAmount($invoice->getAmount())
                ->setLineExtensionAmount($invoice->getAmount())
                ->setTaxExclusiveAmount($invoice->getAmount())
                ->setTaxInclusiveAmount(0)
                ->setAllowanceTotalAmount(0);
        }

        return ['totalTax' => $totalTax, 'invoice' => (new \NumNum\UBL\Invoice())
            ->setId($invoice->getInvoiceCode())
            ->setIssueDate(new \DateTime())
            ->setDueDate($invoice->getDeadlineDate())
            ->setInvoicePeriod($invoicePeriod)
            ->setDelivery($delivery)
            ->setPaymentMeans($means)
            ->setAccountingSupplierParty($supplierCompany)
            ->setAccountingCustomerParty($clientCompany)
            ->setInvoiceLines($invoiceLines)
            ->setLegalMonetaryTotal($legalMonetaryTotal)
            ->setCustomizationID('urn:cen.eu:en16931:2017#compliant#urn:mfin.gov.rs:srbdt:2021')
            ->setDocumentCurrencyCode('RSD')
            ->setTaxTotal($taxTotal)];
    }
}
