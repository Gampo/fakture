<?php

namespace Fakture\Efakture\Model;

class TaxClasses
{
    const NO_PDV = 'SS';
    const PDV = 'S';
    const FOREIGN = 'O';

    const NO_PDV_SCHEME_ID = '9948';

}