<?php

namespace Fakture\Efakture\Model;

class InvoiceStatus
{
    const STATUS_NEW = 'New';
    const STATUS_DRAFT = 'Draft';
    const STATUS_SENT = 'Sent';
    const STATUS_PAID = 'Paid';
    const STATUS_MISTAKE = 'Mistake';
    const STATUS_OVERDUE = 'OverDue';
    const STATUS_ARCHIVED = 'Archived';
    const STATUS_SENDING = 'Sending';
    const STATUS_DELETED = 'Deleted';
    const STATUS_APPROVED = 'Approved';
    const STATUS_REJECTED = 'Rejected';
    const STATUS_CANCELLED = 'Cancelled';
    const STATUS_STORNO = 'Storno';
    const STATUS_UNKNOWN = 'Unknown';
}