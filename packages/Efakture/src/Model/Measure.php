<?php

namespace Fakture\Efakture\Model;

class Measure
{

    public function __construct(
        private string $Code, private ?string $Symbol, private string $NameEng, private string $NameSrbLtn,
        private string $NameSrbCyr, private string $IsOnShortList
    ) {
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->Code;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->Symbol;
    }

    /**
     * @return string
     */
    public function getNameEng(): string
    {
        return $this->NameEng;
    }

    /**
     * @return string
     */
    public function getNameSrbLtn(): string
    {
        return $this->NameSrbLtn;
    }

    /**
     * @return string
     */
    public function getNameSrbCyr(): string
    {
        return $this->NameSrbCyr;
    }

    /**
     * @return string
     */
    public function getIsOnShortList(): string
    {
        return $this->IsOnShortList;
    }

}