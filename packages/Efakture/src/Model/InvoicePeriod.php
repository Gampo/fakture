<?php

namespace Fakture\Efakture\Model;

use NumNum\UBL\Schema;
use Sabre\Xml\Writer;
use DateTime;
use InvalidArgumentException;

class InvoicePeriod extends \NumNum\UBL\InvoicePeriod
{
    private $DescriptionCode;

    public function getDescriptionCode(): string
    {
        return $this->DescriptionCode;
    }

    /**
     * @param string $DescriptionCode
     * @return $this
     */
    public function setDescriptionCode(string $DescriptionCode): InvoicePeriod
    {
        $this->DescriptionCode = $DescriptionCode;
        return $this;
    }

    /**
     * The validate function that is called during xml writing to valid the data of the object.
     *
     * @throws InvalidArgumentException An error with information about required data that is missing to write the XML
     * @return void
     */
    public function validate()
    {
        if ($this->DescriptionCode === null) {
            throw new InvalidArgumentException('Missing DescriptionCode');
        }
    }

    /**
     * The xmlSerialize method is called during xml writing.
     *
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer): void
    {
        $this->validate();

        if ($this->DescriptionCode != null) {
            $writer->write([
                Schema::CBC . 'DescriptionCode' => $this->DescriptionCode,
            ]);
        }
    }
}