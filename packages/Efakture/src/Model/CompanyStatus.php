<?php

namespace Fakture\Efakture\Model;

class CompanyStatus
{
    const STATUS_ACTIVE = 'Active';
    const STATUS_PASSIVE = 'Passive';
    const STATUS_DELETED = 'Deleted';
    const STATUS_MIGRATED = 'Migrated';
    const STATUS_MISTAKE = 'Mistake';

}