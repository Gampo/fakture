<?php
namespace Fakture\Efakture\Model;

use Skeletor\Model\Model;

class EfaktureInvoice extends Model
{

    /**
     * @param $id
     * @param $InvoiceId
     * @param $PurchaseInvoiceId
     * @param $SalesInvoiceId
     * @param $status
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct(
        private $id, private $InvoiceId, private $PurchaseInvoiceId, private $SalesInvoiceId, private $status,
        $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->InvoiceId;
    }

    /**
     * @return mixed
     */
    public function getPurchaseInvoiceId()
    {
        return $this->PurchaseInvoiceId;
    }

    /**
     * @return mixed
     */
    public function getSalesInvoiceId()
    {
        return $this->SalesInvoiceId;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }


}