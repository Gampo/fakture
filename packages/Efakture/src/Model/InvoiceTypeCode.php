<?php
namespace Fakture\Efakture\Model;

class InvoiceTypeCode
{
    const STATUS_COMMERCIAL_INVOICE = 380;
    const STATUS_CREDIT_NOTE = 381;
    const STATUS_BOOK_DEBT = 383;
    const STATUS_ADVANCE_INVOICE = 386;
}
