<?php

namespace Fakture\Apr\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Laminas\Config\Config;

class Api
{
    public function __construct(private Config $config, private Client $client)
    {
    }

    public function test()
    {
        //        $endpoint = 'https://service1.apr.gov.rs:4430/plws/PlService.svc';
        $endpoint = 'https://service1.apr.gov.rs:4430/plwstest/PlService.svc';
        $username = 'Gampo d.o.o.';
        $password = '%C!tF`=xp7A*';
        $nonce = base64_encode(time());
        $created = '2023-01-29T17:25:01Z';
        $phash = base64_encode(sha1($nonce . $created . $password));

        $loginXml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bus="http://BusinessEntityDataMediationModule/serviceExport/BusinessEntityDataInterface/">
<soapenv:Header>
    
  <bus:Security SOAP-ENV:mustUnderstand="1" xmlns:SOAP-ENV="SOAP-ENV">
     <bus:UsernameToken wsu:Id="UsernameToken-1" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-buscurity-utility-1.0.xsd">
        <bus:Username>'.$username.'</bus:Username>
        <bus:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'.$password.'</bus:Password>
     </bus:UsernameToken>
  </bus:Security>

</soapenv:Header>
<soapenv:Body>
    <bus:PreuzmiPodatkeOPrivrednomSubjektu>
        <bus:privredniSubjektiUlazniPodaci>
            <bus:privredniSubjekti>
                <bus:maticniBroj>07216009</bus:maticniBroj>
                <bus:tip>1</bus:tip>
            </bus:privredniSubjekti>
        </bus:privredniSubjektiUlazniPodaci>
    </bus:PreuzmiPodatkeOPrivrednomSubjektu>
</soapenv:Body>
</soapenv:Envelope>';
        $headers = [
            'Content-Type' => 'text/xml',
            'Authorization' => 'Basic ' . base64_encode($username .':'. $password)
//            'SOAPAction' => 'login',
        ];
        $request = new Request('POST', $endpoint, $headers, $loginXml);
        try {
            $response = $this->client->send($request);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }

        var_dump($response);

        die();

        $tested = "
        <bus:Security >
      <bus:UsernameToken>
        <bus:Username>'.$username.'</bus:Username>
        <bus:Password >g112a9eHPR1hXD4UH+Lh3o8JV/o=</bus:Password>
        <bus:Nonce >'.$nonce.'</bus:Nonce>
        <bus:Created>'.$created.'</bus:Created>
        <bus:Password>
        '.$phash.'
        </bus:Password>
      </bus:UsernameToken>
    </bus:Security>
        
        
        
        ";


        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bus="http://BusinessEntityDataMediationModule/serviceExport/BusinessEntityDataInterface/">
        <soapenv:Header/>
        <soapenv:Body>
    <bus:PreuzmiPodatkeOPrivrednomSubjektu>
        <bus:privredniSubjektiUlazniPodaci>
            <bus:privredniSubjekti>
                <bus:maticniBroj>07216009</bus:maticniBroj>
                <bus:tip>1</bus:tip>
                <!--Optional:-->
                <bus:gp>
                    <bus:grupa>1014</bus:grupa>
                </bus:gp>
                <bus:gp>
                    <bus:grupa>1001</bus:grupa>
                </bus:gp>
                <bus:gp>
                    <bus:grupa>1005</bus:grupa>
                </bus:gp>
            </bus:privredniSubjekti>
        </bus:privredniSubjektiUlazniPodaci>
    </bus:PreuzmiPodatkeOPrivrednomSubjektu>
</soapenv:Body></soapenv:Envelope>';


    }

    public function search($string)
    {
        return [];
        $data = [
            ['name' => 'Test klijent 1', 'vat' => '123123123', 'mb' => 12312312, 'address' => 'Veselih drugara 23', 'city' => 'Beograd'],
            ['name' => 'Test klijent 2', 'vat' => '123123125', 'mb' => 12312313, 'address' => 'Veselih drugara 24', 'city' => 'Beograd'],
            ['name' => 'Test klijent 3', 'vat' => '123123124', 'mb' => 12312314, 'address' => 'Veselih drugara 25', 'city' => 'Beograd'],
            ['name' => 'Test klijent 1', 'vat' => '123123123', 'mb' => 12312312, 'address' => 'Veselih drugara 23', 'city' => 'Beograd'],
            ['name' => 'Test klijent 2', 'vat' => '123123125', 'mb' => 12312313, 'address' => 'Veselih drugara 24', 'city' => 'Beograd'],
            ['name' => 'Test klijent 3', 'vat' => '123123124', 'mb' => 12312314, 'address' => 'Veselih drugara 25', 'city' => 'Beograd'],
            ['name' => 'Test klijent 1', 'vat' => '123123123', 'mb' => 12312312, 'address' => 'Veselih drugara 23', 'city' => 'Beograd'],
            ['name' => 'Test klijent 2', 'vat' => '123123125', 'mb' => 12312313, 'address' => 'Veselih drugara 24', 'city' => 'Beograd'],
            ['name' => 'Test klijent 3', 'vat' => '123123124', 'mb' => 12312314, 'address' => 'Veselih drugara 25', 'city' => 'Beograd'],
            ['name' => 'Test klijent 1', 'vat' => '123123123', 'mb' => 12312312, 'address' => 'Veselih drugara 23', 'city' => 'Beograd'],
            ['name' => 'Test klijent 2', 'vat' => '123123125', 'mb' => 12312313, 'address' => 'Veselih drugara 24', 'city' => 'Beograd'],
            ['name' => 'Test klijent 3', 'vat' => '123123124', 'mb' => 12312314, 'address' => 'Veselih drugara 25', 'city' => 'Beograd'],
        ];

        return $data;
    }
}