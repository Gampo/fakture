<?php

namespace Fakture\Apr\Service;

class ClientSearch
{
    private $aprApi;

    public function __construct(Api $api)
    {
        $this->aprApi = $api;
    }

    public function search($search)
    {
        return $this->aprApi->search($search);
    }
}