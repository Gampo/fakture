import Activity from "./Activity.js";
import ServiceGroup from "./ServiceGroup.js";
import Service from "./Service.js";
import Invoice from "./Invoice.js";
import InvoiceRecurring from "./InvoiceRecurring.js";
import InvoiceForm from "./InvoiceForm.js";
import Tenant from "./Tenant.js";
import Translator from "./Translator.js";
import Client from "./Client/Client.js";

export default class PageFactory {
    static make(page) {
        switch (page) {
            case 'Activity':
                return new Activity();
            case 'ServiceGroup':
                return new ServiceGroup();
            case 'Tenant':
                return new Tenant();
            case 'Translator':
                return new Translator();
            case 'Invoice':
                return new Invoice();
            case 'InvoiceRecurring':
                return new InvoiceRecurring();
            case 'InvoiceForm':
                return new InvoiceForm();
            case 'InvoiceRecurringForm':
                return new InvoiceRecurringForm();
            case 'Client':
                return new Client();
            case 'Service':
                return new Service();
        }
    }
}