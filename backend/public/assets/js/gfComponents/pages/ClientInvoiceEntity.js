export default class ClientInvoiceEntity {
    #clientAddress;
    #clientCity;
    #clientCountry;
    #clientId;
    #clientName;
    #clientVat;
    #price;
    #contract;
    constructor(client) {
        this.#clientAddress = client.clientAddress;
        this.#clientCity = client.clientCity;
        this.#clientCountry = client.clientCountry;
        this.#clientId = client.clientId;
        this.#clientName = client.clientName;
        this.#clientVat = client.clientVat;
        this.#price = client.price;
        this.#contract = client.contract;
    }

    getClientAddress() {
        return this.#clientAddress;
    }

    getClientCity() {
        return this.#clientCity;
    }

    getClientCountry() {
        return this.#clientCountry;
    }

    getClientId() {
        return this.#clientId;
    }

    getClientName() {
        return this.#clientName;
    }

    getClientVat() {
        return this.#clientVat;
    }

    getPrice() {
        return this.#price;
    }

    getContract() {
        return this.#contract;
    }

}