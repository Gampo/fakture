import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/Page/CrudPage.js";
// import {ClientContractHandler} from "./ClientContractHandler.js";
import {dataTablesAssets} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTablesAssets.js";
import {dataTableSelectors} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTableSelectors.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/Page/pageEvents.js";
import ClientContractHandler from "./ClientContractHandler.js";
import InputAjaxSearch from "../../InputAjaxSearch.js";

export default class Client extends CrudPage {
    constructor(crudTable) {
        super(crudTable);
        this.addAdditionalFormReadyEventListener();
        this.dataTableOptions = {
            enableCheckboxes: true,
            shiftCheckboxModifier: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'created_at'},
            {order: 'DESC', orderBy: 'updated_at'}
        ];
    }

    addAdditionalFormReadyEventListener() {
        this.eventEmitter.on(pageEvents.entityFormReady, () => {
            this.addSelectChangeListener();
            this.initAPRAutocomplete();

            let clientContractHandler = new ClientContractHandler();
            clientContractHandler.init();
            console.log('aaaa');
        });
    }
    addSelectChangeListener() {
        let form = this.modal.getFormInModal();
        let selectClientTypeElement = form.querySelector('#clientType');
        let pibInput = form.querySelector('#pib');
        let companyLabel = form.querySelector('#companyIdLabel');
        let personalLabel = form.querySelector('#personalIdLabel');
        selectClientTypeElement.addEventListener('change', () => {
            if(selectClientTypeElement.value === '1') {
                pibInput.parentElement.classList.remove('hidden');
                pibInput.classList.remove('hidden');
                companyLabel.classList.remove('hidden');
                personalLabel.classList.add('hidden');
            }
            if(selectClientTypeElement.value === '2') {
                pibInput.parentElement.classList.add('hidden');
                pibInput.classList.add('hidden');
                companyLabel.classList.add('hidden');
                personalLabel.classList.remove('hidden');
            }
        });
    }

    initAPRAutocomplete() {
        if(document.getElementById('aprSearchContainer')) {
            let productSearchInput = new InputAjaxSearch({
                containerId: 'aprSearchContainer',
                searchInputId: 'entitySearchInput',
                optionClickCallback: this.populateFields,
                fetchUrl: `/client/searchApr/?query=`,
                showOptionsBeforeInput: false,
                inputClass: 'form-control',
                placeholder: 'Search...'
            });
            productSearchInput.init();
        }
    }

    populateFields(data) {
        let map = [
            {inputName: 'invoiceName', dataName: 'name'},
            {inputName: 'invoiceAddress', dataName: 'address'},
            {inputName: 'invoiceCity', dataName: 'city'},
            {inputName: 'invoiceCountry', dataName: 'country'},
            {inputName: 'mb', dataName: 'mb'},
            {inputName: 'pib', dataName: 'vat'}
        ];

        map.forEach((obj) => {
            let elem = document.getElementsByName(obj.inputName)[0];
            if(elem && data[obj.dataName]) {
                elem.value = data[obj.dataName];
            }
        });
    }


}