import {ClientFormElementSelectors} from "./ClientFormElementSelectors.js";
import {ClientContractEventNames} from "./ClientContractEventNames.js";

export default class ClientContract {
    constructor(params) {
        this.id = params.id;
        this.content = params.content;
        this.eventTank = params.eventTank;
        this.serviceIdsInUseOnConstruct = params.serviceIdsInUse;
        this.serviceId = params.serviceId ?? null;
        this.deleteButton = this.content.querySelector(`.${ClientFormElementSelectors.classes.deleteClientContract}`);
        this.serviceSelect = this.content.querySelector(`.${ClientFormElementSelectors.classes.clientContractServiceSelect}`);
    }

    init() {
        this.addEventListeners();
        this.disableUsedServices();
    }

    addEventListeners() {
       this.addServiceChangeEventListener();
       this.addServiceRemoveListener();
    }

    addServiceChangeEventListener() {
        this.serviceSelect.addEventListener('change', () => {
            let oldServiceIdDummy = this.serviceId;
            this.updateServiceIdProperty(this.serviceSelect.value);
            this.eventTank.dispatchEvent(new CustomEvent(ClientContractEventNames.clientContractServiceChanged, {
                    detail: {
                        id: this.id,
                        oldServiceId: oldServiceIdDummy,
                        newServiceId: this.serviceId,
                    }
            }
            ));
        });
    }

    addServiceRemoveListener() {
        this.deleteButton.addEventListener('click', () => {
            this.content.remove();
            this.eventTank.dispatchEvent(new CustomEvent(ClientContractEventNames.clientContractRemoved, {
                detail: {
                    id: this.id,
                    serviceId: this.serviceId
                }
            }))
        });
    }

    enableServiceOption(serviceId) {
        let option = this.serviceSelect.querySelector(`option[value="${serviceId}"]`);
        if(option && serviceId !== '-1') {
            option.disabled = false;
        }
    }

    disableServiceOption(serviceId) {
        let option = this.serviceSelect.querySelector(`option[value="${serviceId}"]`);
        if(option && serviceId !== '-1') {
            option.disabled = true;
        }
    }

    updateServiceOptions(serviceIdToEnable, serviceIdToDisable) {
        this.enableServiceOption(serviceIdToEnable);
        this.disableServiceOption(serviceIdToDisable);
    }

    updateServiceIdProperty(serviceId) {
        this.serviceId = serviceId;
    }

    disableUsedServices() {
        this.serviceIdsInUseOnConstruct.forEach((serviceId) => {
           this.disableServiceOption(serviceId);
        });
    }

    getContent() {
        return this.content;
    }

    getServiceId() {
        return this.serviceId;
    }

    getId() {
        return this.id;
    }
}