export const ClientContractEventNames = Object.freeze({
    clientContractServiceChanged: 'clientContractServiceChanged',
    clientContractRemoved: 'clientContractRemoved'
});