export const ClientFormElementSelectors = Object.freeze({
   ids: {
      addNewClientContactInformation: 'addContractInformation',
      contractInformationContainer: 'contractInformationContainer',
      contractInformationTemplate: 'clientContractTemplate'
   },
   classes: {
      clientContract: 'clientContract',
      deleteClientContract: 'deleteClientContract',
      clientContractServiceSelect: 'clientContractServiceSelect'
   }
});