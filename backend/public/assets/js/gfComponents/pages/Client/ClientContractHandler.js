import {ClientFormElementSelectors} from "./ClientFormElementSelectors.js";
import ClientContract from "./ClientContract.js";
import {ClientContractEventNames} from "./ClientContractEventNames.js";


export default class ClientContractHandler {

    contracts = {};

    idPrefix = 'c';

    constructor() {
        this.addClientContractButton =
            document.getElementById(ClientFormElementSelectors.ids.addNewClientContactInformation);
        this.contractInformationContainer =
            document.getElementById(ClientFormElementSelectors.ids.contractInformationContainer);
        this.contractInformationTemplate =
            document.getElementById(ClientFormElementSelectors.ids.contractInformationTemplate);
    }

    init() {
        this.addEventListeners();
        this.addContractRemovedListener();
        this.addServiceChangedForContractListener();
        this.registerExistingContracts();
    }

    addEventListeners() {
        this.addNewContractListener();
    }

    addNewContractListener() {
        this.addClientContractButton.addEventListener('click', () => {this.addNewContract()});
    }

    addContractRemovedListener() {
        this.contractInformationContainer.addEventListener(ClientContractEventNames.clientContractRemoved, (e) => {
            this.removeFromContractList(e.detail.id, e.detail.serviceId);
            this.updateServiceOptionsForContracts(e.detail.id, true,false, e.detail.serviceId);
        });
    }

    addServiceChangedForContractListener() {
        this.contractInformationContainer.addEventListener(ClientContractEventNames.clientContractServiceChanged, (e) => {
            this.updateServiceOptionsForContracts(e.detail.id, false,
                {oldServiceId:e.detail.oldServiceId, newServiceId: e.detail.newServiceId});
        });
    }

    registerExistingContracts() {
        this.contractInformationContainer.querySelectorAll(`.${ClientFormElementSelectors.classes.clientContract}`).forEach((existingContract) => {
            this.addExistingContract(existingContract);
        });
    }

    addExistingContract(content) {
        let contractId = this.generateUniqueId();
        let contract = new ClientContract({
            id: contractId,
            content:  content,
            eventTank: this.contractInformationContainer,
            serviceId: content.querySelector(`.${ClientFormElementSelectors.classes.clientContractServiceSelect}`).value,
            serviceIdsInUse: this.getUsedServiceIds()
        });
        this.contracts[contractId] = contract;
        contract.init();
    }


    addNewContract() {
        let contractId = this.generateUniqueId();
        let contract = new ClientContract({
            id: contractId,
            content:  this.contractInformationTemplate.content.cloneNode(true).querySelector(`.${ClientFormElementSelectors.classes.clientContract}`),
            eventTank: this.contractInformationContainer,
            serviceIdsInUse: this.getUsedServiceIds()
        });
        this.contracts[contractId] = contract;
        contract.init();
        this.contractInformationContainer.appendChild(contract.getContent());
    }

    removeFromContractList(contractId) {
        if(this.contracts[contractId]) {
            delete this.contracts[contractId];
        }
    }


    updateServiceOptionsForContracts(contractId, serviceDeleted = false, serviceChanged = false, serviceId = null) {
        Object.values(this.contracts).forEach((contract) => {
            if(serviceDeleted && contractId !== contract.getId()) {
                contract.enableServiceOption(serviceId);
                return;
            }
            if(serviceChanged && contractId !== contract.getId()) {
                contract.updateServiceOptions(serviceChanged.oldServiceId, serviceChanged.newServiceId);
            }
        });
    }

    generateUniqueId() {
        let id = Date.now();
        if(!this.contracts[id]) {
            return this.idPrefix + id;
        } else { // 1 check should suffice
            return this.idPrefix + Date.now();
        }
    }

    getUsedServiceIds() {
        let serviceIds = [];
        Object.values(this.contracts).forEach((contract) => {
           if(contract.getServiceId()) {
               serviceIds.push(contract.getServiceId());
           }
        });
        return serviceIds;
    }

}