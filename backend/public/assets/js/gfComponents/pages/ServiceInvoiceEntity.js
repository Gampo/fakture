export default class ServiceInvoiceEntity {

    mainRow;

    clone;

    nameInput;

    qtyInput;

    measureUnitInput;

    serviceIdInput;

    priceInput;

    taxSelect;

    basePriceInput;

    rowTaxInput;

    rowTotalInput;

    removeServiceButton;

    constructor(serviceData, eventTank, serviceDeletedEventName, recalculationRequiredEventName, contractUsedEventName) {
        this.serviceId = serviceData.id;
        this.clientId = parseInt(serviceData.tenant?.id)
        this.serviceName = serviceData.name;
        this.price = serviceData.price ? serviceData.price : '0.00';
        this.currency = serviceData.currency ?? '';
        this.optionElement = this.generateOptionElement();
        this.template = document.getElementById('serviceEntityTemplate');
        this.eventTank = eventTank;
        this.serviceDeletedEventName = serviceDeletedEventName;
        this.recalculationRequiredEventName = recalculationRequiredEventName;
        this.contractUsedEventName = contractUsedEventName;
        this.contract = null;
    }


    getOptionElement() {
        return this.optionElement;
    }

    getServiceId() {
        return this.serviceId;
    }

    getTax() {
        return this.taxSelect.value;
    }

    getPrice() {
        return this.priceInput.value.replace(/,/g, '') ;
    }

    getQty() {
        return this.qtyInput.value;
    }

    getMainRow() {
        return this.mainRow;
    }

    getRowTotalInput() {
        return this.rowTotalInput;
    }

    getBasePriceInput() {
        return this.basePriceInput;
    }

    getRowTaxInput() {
        return this.rowTaxInput;
    }

    getPriceInput() {
        return this.priceInput;
    }

    setPriceInputValue(value) {
        this.priceInput.value = value;
    }

    setBasePriceInputValue(value) {
        this.basePriceInput.value = (Number(value) * this.getQty()).toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2});
    }

    generateOptionElement() {
        let option = document.createElement('option');
        option.value = this.serviceId;
        option.innerText = this.serviceName + (this.currency !== '' ? ` - ${this.currency}` : '');
        return option;
    }

    disableOption() {
        this.optionElement.disabled = true;
        this.optionElement.style.background = 'lightgray';
        this.optionElement.style.color = 'white';
    }

    enableOption() {
        this.optionElement.disabled = false;
        this.optionElement.style.background = 'transparent';
        this.optionElement.style.color = '#6e707e';
    }

    generateServiceElement() {
        if ('content' in document.createElement('template')) {
            this.disableOption(); // Disable option as it is selected and should not be selectable until removed
            this.content = this.template.content;
            this.clone =  this.content.cloneNode(true);
            this.setProperties();
            this.setValues();
            this.addEventListeners();
            return this.clone;
        }
    }

    initFromExisting(mainRow) {
        this.disableOption();
        this.setProperties(mainRow);
        this.addEventListeners();
    }

    setProperties(mainRow = null) {
        let target = this.clone;
        if(mainRow) {
            target = mainRow;
        }
        this.mainRow = mainRow ?? this.clone.querySelector('.mainServiceEntityRow');
        this.nameInput = target.querySelector('input[name="selectedServices[name][]"]');
        this.qtyInput = target.querySelector('input[name="selectedServices[qty][]"]');
        this.measureUnitInput = target.querySelector('input[name="selectedServices[measureUnit][]"]');
        this.serviceIdInput = target.querySelector('input[name="selectedServices[serviceId][]"]');
        this.priceInput = target.querySelector('input[name="selectedServices[price][]"]');
        this.taxSelect = target.querySelector('select[name="selectedServices[tax][]"]');
        this.basePriceInput = target.querySelector('.basePrice');
        this.rowTaxInput = target.querySelector('.rowTax');
        this.rowTotalInput = target.querySelector('.rowTotal');
        this.removeServiceButton = target.querySelector('.removeService');
        this.useContractCheckbox = target.querySelector('.useContract');
    }

    setValues() {
        let amount = this.getAmountBasedOnContract();
        this.nameInput.value = this.serviceName;
        this.qtyInput.value = 1;
        this.serviceIdInput.value = this.serviceId;
        this.priceInput.value = Number(amount).toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2});
        this.basePriceInput.value = Number(amount).toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2});
        this.rowTaxInput.value = '0.00';
        this.rowTotalInput.value = Number(amount).toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2});
    }



    addEventListeners() {
       this.addRemoveServiceEventListener();
       this.addQuantityChangeListener();
       this.addTaxChangeListener();
       this.addPriceChangeListener();
    }

    addRemoveServiceEventListener() {
        this.removeServiceButton.addEventListener('click', () => {
            this.enableOption(); // Enable selecting this service again as it has been removed
            this.mainRow.remove();
            this.eventTank.dispatchEvent(new CustomEvent(this.serviceDeletedEventName, {'detail': this.serviceId}))
        });
    }

    addQuantityChangeListener() {
        this.qtyInput.addEventListener('change', () => {
            this.requireRecalculation();
        });
    }

    addTaxChangeListener() {
        this.taxSelect.addEventListener('change', () => {
           this.requireRecalculation();
        });
    }

    addPriceChangeListener() {
        this.priceInput.addEventListener('change', () => {
           this.requireRecalculation();
        });
    }

    requireRecalculation() {
        this.eventTank.dispatchEvent(new CustomEvent(this.recalculationRequiredEventName));
    }

    enableUseContract() {
        let checkedContracts =  document.querySelectorAll('.useContract[type="checkbox"]:checked');
        if(checkedContracts.length === 0) {
            this.useContractCheckbox.checked = true;
            this.dispatchContractEvent();
        }
        this.showContractCheckbox();
        this.useContractCheckbox.setAttribute('data-deadline', this.contract.deadline);
        this.addContractCheckboxListener();
    }

    enableUseContractForExisting() {
        this.showContractCheckbox();
        this.useContractCheckbox.setAttribute('data-deadline', this.contract.deadline);
        this.addContractCheckboxListener();
        if(this.useContractCheckbox.getAttribute('data-active-contract')) {
            this.useContractCheckbox.checked = true;
        }
    }

    disableUseContract() {
        this.hideContractCheckbox();
        this.useContractCheckbox.checked = false;
    }

    addContractCheckboxListener() {
        this.useContractCheckbox.addEventListener('change', () => {
            document.querySelectorAll('.useContract[type="checkbox"]:checked').forEach((checkedContract) => {
                if(checkedContract !== this.useContractCheckbox) {
                    checkedContract.checked = false;
                }
            });
            this.dispatchContractEvent();
        });
    }

    showContractCheckbox() {
        this.useContractCheckbox.parentElement.style.display = 'block';
    }

    hideContractCheckbox() {
        this.useContractCheckbox.parentElement.style.display = 'none';
    }
    dispatchContractEvent() {
        this.eventTank.dispatchEvent(new CustomEvent(this.contractUsedEventName, {
            detail: {
                checked: this.useContractCheckbox.checked,
                contract:this.contract,
                serviceId:this.serviceId
            }}
        ));
    }

    getAmountBasedOnContract() {
        if(this.contract && this.contract.price) {
            this.enableUseContract();
            if(this.contract.price.amount) {
                return this.contract.price.amount;
            }
        }
        return Number(this.price);
    }
}