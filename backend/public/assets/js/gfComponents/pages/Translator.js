import BasePage from "https://skeletor.greenfriends.systems/gfcomponents/0.x.x/components/BasePage.js";

export default class Translator extends BasePage {
    constructor(crudTable) {
        super(crudTable);
        this.modalStyleConfig = {
            create: {
                modalWidth: '1200px',
                modalHeight: '800px'
            },
            edit: {
                modalWidth: '1200px',
                modalHeight: '800px'
            }
        };
    }

    populateFields(data) {
        let map = [
            {inputName: 'id', dataName: 'id'},
            {inputName: 'originalString', dataName: 'originalString'},
            {inputName: 'translatedString', dataName: 'translatedString'},
            {inputName: 'language', dataName: 'language'},
            {inputName: 'languageId', dataName: 'languageId'},
            {inputName: 'createdAt', dataName: 'createdAt'},
            {inputName: 'updatedAt', dataName: 'updatedAt'},
        ];

        map.forEach((obj) => {
            let elem = document.getElementsByName(obj.inputName)[0];
            if(elem && data[obj.dataName]) {
                elem.value = data[obj.dataName];
            }
        });
    }
}