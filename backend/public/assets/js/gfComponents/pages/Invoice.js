import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/Page/CrudPage.js";
import {dataTablesAssets} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTablesAssets.js";
import {dataTableSelectors} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTableSelectors.js";

export default class Invoice extends CrudPage {
    constructor() {
        super();
        this.dataTableOptions = {
            enableCheckboxes: true,
            shiftCheckboxModifier: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'created_at, invoiceCode'},
            {order: 'DESC', orderBy: 'updated_at'}
        ];
        this.addExportAction();
    }

    addMapAction() {
        this.dataTableActions.mapProducts = {
            order:2,
            className: 'mapEntity',
            content: dataTablesAssets.mapIcon,
            callback: async (entity) => {
                //make a request
                //parse response - print message
                //request table reload
                console.log(entity);
            }
        };
    }

    addExportAction() {
        this.dataTableBulkActions.export = {
            content: 'Export',
            callback: async (ids) => {
                window.location = '/' + this.baseAction + '/export/?ids=' + ids;
            }
        }
    }

    setEditDataTableAction() {
        this.dataTableActions.edit = {
            order: 1,
            content: dataTablesAssets.editIcon,
            className: dataTableSelectors.classes.editEntityButton,
            callback: async (entity) => {
                window.location.href = this.endpoints.entityFormEndpoint + entity.id + '/';
            }
        }
    }
}