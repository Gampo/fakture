import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/Page/CrudPage.js";
import {dataTablesAssets} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTablesAssets.js";
import {dataTableSelectors} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTableSelectors.js";

export default class Activity extends CrudPage {
    constructor() {
        super();
    }

    populateFields(data) {
        let map = [
            {inputName: 'id', dataName: 'id'},
            {inputName: 'name', dataName: 'name'},
            {inputName: 'createdAt', dataName: 'createdAt'},
            {inputName: 'updatedAt', dataName: 'updatedAt'},
        ];

        map.forEach((obj) => {
            let elem = document.getElementsByName(obj.inputName)[0];
            if(elem && data[obj.dataName]) {
                elem.value = data[obj.dataName];
            }
        });
    }
}