document.addEventListener('DOMContentLoaded', () => {
   const invoiceInstructionTemplate = document.getElementById('invoiceInstructionTemplate');
   const addNewInstruction = document.getElementById('addInvoiceInstruction');
   const instructionsContainer = document.getElementById('invoiceInstructionContainer');
   const useForeignInvoicesCheckbox = document.getElementById('useForeignInvoice');
   const foreignInstructionsContainer = document.querySelector('.foreignInstructionsContainer');

   let maxNumOfInstructions = null;
   let selectedOptionValues = [];
   document.querySelectorAll('.invoiceInstructionSelect').forEach((select) => {
      selectedOptionValues.push(select.value);
   });

   const handleInstructionsVisibility = () => {
      foreignInstructionsContainer.style.display = useForeignInvoicesCheckbox.checked ? 'block' : 'none';
   }

   const setMaxNumberOfInstructions = () => {
      let invoiceInstructionSelect = document.querySelector('.invoiceInstructionSelect');
      if(invoiceInstructionSelect) {
         maxNumOfInstructions = invoiceInstructionSelect.querySelectorAll('option').length;
      }
   };

   const getNumberOfInstructions = () => {
     return document.querySelectorAll('.invoiceInstruction').length;
   };
   const getInvoiceInstructionTemplateHtml = () => {
      return invoiceInstructionTemplate.content.cloneNode(true);
   };

   const addDeleteInstructionListener = (clone) => {
      let removeButton = clone.querySelector('.deleteInvoiceInstruction');
      let select = clone.querySelector('.invoiceInstructionSelect');
      removeButton.addEventListener('click', () => {
         enableOptionsForExisting(select.value);
         removeButton.parentElement.remove();
      });
   };

   const addChangeInstructionListener = (clone) => {
      let select = clone.querySelector('.invoiceInstructionSelect');
      select.addEventListener('change', () => {
         enableOptionsForExisting(select.getAttribute('data-old-value'));
         disableOptionsForExisting(select.value, select);
         select.setAttribute('data-old-value', select.value);
      });
   };


   const disableOptions = (targetElement) => {
      document.querySelectorAll('.invoiceInstructionSelect').forEach((select) => {
         if(select !== targetElement) {
            targetElement.querySelector(`option[value="${select.value}"]`).disabled = true;
         }
      });
   }

   const disableOptionsForExisting = (value, initiator) => {
      document.querySelectorAll('.invoiceInstructionSelect').forEach((select) => {
         if(initiator !== select) {
            select.querySelector(`option[value="${value}"]`).disabled = true;
         }
      });
   };

   const enableOptionsForExisting = (value) => {
      document.querySelectorAll('.invoiceInstructionSelect').forEach((select) => {
         select.querySelectorAll('option:disabled').forEach((disabledOption) => {
            if(disabledOption.value === value) {
               disabledOption.disabled = false;
            }
         });
      });
   };

   const enableFirstSelectableOption = (targetElement) => {
     targetElement.querySelector('option:enabled').selected = true;
   };

   const initExisting = (container, selectedOption) => {
      addDeleteInstructionListener(container);
      addChangeInstructionListener(container);
      let select =  container.querySelector('.invoiceInstructionSelect');
      select.querySelectorAll('option').forEach((option) => {
         if(selectedOption.includes(option.value) && select.value !== option.value) {
            option.disabled = true;
         }
      });
   };


   handleInstructionsVisibility();

   useForeignInvoicesCheckbox.addEventListener('change', () => {
      handleInstructionsVisibility();
   });


   document.querySelectorAll('.invoiceInstruction').forEach((existingInstruction) => {
      initExisting(existingInstruction, selectedOptionValues);
   });
   setMaxNumberOfInstructions();

   addNewInstruction.addEventListener('click', () => {
      if(maxNumOfInstructions && getNumberOfInstructions() >= maxNumOfInstructions) {
         return;
      }
      let clone = getInvoiceInstructionTemplateHtml();
      let select = clone.querySelector('.invoiceInstructionSelect');
      addDeleteInstructionListener(clone);
      addChangeInstructionListener(clone);
      disableOptions(select);
      enableFirstSelectableOption(select)
      disableOptionsForExisting(select.value, select);
      select.setAttribute('data-old-value', select.value);
      instructionsContainer.appendChild(clone);
      if(maxNumOfInstructions === null) {
         setMaxNumberOfInstructions();
      }
      startEditor();
   });

      let toolbar = [
         // [groupName, [list of button]]
         ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
         ['font', ['superscript', 'subscript']],
         // ['fontsize', ['fontsize']],
         // ['color', ['color']],
         ['help', ['undo', 'redo']],
      ];

      $(document).ready(function() {
         startEditor();
      });

      function startEditor() {
         $('.instruction').summernote({
            dialogsInBody: true,
            tabsize: 2,
            height: 200,
            spellCheck: false,
            disableGrammar: true,
            callbacks: {
               onChange: function(contents, $editable) {
                  $editable.parents('.invoiceInstruction').find('[name="invoiceInstruction[instruction][]"]').val(contents);
               },
               onPaste: function (e) {
                  var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
                  e.preventDefault();
                  document.execCommand('insertText', false, bufferText);
               }
            },
            toolbar: toolbar
         });
      }
});