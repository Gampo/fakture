import PageFactory from "./gfComponents/pages/PageFactory.js";
import {dataTableSelectors} from "https://skeletor.greenfriends.systems/dtables/1.x/0.0/src/DataTable/dataTableSelectors.js";
import InvoiceForm from "./gfComponents/pages/InvoiceForm.js?v=1.0.3";
import InvoiceRecurringForm from "./gfComponents/pages/InvoiceRecurringForm.js?v=1.0.2";

$(document).ready(function() {
    let userId = $('#userId').attr('data-userId');

    //moment.js initialize
    // moment().format();

    $('.datepicker').datepicker({
        dateFormat: 'yy-m-d'
    });

    if (document.getElementById('invoiceForm')) {
        let invoicePage = new InvoiceForm();
    }
    if (document.getElementById('invoiceRecurringForm')) {
        let invoiceRecurringForm = new InvoiceRecurringForm();
    }

});

if (document.getElementById(dataTableSelectors.ids.table)) {
    document.addEventListener('DOMContentLoaded', () => {
        const pageIdentifier = document.getElementById(dataTableSelectors.ids.table).getAttribute(dataTableSelectors.attributes.page);
        console.log(pageIdentifier);
        const pageEntity = PageFactory.make(pageIdentifier);
        console.log(pageEntity);
        pageEntity.init();
    });
}
