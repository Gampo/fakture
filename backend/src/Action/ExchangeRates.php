<?php

namespace Fakture\Backend\Action;

use Fakture\Price\Service\ExchangeRate;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Skeletor\Mapper\NotFoundException;
use Tamtamchik\SimpleFlash\Flash;

class ExchangeRates extends Html
{

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, private ExchangeRate $exchangeRate
    ) {
        parent::__construct($logger, $config, $template);
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        $rates = $this->exchangeRate->convertForDate('2022-12-07', 'eur', 100);

        var_dump($rates);
        die();

        return $response->withStatus(302)->withHeader('Location', $url);
    }

}