<?php
namespace Fakture\Backend\Controller;

use Fakture\Invoice\Model\InvoiceBlank;
use Fakture\Invoice\Service\InvoiceRecurring;
use Fakture\Price\Service\Price;
use Fakture\Tenant\Repository\TenantRepository;
use Fakture\Invoice\Repository\InvoiceRepository;
use Fakture\Service\Repository\ServiceRepository as Service;
use Fakture\Client\Repository\ClientRepository as Client;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Psr\Log\LoggerInterface;
use Skeletor\Controller\AjaxCrudController;
use Fakture\User\Service\User;
use Skeletor\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;

class InvoiceRecurringController extends AjaxCrudController
{
    const TITLE_VIEW = "Pregledaj automatski račun";
    const TITLE_CREATE = "Kreiraj nov automatski račun";
    const TITLE_UPDATE = "Izmeni automatski račun: ";
    const TITLE_UPDATE_SUCCESS = "Automatski Račun izmenjen uspešno.";
    const TITLE_CREATE_SUCCESS = "Automatski Račun kreiran uspešno.";
    const TITLE_DELETE_SUCCESS = "Automatski Račun obrisan uspešno.";
    const PATH = 'invoice-recurring';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => false, 'useLink' => true];

    /**
     * @var TenantRepository
     */
    private $tenantRepo;
    private $clientRepo;
    private $user;
    private $logger;

    /**
     * @param InvoiceRepository $clientRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param TenantRepository $tenantRepo
     * @param User $userService
     */
    public function __construct(
        InvoiceRecurring $service, Session $session, Config $config, Flash $flash, Engine $template, LoggerInterface $logger,
        TenantRepository $tenantRepo, Client $clientRepo, Service $serviceRepo, User $user, Price $price
    ) {
        parent::__construct($service, $session, $config, $flash, $template);

        $this->tenantRepo = $tenantRepo;
        $this->clientRepo = $clientRepo;
        $this->serviceRepo = $serviceRepo;
        $this->user = $user;
        $this->price = $price;
        $this->logger = $logger;
    }

    /**
     * @TODO used for manual action during testing
     *
     * @return void
     */
    public function createInvoice()
    {
        $recurringInvoice = $this->service->getById($this->getRequest()->getAttribute('id'));

        $this->service->createInvoice($recurringInvoice);

        echo 'ok';
        exit();
    }

    /**
     * Create automatic invoices.
     *
     * @return void
     */
    public function createInvoices()
    {
        try {
            foreach ($this->tenantRepo->fetchAll() as $tenant) {
                if ($tenant->getId() == 1) {
                    continue;
                }
                echo 'tenant ' . $tenant->getName() . PHP_EOL;
//                var_dump(count($this->service->getEntities(['tenantId' => $tenant->getId()])));
                foreach ($this->service->getEntities(['tenantId' => $tenant->getId()]) as $invoice) {
                    echo 'created invoice for ' . $invoice->getId()  . PHP_EOL;
                    $this->service->createInvoice($invoice);
                    sleep(1);
                }
            }
        } catch (\Exception $e) {
            $msg = sprintf('Failed to create auto invoice %s for tenant %s: %s',
                $invoice->getId(), $tenant->getName(), $e->getMessage());
            $this->logger->error($msg);
        }
        exit();
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $this->setGlobalVariable('pageTitle', self::TITLE_CREATE);
        $invoice = null;
        $tenant = $this->tenantRepo->getById($this->user->getLoggedInTenantId());
        $tenantClient = $this->clientRepo->getById($tenant->getClientId());
        if ($id) {
            $invoice = $this->service->getById($id);
            $this->setGlobalVariable('pageTitle', self::TITLE_UPDATE . $invoice->getId());
        }

        $filter = [];
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== \Skeletor\User\Model\User::ROLE_ADMIN) {
            $tenant = $this->tenantRepo->getById($this->getSession()->getStorage()->offsetGet('tenantId'));
            $filter['tenantId'] = $tenant->getId();
            $users = $this->user->getByTenant($tenant);
        } else {
            $users = $this->user->getEntities();
            //must do better
            if ($id) {

            }
        }

        if (!$invoice) {
            //defaults
            $invoice = new InvoiceBlank(...[
                'issueDate' => new \DateTime(),
                'turnoverDate' => new \DateTime(),
                'deadlineDate' => (new \DateTime())->modify('+30 day'),
                'issueLocation' => ($tenant) ? $tenantClient?->getCity() : '',
                'turnoverLocation' => ($tenant) ? $tenantClient?->getCity(): ''
            ]);
        }

        return $this->respond('form', [
            'model' => $invoice,
            'tenantClient' => $tenantClient,
            'tenants' => $this->tenantRepo->fetchAll($filter),
            'clients' => $this->clientRepo->fetchAll($filter + ['isActive' => 1]),
            'users' => $users,
            'tenant' => $tenant,
        ]);
    }

    public function pdf()
    {
        $invoice = $this->service->getById((int) $this->getRequest()->getAttribute('id'));
        $tenantClient = $invoice->getTenant()->getClient();
        $user = $this->user->getById($this->user->getLoggedInUserId());

        $pdf = $this->service->parseDataForPdf($invoice, $tenantClient);
        $response = $this->respondPartial('pdf', [
            'invoice' => $invoice, 'tenantClient' => $tenantClient, 'user' => $user
        ]);
        $pdf->writeHTML($response->getBody()->getContents());
        $pdf->Output('example_001.pdf', 'I');

//        return $this->respondPartial('pdf', []);
    }

    public function create(): Response
    {
        try {
            $entity = $this->service->create($this->getRequest());
        } catch (ValidatorException $e) {
            foreach ($this->service->parseErrors() as $error) {
                $this->getFlash()->error($error);
            }
            return $this->redirect('/invoice-recurring/form/');
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect('/invoice-recurring/form/');
        }
        $this->getFlash()->success(static::TITLE_CREATE_SUCCESS);

        return $this->redirect('/invoice-recurring/view/');
    }

    public function update(): Response
    {
        try {
            $this->service->update($this->getRequest());
        } catch (ValidatorException $e) {
            foreach ($this->service->parseErrors() as $error) {
                $this->getFlash()->error($error);
            }
            return $this->redirect(sprintf('/invoice-recurring/form/%s/', $this->getRequest()->getAttribute('id')));
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect(sprintf('/invoice-recurring/form/%s/', $this->getRequest()->getAttribute('id')));
        }
        $this->getFlash()->success(static::TITLE_UPDATE_SUCCESS);

        return $this->redirect('/invoice-recurring/view/');
    }
}