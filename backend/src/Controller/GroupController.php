<?php
namespace Fakture\Backend\Controller;

use Fakture\Service\Service\Group;
use Skeletor\Controller\AjaxCrudController;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Tamtamchik\SimpleFlash\Flash;

class GroupController extends AjaxCrudController
{
    const TITLE_VIEW = "View groups";
    const TITLE_CREATE = "Create new group";
    const TITLE_UPDATE = "Edit group: ";
    const TITLE_UPDATE_SUCCESS = "Group updated successfully.";
    const TITLE_CREATE_SUCCESS = "Group created successfully.";
    const TITLE_DELETE_SUCCESS = "Group deleted successfully.";
    const PATH = 'service-group';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    /**
     * @param Group $service
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     */
    public function __construct(Group $service, Session $session, Config $config, Flash $flash, Engine $template)
    {
        parent::__construct($service, $session, $config, $flash, $template);
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $client = null;
        $this->setGlobalVariable('pageTitle', self::TITLE_CREATE);
        if ($id) {
            $client = $this->service->getById($id);
            $this->setGlobalVariable('pageTitle', self::TITLE_UPDATE . $client->getName());
        }

        return $this->respondPartial('form', [
            'model' => $client,
            'tenants' => $this->service->getTenants(),
            'loggedInTenantId' => $this->getSession()->getStorage()->offsetGet('tenantId'),
        ]);
    }

}