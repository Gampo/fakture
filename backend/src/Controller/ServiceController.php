<?php
namespace Fakture\Backend\Controller;

use Fakture\Service\Service\Group;
use Fakture\Service\Service\Service;
use Skeletor\Controller\AjaxCrudController;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Psr\Log\LoggerInterface as Logger;
use Tamtamchik\SimpleFlash\Flash;
use Fakture\User\Service\User;

class ServiceController extends AjaxCrudController
{
    const TITLE_VIEW = "View services";
    const TITLE_CREATE = "Create new service";
    const TITLE_UPDATE = "Edit service: ";
    const TITLE_UPDATE_SUCCESS = "Service updated successfully.";
    const TITLE_CREATE_SUCCESS = "Service created successfully.";
    const TITLE_DELETE_SUCCESS = "Service deleted successfully.";
    const PATH = 'service';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    /**
     * @param Service $serviceRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param Logger $logger
     * @param User $userService
     */
    public function __construct(
        Service $service, Session $session, Config $config, Flash $flash, Engine $template, private User $user,
        private Group $group
    ) {
        parent::__construct($service, $session, $config, $flash, $template);
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $model = null;
        $this->setGlobalVariable('pageTitle', self::TITLE_CREATE);
        if ($id) {
            $model = $this->service->getById($id);
            $this->setGlobalVariable('pageTitle', self::TITLE_UPDATE . $model->getName());
        }

        return $this->respondPartial('form', [
            'model' => $model,
            'tenants' => $this->service->getTenants(),
            'groups' => $this->group->getEntities(['tenantId' => $this->user->getLoggedInTenantId()]),
            'loggedInTenantId' => $this->getSession()->getStorage()->offsetGet('tenantId'),
        ]);
    }

    // @TODO could be replaced with getEntityData ?
    public function getService()
    {
        $this->getResponse()->getBody()->write(json_encode(
            $this->service->getDataForInvoice($this->getRequest()->getAttribute('id'))
        ));
        $this->getResponse()->getBody()->rewind();

        return $this->getResponse()->withHeader('Content-Type', 'application/json');
    }

    public function getEntities()
    {
        $this->getResponse()->getBody()->write(json_encode($this->service->getEntities([], null, null,
            $this->getRequest()->getQueryParams()['currency']
        )));
        $response = $this->getResponse()->withHeader('Content-Type', 'application/json');
        $response->getBody()->rewind();

        return $response;
    }
}