<?php
namespace Fakture\Backend\Controller;

use Fakture\Client\Service\Client as ClientService;
use Fakture\Service\Service\Service;
use Skeletor\Controller\AjaxCrudController;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Tamtamchik\SimpleFlash\Flash;

class ClientController extends AjaxCrudController
{
    const TITLE_VIEW = "View clients";
    const TITLE_CREATE = "Create new client";
    const TITLE_UPDATE = "Edit client: ";
    const TITLE_UPDATE_SUCCESS = "Client updated successfully.";
    const TITLE_CREATE_SUCCESS = "Client created successfully.";
    const TITLE_DELETE_SUCCESS = "Client deleted successfully.";
    const PATH = 'client';

    private $clientService;

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    /**
     * @param ClientService $service
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     */
    public function __construct(
        ClientService $service, Session $session, Config $config, Flash $flash, Engine $template,
        private Service $serviceService
    ) {
        parent::__construct($service, $session, $config, $flash, $template);
        $this->clientService = $service;
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $client = null;
        $this->setGlobalVariable('pageTitle', self::TITLE_CREATE);
        if ($id) {
            $client = $this->clientService->getById($id);
            $this->setGlobalVariable('pageTitle', self::TITLE_UPDATE . $client->getName());
        }

        return $this->respondPartial('form', [
            'model' => $client,
            'tenants' => $this->clientService->getTenants(),
            'loggedInTenantId' => $this->getSession()->getStorage()->offsetGet('tenantId'),
            'services' => $this->serviceService->getEntities()
        ]);
    }

    public function searchApr()
    {
        $this->getResponse()->getBody()->write(json_encode($this->clientService->searchApr(
            $this->getRequest()->getQueryParams()['query']
        )));
        $this->getResponse()->getBody()->rewind();

        return $this->getResponse()->withHeader('Content-Type', 'application/json');
    }

    public function getEntityData()
    {
        $this->getResponse()->getBody()->write(json_encode($this->service->getEntityData(
            (int) $this->getRequest()->getAttribute('id'), $this->getRequest()->getQueryParams()['currency']
        )));
        $this->getResponse()->getBody()->rewind();

        return $this->getResponse()->withHeader('Content-Type', 'application/json');
    }
}