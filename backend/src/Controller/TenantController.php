<?php
namespace Fakture\Backend\Controller;

use Fakture\Client\Service\Client;
use Fakture\Tenant\Service\Tenant;
use Fakture\User\Service\User;
use GuzzleHttp\Psr7\Response;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Skeletor\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\AjaxCrudController;
use League\Plates\Engine;

class TenantController extends AjaxCrudController
{
    const TITLE_VIEW = "View tenants";
    const TITLE_CREATE = "Create new tenant";
    const TITLE_UPDATE = "Edit tenant: ";
    const TITLE_UPDATE_SUCCESS = "Podaci su sačuvani uspešno.";
    const TITLE_CREATE_SUCCESS = "Tenant created successfully.";
    const TITLE_DELETE_SUCCESS = "Tenant deleted successfully.";
    const PATH = 'tenant';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => false, 'useLink' => true];

    /**
     * @param Tenant $service
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param User $userService
     */
    public function __construct(
        Tenant $service, Session $session, Config $config, Flash $flash, Engine $template, User $userService, Client $client
    ) {
        parent::__construct($service, $session, $config, $flash, $template);

        $this->userService = $userService;
        $this->client = $client;
    }

    public function create(): Response
    {
        try {
            $entity = $this->service->create($this->getRequest());
        } catch (ValidatorException $e) {
            foreach ($this->service->parseErrors() as $error) {
                $this->getFlash()->error($error);
            }
            return $this->redirect(sprintf('/tenant/form/'));
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect(sprintf('/tenant/form/'));
        }
        $this->getFlash()->success(static::TITLE_CREATE_SUCCESS);

        return $this->redirect('/tenant/view/');
    }

    public function update(): Response
    {
        try {
            $this->service->update($this->getRequest());
        } catch (ValidatorException $e) {
            foreach ($this->service->parseErrors() as $error) {
                $this->getFlash()->error($error);
            }
            return $this->redirect(sprintf('/tenant/form/%s/', $this->getRequest()->getAttribute('id')));
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect(sprintf('/tenant/form/%s/', $this->getRequest()->getAttribute('id')));
        }
        $this->getFlash()->success(static::TITLE_UPDATE_SUCCESS);
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') === \Skeletor\User\Model\User::ROLE_ADMIN) {
            return $this->redirect('/tenant/view/');
        }
        return $this->redirect('/invoice/view/');
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $model = $tenantClient = null;
        $this->setGlobalVariable('pageTitle', static::TITLE_CREATE);
        if ($id) {
            $model = $this->service->getById($id);
            if ($model->getClientId()) {
                $tenantClient = $this->client->getById($model->getClientId());
            }
            $this->setGlobalVariable('pageTitle', static::TITLE_UPDATE . $model->getName());
        }

        return $this->respondPartial('form', [
            'model' => $model,
            'tenantClient' => $tenantClient,
            'adminPath' => $this->tableViewConfig['adminPath'],
        ]);
    }
}