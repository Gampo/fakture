<?php
namespace Fakture\Backend\Controller;

//use Fakture\Tenant\Repository\TenantRepository;
use Fakture\Invoice\Repository\TemplateRepository;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Controller\CrudController;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Activity\Service\Activity;

class TemplateController extends CrudController
{
    const TITLE_VIEW = "View templates";
    const TITLE_CREATE = "Create new template";
    const TITLE_UPDATE = "Edit template: ";
    const TITLE_UPDATE_SUCCESS = "Template updated successfully.";
    const TITLE_CREATE_SUCCESS = "Template created successfully.";
    const TITLE_DELETE_SUCCESS = "Template deleted successfully.";
    const PATH = 'template';

    /**
     * @param TemplateRepository $templateRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param Logger $logger
     */
    public function __construct(
        TemplateRepository $templateRepo, Session $session, Config $config, Flash $flash, Engine $template, Logger $logger,
        Activity $activity
    ) {
        parent::__construct($templateRepo, $session, $config, $flash, $template, $logger, null, $activity);

    }

}