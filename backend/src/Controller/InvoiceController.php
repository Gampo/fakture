<?php
namespace Fakture\Backend\Controller;

use Fakture\Efakture\Mapper\EfaktureInvoice;
use Fakture\Efakture\Model\InvoiceStatus;
use Fakture\Invoice\Model\InvoiceBlank;
use Fakture\Invoice\Service\Invoice;
use Fakture\Price\Service\Price;
use Fakture\Tenant\Repository\TenantRepository;
use Fakture\Invoice\Repository\InvoiceRepository;
use Fakture\Service\Repository\ServiceRepository as Service;
use Fakture\Client\Repository\ClientRepository as Client;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Skeletor\Controller\AjaxCrudController;
use Fakture\User\Service\User;
use Skeletor\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;
use Fakture\Efakture\Service\Api;

class InvoiceController extends AjaxCrudController
{
    const TITLE_VIEW = "Pregledaj račun";
    const TITLE_CREATE = "Kreiraj nov račun";
    const TITLE_UPDATE = "Izmeni račun: ";
    const TITLE_UPDATE_SUCCESS = "Račun izmenjen uspešno.";
    const TITLE_CREATE_SUCCESS = "Račun kreiran uspešno.";
    const TITLE_DELETE_SUCCESS = "Račun obrisan uspešno.";
    const PATH = 'invoice';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => false, 'useLink' => true];

    /**
     * @param InvoiceRepository $clientRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param TenantRepository $tenantRepo
     * @param User $userService
     */
    public function __construct(
        Invoice $service, Session $session, Config $config, Flash $flash, Engine $template,
        private TenantRepository $tenantRepo, private Client $clientRepo, private User $user, private Api $api
    ) {
        parent::__construct($service, $session, $config, $flash, $template);
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $this->setGlobalVariable('pageTitle', self::TITLE_CREATE);
        $invoice = null;
        $tenant = $this->tenantRepo->getById($this->user->getLoggedInTenantId());
        $tenantClient = $this->clientRepo->getById($tenant->getClientId());
        if ($id) {
            $invoice = $this->service->getById($id);
            $this->setGlobalVariable('pageTitle', self::TITLE_UPDATE . $invoice->getId());
        }

        $filter = [];
        $users = [];
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== \Skeletor\User\Model\User::ROLE_ADMIN) {
            $tenant = $this->tenantRepo->getById($this->getSession()->getStorage()->offsetGet('tenantId'));
            $filter['tenantId'] = $tenant->getId();
            $users = $this->user->getByTenant($tenant);
        } else {
            //must do better
            if ($id) {

            }
        }

        if (!$invoice) {
            //defaults
            $invoice = new InvoiceBlank(...[
                'issueDate' => new \DateTime(),
                'turnoverDate' => new \DateTime(),
                'deadlineDate' => (new \DateTime())->modify('+30 day'),
                'issueLocation' => ($tenant) ? $tenantClient?->getCity() : '',
                'turnoverLocation' => ($tenant) ? $tenantClient?->getCity(): ''
            ]);
        }

        return $this->respond('form', [
            'model' => $invoice,
            'tenantClient' => $tenantClient,
            'tenant' => $tenant,
            'tenants' => $this->tenantRepo->fetchAll($filter, null, ['orderBy' => 'name', 'dir' => 'ASC']),
            'clients' => $this->clientRepo->fetchAll($filter + ['isActive' => 1], null, ['orderBy' => 'name', 'dir' => 'ASC']),
            'users' => $users,
        ]);
    }

    /**
     * @return \Psr\Http\Message\MessageInterface|void
     */
    public function pdf()
    {
        $invoice = $this->service->getById((int) $this->getRequest()->getAttribute('id'));

        return $this->service->returnPdfToDownloadFromInvoice($invoice, $this->getResponse());
    }

    public function create(): Response
    {
        try {
            $entity = $this->service->create($this->getRequest());
        } catch (ValidatorException $e) {
            foreach ($this->service->parseErrors() as $error) {
                $this->getFlash()->error($error);
            }
            return $this->redirect(sprintf('/invoice/form/%s/', $this->getRequest()->getAttribute('id')));
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect(sprintf('/invoice/form/%s/', $this->getRequest()->getAttribute('id')));
        }
        $this->getFlash()->success(static::TITLE_CREATE_SUCCESS);

        return $this->redirect('/invoice/view/');
    }

    public function update(): Response
    {
        try {
            $this->service->update($this->getRequest());
        } catch (ValidatorException $e) {
            foreach ($this->service->parseErrors() as $error) {
                $this->getFlash()->error($error);
            }
            return $this->redirect(sprintf('/invoice/form/%s/', $this->getRequest()->getAttribute('id')));
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect(sprintf('/invoice/form/%s/', $this->getRequest()->getAttribute('id')));
        }
        $this->getFlash()->success(static::TITLE_UPDATE_SUCCESS);

        return $this->redirect('/invoice/view/');
    }

    public function void()
    {
        /* @var \Fakture\Invoice\Model\Invoice $invoice */
        $invoice = $this->service->getById($this->getRequest()->getAttribute('id'));
        try {
            $this->service->voidInvoice($invoice);
            $this->getFlash()->success('Faktura uspešno stornirana.');
        } catch(\Exception $e) {
            $this->getFlash()->error($e->getMessage());
        }

        return $this->redirect('/invoice/view/');
    }

    public function flow()
    {
        $invoice = $this->service->getById($this->getRequest()->getAttribute('id'));
        $this->service->advanceFlow($invoice);

        return $this->redirect('/invoice/view/');
    }

    public function test()
    {
        // SalesInvoiceId
//        $this->api->getExitInvoice(761920);
        $tenant = $this->tenantRepo->getById(5);
//        $this->api->checkIfCompanyRegistered($tenant);

//        $sefInvoiceData = $this->api->getOutputInvoice($tenant, 1269762);
//        var_dump($sefInvoiceData);
//        die();

//        $this->api->voidInvoice($tenant, 1269762, 'test');
//        die();

//        $this->api->cancelInvoice($tenant, 761920, 'test');
//        die();

        $invoice = $this->service->getById(231);
        $this->api->sendUblInvoice($invoice, $this->clientRepo->getById($tenant->getClientId()));

//        foreach ($this->api->getOutputInvoiceIds($tenant) as $id) {
//            $sefInvoiceData = $this->api->getOutputInvoice($tenant, $id);
//            if ($sefInvoiceData->Status === 'Sent') {
//                var_dump($sefInvoiceData);
//                die();
//            }
//        }
        die();

    }

    public function createEfakturaInvoice(\Fakture\Invoice\Model\Invoice $invoice)
    {
        $client = $this->clientRepo->getById($invoice->getTenant()->getClientId());
        $result = $this->api->sendUblInvoice($invoice, $client);
//        $id = $this->efaktureInvoice->insert([
//            'invoiceId' => $invoice->getId(),
//            'sefInvoiceId' => $result->InvoiceId,
//            'sefPurchaseInvoiceId' => $result->PurchaseInvoiceId,
//            'sefSalesInvoiceId' => $result->SalesInvoiceId,
//            'status' => 'Sent',
//        ]);
//        var_dump($id);
    }
}