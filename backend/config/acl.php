<?php

$guest = [
    '/',
    '/fetchExchangeRate/',
    '/invoice-recurring/createInvoices/',
    '/login/loginForm/',
    '/login/login/',
    '/login/resetPassword',
    '/cron/*',
];

// merchant admin
$level3 = [
    '/client/*',
    '/invoice/*',
    '/invoice-recurring/*',
    '/tenant/form/*',
    '/tenant/update/*',
    '/tenant/create/*',
    '/price/*',
    '/service/*',
    '/service-group/*',
    '/user/form/*',
    '/template/view/*',
    '/template/form/*',
    '/user/view/*',
    '/user/update/*',
    '/login/logout/',
];

// merchant staff
$level2 = [
    '/client/view/',
    '/client/tableHandler/*',
    '/client/form/*', // can view client info, cannot write
    '/invoice/*',
    '/tenant/form/*',
    '/tenant/update/*',
    '/tenant/create/*',
    '/price/*',
    '/service/*',
    '/service-group/*',
    '/user/form/*',
    '/template/view/*',
    '/template/form/*',
    '/user/view/*',
    '/user/update/*',
    '/login/logout/',
];

$level1 = [
    '/cache/*',
    '/user/*',
    '/tenant/*',
    '/template/*',
    '/translator/*',
    '/activity/*',
];

//can also see everything level2 can see
$level1 = array_merge($level3, $level2, $level1);

return [
    0 => $guest,
    1 => $level1,
    2 => $level2,
    3 => $level3,
];