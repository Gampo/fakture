<?php

/**
 * Define routes here.
 *
 * Routes follow this format:
 *
 * [METHOD, ROUTE, CALLABLE] or
 * [METHOD, ROUTE, [Class => method]]
 *
 * When controller is used without method (as string), it needs to have a magic __invoke method defined.
 *
 * Routes can use optional segments and regular expressions. See nikic/fastroute
 */
//@TODO find a proper way to use adminPath
/**
 * @var $adminPath string secret path to admin
 */
return [
//    ['GET', '/git/bb-hook', 'Bipsys\Controller\GitController'],

    // backend
    [['GET'], '/', \Fakture\Backend\Action\Index::class],
    [['GET'], '/fetchExchangeRate/', \Fakture\Backend\Action\ExchangeRates::class],
    [['GET', 'POST'], '/login/{action}/', \Fakture\Backend\Controller\LoginController::class],
    [['GET', 'POST'], '/user/{action}/[{userId}/]', \Fakture\Backend\Controller\UserController::class],
    [['GET', 'POST'], '/client/{action}/[{id}/]', \Fakture\Backend\Controller\ClientController::class],
    [['GET', 'POST'], '/service/{action}/[{id}/]', \Fakture\Backend\Controller\ServiceController::class],
    [['GET', 'POST'], '/service-group/{action}/[{id}/]', \Fakture\Backend\Controller\GroupController::class],
    [['GET', 'POST'], '/tenant/{action}/[{id}/]', \Fakture\Backend\Controller\TenantController::class],
    [['GET', 'POST'], '/invoice/{action}/[{id}/]', \Fakture\Backend\Controller\InvoiceController::class],
    [['GET', 'POST'], '/invoice-recurring/{action}/[{id}/]', \Fakture\Backend\Controller\InvoiceRecurringController::class],

    [['GET', 'POST'], '/template/{action}/[{id}/]', \Fakture\Backend\Controller\TemplateController::class],
    [['GET', 'POST'], '/activity/{action}/[{id}/]', \Skeletor\Activity\Controller\ActivityController::class],
    [['GET', 'POST'], '/translator/{action}/[{id}/]', \Skeletor\Translator\Controller\TranslatorController::class],


];
