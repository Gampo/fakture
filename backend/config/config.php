<?php

date_default_timezone_set('Europe/Belgrade');

return array(
    'baseUrl' => 'https://fakture.djavolak.info',
    'appType' => 'backend',
    'timezone' => 'Europe/Belgrade',
    'adminPath' => '',
    'imageBasePath' => IMAGES_PATH,
    'compileAssets' => false,
    'mailRecipients' => [
        'errorNotice' => [
            'djavolak@mail.ru',
        ],
        'contactForm' => [
            'djavolak@mail.ru',
        ],
        'general' => [
            'djavolak@mail.ru',
        ]
    ],
);

