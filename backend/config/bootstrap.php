<?php

use Skeletor\User\Repository\UserRepository;
use Laminas\Session\SessionManager;
use Laminas\Session\ManagerInterface;
use Laminas\Session\Config\SessionConfig;
use Monolog\ErrorHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Acl\Acl;
use Skeletor\Mapper\PDOWrite;
use Skeletor\Mapper\PDORead;
use \League\Flysystem\Filesystem;
use League\Plates\{Engine, Template\Theme, Extension\ExtensionInterface};

class GF_Mail_Log_Handler extends \Monolog\Handler\AbstractHandler
{
    /**
     * @var \Fakture\Service\Mailer
     */
    private $mail;

    public function __construct($level = \Monolog\Logger::ERROR, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
    }

    public function setMail(\Fakture\Service\Mailer $mail)
    {
        $this->mail = $mail;
    }

    public function handle(array $record): bool
    {
        if ($record['level'] >= \Monolog\Logger::ERROR) {
            $this->mail->applicationErrorNotice($record);
        }

        return false;
    }
}



class FormToken implements ExtensionInterface
{
    public function register(Engine $engine)
    {
        $engine->registerFunction('formToken', [$this, 'formToken']);
    }

    public function formToken()
    {

    }
}


$containerBuilder = new \DI\ContainerBuilder;
/* @var \DI\Container $container */
$container = $containerBuilder
//    ->addDefinitions(require_once __DIR__ . '/config_web.php')
    ->build();

$container->set(\League\Route\Router::class, function() use ($container) {
    $router = new League\Route\Router();

    $adminPath = $container->get(Config::class)->adminPath;
    $routeList = require APP_PATH . '/config/routes.php';

    var_dump($routeList);
    die();

    $router->group('/admin', function (\League\Route\RouteGroup $route) {
        $route->map('GET', '/acme/route1', 'AcmeController::actionOne')->setName('actionOne');
        $route->map('GET', '/acme/route2', 'AcmeController::actionTwo')->setName('actionTwo');
    });
});

$container->set(\Skeletor\User\Repository\UserRepositoryInterface::class, function() use ($container) {
    return $container->get(\Skeletor\User\Repository\UserRepository::class);
});

// @TODO must do better
//$container->set(\Skeletor\Translator\Service\Translator::class, function() use ($container) {
//    return new \Skeletor\Translator\Service\Translator(
//        $container->get(\Skeletor\Translator\Repository\TranslatorRepository::class),
//        1,
//        $container->get(\Redis::class)
//    );
//});

$container->set(Engine::class, function() use ($container) {
    $defaultTheme = APP_PATH . '/../vendor/dj_avolak/skeletor/themes/admin';
    $theme = APP_PATH . '/../themes/admin';
    $plates = new \League\Plates\Engine($theme);
    $plates->addFolder('defaultTheme', $defaultTheme, true);
    $plates->addFolder('layout', APP_PATH . '/../themes/admin/layout');
    $plates->addFolder('partialsGlobal', APP_PATH . '/../themes/admin/partials/global');
    $plates->addFolder('partialsGlobalDefault', $defaultTheme . '/partials/global');
    $plates->registerFunction('printError', function($error, $label) use($plates) {
        return $plates->render('partialsGlobal::error', ['error' => $error, 'label' => $label]);
    });
    $plates->registerFunction('formToken', function () { return \Volnix\CSRF\CSRF::getHiddenInputString(); });
    $translator = $container->get(\Skeletor\Translator\Service\Translator::class);
    $translator(2);
    $plates->loadExtension($translator);

    return $plates;
});

$container->set(Filesystem::class, function() use ($container) {
    $adapter = new League\Flysystem\Local\LocalFilesystemAdapter(APP_PATH);

    return new Filesystem($adapter);
});

$container->set(\FastRoute\Dispatcher::class, function() use ($container) {
    $adminPath = $container->get(Config::class)->adminPath;
    $routeList = require APP_PATH . '/config/routes.php';

    /** @var \FastRoute\Dispatcher $dispatcher */
    return FastRoute\simpleDispatcher(
        function (\FastRoute\RouteCollector $r) use ($routeList) {
            foreach ($routeList as $routeDef) {
                $r->addRoute($routeDef[0], $routeDef[1], $routeDef[2]);
            }
        }
    );
});

$container->set(Acl::class, function() use ($container) {
    $acl = new Acl(
        $container->get(ManagerInterface::class),
        $container->get(Config::class),
        require APP_PATH . '/config/acl.php',
        require APP_PATH . '/config/aclMessages.php'
    );

    return $acl;
});

$container->set(Skeletor\Middleware\MiddlewareInterface::class, function() use ($container) {
    return new \Skeletor\Middleware\AuthMiddleware(
        $container->get(ManagerInterface::class),
        $container->get(Config::class),
        $container->get(Flash::class),
        $container->get(UserRepository::class),
        $container->get(Acl::class)
    );
});

$container->set(Config::class, function() use ($container) {
    $params = include(APP_PATH . "/config/config.php");
    $config = new \Laminas\Config\Config($params);
    $config = $config->merge(new \Laminas\Config\Config(include(APP_PATH . "/config/config-local.php")));
    $config = $config->merge(new \Laminas\Config\Config(['gitLog' => file_get_contents(DATA_PATH . '/.gitLog')]));

    return $config;
});

$container->set(\Skeletor\User\Model\UserFactoryInterface::class, function() use ($container) {
    return new \Fakture\User\Model\UserFactory(
        $container->get(\DateTime::class),
        $container->get(\Fakture\Tenant\Repository\TenantRepository::class)
    );
});

$container->set(ManagerInterface::class, function() use ($container) {
    // server should keep session data for AT LEAST
    ini_set('session.gc_maxlifetime', 60*60*24);
    // each client should remember their session id for EXACTLY 1 hour
//    session_set_cookie_params(3600);
    $sessionConfig = new SessionConfig();
    $redisHost = array_keys($container->get(Config::class)->redis->hosts->toArray())[0];
    $redisPort = array_values($container->get(Config::class)->redis->hosts->toArray())[0];
    $sessionConfig->setOptions([
        'remember_me_seconds' => 2592000, //2592000, // 30 * 24 * 60 * 60 = 30 days
        'use_cookies'         => true,
//        'cookie_httponly'     => true,
        'name'                => 'fakture',
        'cookie_lifetime'     => 30 * 24 * 60 * 60,
        'phpSaveHandler'      => 'redis',
        'savePath'            => sprintf('tcp://%s:%s?weight=1&timeout=1', $redisHost, $redisPort),
    ]);
    $session = new SessionManager($sessionConfig);
    $session->start();

    return $session;
});

$container->set(\Skeletor\Action\Web\NotFoundInterface::class, function() use ($container) {
    return $container->get(\Skeletor\Action\Web\NotFound::class);
});

$container->set(Logger::class, function() use ($container) {
    $logger = new \Monolog\Logger('fakture');
    $date = $container->get(\DateTime::class);
    $logDir = DATA_PATH . '/logs/';
    $logSubDir = $logDir . $date->format('Y') . '-' . $date->format('m');
    $logFile = $logSubDir . '/' . gethostname() . '-backend-' . $date->format('d') . '.log';
    $debugLog = DATA_PATH . '/logs/'. gethostname() . '-backend-debug.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        mkdir($logDir);
    }
    if (!is_dir($logSubDir)) {
        mkdir($logSubDir);
    }
    if (!is_file($logFile)) {
        touch($logFile);
    }
    $logger->pushHandler(
        new StreamHandler($logFile, \Monolog\Logger::ERROR)
    );
    $env = strtolower(getenv('APPLICATION_ENV'));
//    if ($env && strtolower($env) === 'production') {
//        $mailHandler = new GF_Mail_Log_Handler(\Monolog\Logger::ERROR, true);
//        $mailHandler->setMail($container->get(\WCP\Memo\Product\Mailer::class));
//        $logger->pushHandler($mailHandler);
//    }
    $logger->pushHandler(
        new StreamHandler($debugLog,\Monolog\Logger::DEBUG)
    );
    if ($env !== 'production') {
        $logger->pushHandler(new BrowserConsoleHandler());
    }
    ErrorHandler::register($logger);

    return $logger;
});

$container->set(PDOWrite::class, function() use ($container) {
    $config = $container->get(Config::class);
    $dsn = "mysql:host={$config->db->write->host};dbname={$config->db->write->name}";
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    return new PDOWrite($dsn, $config->db->write->user, $config->db->write->pass, $options);
});

$container->set(PDORead::class, function() use ($container) {
    $dbConfig = $container->get(Config::class)->db->read->toArray();
    shuffle($dbConfig);
    $dbConfig = $dbConfig[0];
    $dsn = "mysql:host={$dbConfig['host']};dbname={$dbConfig['name']}";
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    return new PDORead($dsn, $dbConfig['user'], $dbConfig['pass'], $options);
});

$container->set(\Redis::class, function() use ($container) {
    $config = $container->get(Config::class);
    $redis = new \Redis();
    foreach ($config->redis->hosts as $host => $port) {
        $redis->connect($host, $port);
    }
    return $redis;
});

$container->set(\DateTime::class, function() use ($container) {
    $dt = new \DateTime('now', new \DateTimeZone($container->get(Config::class)->offsetGet('timezone')));
    return $dt;
});


$container->set(Flash::class, function () use ($container) {
    //session needs to be started for flash
    $container->get(ManagerInterface::class);

    return new Flash();
});

$container->set(\Laminas\Mail\Transport\TransportInterface::class, function() use ($container) {
    $transport = new \Laminas\Mail\Transport\Smtp();
    $options = new \Laminas\Mail\Transport\SmtpOptions($container->get(Config::class)->mailServer->toArray());
    $transport->setOptions($options);

    return $transport;
});

//$container->set(\com\zoho\api\authenticator\store\TokenStore::class, function() use ($container) {
//    return new \WCP\Zoho\Product\RedisStore($container->get(\Redis::class));
//});

return $container;